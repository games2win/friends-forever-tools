﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Prime31;

public class ChaptersCompletedUIText : MonoBehaviour
{
	public Text chapterText;

    public int currentButtonNumber;

    void OnEnable()
    {
        chapterText = GetComponent<Text>();
        Messenger.AddListener<int,int>("DynamicChapterStoryNumber", changeStatus);
        Messenger.MarkAsPermanent("DynamicChapterStoryNumber");


    }

    void OnDisable()
    {
        Messenger.RemoveListener<int,int>("DynamicChapterStoryNumber", changeStatus);
    }

    void changeStatus(int currentStoryNumber, int buttonnumber)
    {
        // Debug.Log("currentStoryNumber>>>>>> " + currentButtonNumber + "  " + buttonnumber);
        if (currentButtonNumber == buttonnumber)
        {
            // Debug.Log("currentStoryNumber>>>>>> " + currentStoryNumber);
            if (currentStoryNumber != 7)
            {
                if (currentStoryNumber == 1)
                {
                    int cChapter = P31Prefs.getInt("currentChapterNumber", 1);
                    // Debug.Log("currentChapterNumber>>>>>> " + cChapter);

                    if (P31Prefs.getBool("isStoryCompleted"))
                    {
                        if (cChapter == 1)
                        {
                            chapterText.text = "30/30";
                        }
                        else
                        {
                            chapterText.text = (cChapter - 1) + "/30";
                        }
                    }
                    else
                    {
                        chapterText.text = (cChapter - 1) + "/30";
                        if (chapterText.text == "-1" + "/30")
                        {
                            chapterText.text = "0" + "/30";
                        }
                    }
                    //Debug.Log("chapterText>>>>>> " + chapterText.text);
                }
                else
                {
                    int cChapter = P31Prefs.getInt("Story" + currentStoryNumber + "currentChapterNumber", 1);
                    if (P31Prefs.getBool("isStory" + currentStoryNumber + "Completed"))
                    {
                        if (cChapter == 1)
                        {
                            chapterText.text = "30/30";
                        }
                        else
                        {
                            chapterText.text = (cChapter - 1) + "/30";
                        }
                    }
                    else
                    {
                        Debug.Log("COMPLETED CHAPTER" + cChapter);
                        chapterText.text = (cChapter - 1) + "/30";
                        if (chapterText.text == "-1" + "/30")
                        {
                            chapterText.text = "0" + "/30";
                        }
                    }

                }
            } 
        }
    }
}
