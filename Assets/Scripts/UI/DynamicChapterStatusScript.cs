﻿using System.Collections;
using System.Collections.Generic;
using Prime31;
using UnityEngine;
using UnityEngine.UI;

public class DynamicChapterStatusScript : MonoBehaviour {

    public int currentButtonNumber;
	[HideInInspector]
	public int currentStoryNumber;

    private Text currentStoryName;

    private Text currentGenre;

    private Image currentStoryImage;

    private Button currentButton;

    private int[] storyOrderArray = {2, 7, 1, 4, 3, 5, 6};

    private GameObject heartpoof;

    public Text chapterTextStatus;

    public GameObject ChapterStatus;
    public GameObject CoinStatus;
    public Text CoinTextValue;


    void Awake()
	{
        currentStoryImage = GetComponent<Image>();
        currentButton = GetComponent<Button>();
        currentStoryName = transform.GetChild(0).GetComponent<Text>();
        currentGenre = transform.GetChild(1).GetComponent<Text>();
        if (GameConstants.currentPlayingStoryNumber == 0)
        {
            setDynamicValues();
        }
        else
        {
            setExistingDynamicValues();
        }
        ChapterPlayedStatus();
        UnlockStatus();
    }
	void Start()
	{
		loadStoryDetails();
	}
	
	void OnEnable()
	{
		currentButton.onClick.AddListener(startStory);
    }
	void OnDisable()
	{
		currentButton.onClick.RemoveListener(startStory);
	}

	void setDynamicValues()
	{
        if (GameConstants.isStoryOrderFetched)
        {
            storyOrderArray = PlayerPrefsX.GetIntArray("story_order");
           
        }
        currentStoryNumber = storyOrderArray[currentButtonNumber - 1];
        Messenger.Broadcast<int,int>("DynamicChapterStoryNumber", currentStoryNumber,currentButtonNumber);
    }

    void setExistingDynamicValues()
    {
        if (GameConstants.isStoryOrderFetched)
        {
            storyOrderArray = PlayerPrefsX.GetIntArray("story_order");
           
           
        }
         storyOrderArray = storyOrderArray.ShuffleStory(GameConstants.currentPlayingStoryNumber);
        currentStoryNumber = storyOrderArray[currentButtonNumber - 1];
        Messenger.Broadcast<int,int>("DynamicChapterStoryNumber", currentStoryNumber,currentButtonNumber);
    }

	void loadStoryDetails()
	{
        currentStoryImage.sprite = Resources.Load<Sprite>("Story"+currentStoryNumber+"Thumb");
        currentStoryName.text = GameConstants.getFullStoryName(currentStoryNumber).ToUpper();
        currentGenre.text = GameConstants.getGenre(currentStoryNumber).ToUpper();
    }

	public void startStory()
	{
        if (GameConstants.checkIfStoryUnlocked(currentStoryNumber))
        {
            heartpoof = Resources.Load("HeartPoofEdited") as GameObject;
            Instantiate(heartpoof, this.transform.position, Quaternion.identity);
            StartCoroutine("loadStory");
        }
        else
        {
             SceneManagerScript.sceneManagerScript.loadLevelSelection(currentStoryNumber);
        }

    }

    IEnumerator loadStory()
    {
        yield return new WaitForSeconds(1.2f);
        SceneManagerScript.sceneManagerScript.loadLevelSelection(currentStoryNumber);
    }

    void UnlockStatus()
    {
            if (GameConstants.checkIfStoryUnlocked(currentStoryNumber))
            {
                ChapterStatus.SetActive(true);
                CoinStatus.SetActive(false);
            }
            else
            {
                CoinTextValue.text = DynamicConstants.CoinsForUnlockingStory(currentStoryNumber).ToString();
                CoinStatus.SetActive(true);
                ChapterStatus.SetActive(false);
            }
        
    }

    private void ChapterPlayedStatus()
    {
            if (currentStoryNumber != 7)
            {
                if (currentStoryNumber == 1)
                {
                    int cChapter = P31Prefs.getInt("currentChapterNumber", 1);
                    // Debug.Log("currentChapterNumber>>>>>> " + cChapter);

                    if (P31Prefs.getBool("isStoryCompleted"))
                    {
                        if (cChapter == 1)
                        {
                            chapterTextStatus.text = "30/30";
                        }
                        else
                        {
                            chapterTextStatus.text = (cChapter - 1) + "/30";
                        }
                    }
                    else
                    {
                        chapterTextStatus.text = (cChapter - 1) + "/30";
                        if (chapterTextStatus.text == "-1" + "/30")
                        {
                            chapterTextStatus.text = "0" + "/30";
                        }
                    }
                    //Debug.Log("chapterText>>>>>> " + chapterText.text);
                }
                else
                {
                    int cChapter = P31Prefs.getInt("Story" + currentStoryNumber + "currentChapterNumber", 1);
                    if (P31Prefs.getBool("isStory" + currentStoryNumber + "Completed"))
                    {
                        if (cChapter == 1)
                        {
                            chapterTextStatus.text = "30/30";
                        }
                        else
                        {
                            chapterTextStatus.text = (cChapter - 1) + "/30";
                        }
                    }
                    else
                    {
                        Debug.Log("COMPLETED CHAPTER" + cChapter);
                        chapterTextStatus.text = (cChapter - 1) + "/30";
                        if (chapterTextStatus.text == "-1" + "/30")
                        {
                            chapterTextStatus.text = "0" + "/30";
                        }
                    }

                }
            } else if (currentStoryNumber == 7)
        {
           // if (currentButtonNumber == buttonnumber)
         //   {
                

                int cChapter = P31Prefs.getInt("Story" + currentStoryNumber + "currentChapterNumber", 1);
                if (P31Prefs.getBool("isStory" + currentStoryNumber + "Completed"))
                {
                    if (cChapter == 1)
                    {
                        chapterTextStatus.text = "15/15";
                    }
                    else
                    {
                        chapterTextStatus.text = (cChapter - 1) + "/15";
                    }
                }
                else
                {
                    Debug.Log("COMPLETED CHAPTER" + cChapter);
                    chapterTextStatus.text = (cChapter - 1) + "/15";
                    if (chapterTextStatus.text == "-1" + "/15")
                    {
                        chapterTextStatus.text = "0" + "/15";
                    }
                }
           // }
        }
    }

}
