﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Prime31;

public class ChaptersCompletedUITextForSwitch : MonoBehaviour
{
    public Text chapterText;

    public int currentButtonNumber;

    void OnEnable()
    {
        Debug.Log("ON ENABLE" + currentButtonNumber);
        chapterText = GetComponent<Text>();
        Messenger.AddListener<int, int>("DynamicChapterStoryNumber", changeStatus);
        Messenger.MarkAsPermanent("DynamicChapterStoryNumber");
        

    }

    void OnDisable()
    {
        Messenger.RemoveListener<int, int>("DynamicChapterStoryNumber", changeStatus);
    }

    void changeStatus(int currentStoryNumber, int buttonnumber)
    {
        // Debug.Log("currentStoryNumber>>>>>> " + currentButtonNumber + "  " + buttonnumber);
         Debug.Log("currentStoryNumber<<<<<" + currentStoryNumber);
        if (currentStoryNumber == 7)
        {
             Debug.Log("currentStoryNumber2242<<<<<" + currentStoryNumber + " " +  currentButtonNumber + " " + buttonnumber);
           // if (currentButtonNumber == buttonnumber)
         //   {
                

                int cChapter = P31Prefs.getInt("Story" + currentStoryNumber + "currentChapterNumber", 1);
                if (P31Prefs.getBool("isStory" + currentStoryNumber + "Completed"))
                {
                    if (cChapter == 1)
                    {
                        chapterText.text = "15/15";
                    }
                    else
                    {
                        chapterText.text = (cChapter - 1) + "/15";
                    }
                }
                else
                {
                    Debug.Log("COMPLETED CHAPTER" + cChapter);
                    chapterText.text = (cChapter - 1) + "/15";
                    if (chapterText.text == "-1" + "/15")
                    {
                        chapterText.text = "0" + "/15";
                    }
                }
           // }
        }
    }
}
