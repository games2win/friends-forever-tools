﻿using UnityEngine;
using System.Collections;

public class TutorialScript : MonoBehaviour {

	public GameObject tutorialPanel;

	public GameObject part0;
	public GameObject part1;
	public GameObject part2;
	public GameObject part3;
	public GameObject part4;


	public void showTutorial(int num)
	{
		switch (num) {
		case 0:
			part0.SetActive (false);
			part1.SetActive (true);
			break;
		case 1:
			part1.SetActive (false);
			part2.SetActive (true);
			break;
		case 2:
			part2.SetActive (false);
			part3.SetActive (true);
			break;
		case 3:
			part3.SetActive (false);
			part4.SetActive (true);
			break;
		case 4:
			G2WBackButtonHelper.isTutorialOn = false;
			tutorialPanel.SetActive (false);
			Messenger.Broadcast<string> (MessengerConstants.TUTORIAL_FINISHED, "done");
			break;
		}
	}
}
