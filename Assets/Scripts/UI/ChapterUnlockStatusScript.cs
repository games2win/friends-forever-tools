﻿using UnityEngine;
using UnityEngine.UI;

public class ChapterUnlockStatusScript : MonoBehaviour
{

    public GameObject chapterStatusObject;

    public GameObject unlockCoinObject;

    public Text unlockCoinValue;

    public int currentButtonNumber;

    // Use this for initialization
    void OnEnable()
    {
        Messenger.AddListener<int,int>("DynamicChapterStoryNumber", changeStatus);
        Messenger.MarkAsPermanent("DynamicChapterStoryNumber");


    }

    void OnDisable()
    {
        Messenger.RemoveListener<int,int>("DynamicChapterStoryNumber", changeStatus);
    }

    void changeStatus(int storyNumber, int buttonnumber)
    {
       
        if (currentButtonNumber == buttonnumber)
        {
             Debug.Log("CURRENT UNLOCK STATUS" + currentButtonNumber + " " + buttonnumber + " " + storyNumber);
            if (GameConstants.checkIfStoryUnlocked(storyNumber))
            {
                chapterStatusObject.SetActive(true);
                unlockCoinObject.SetActive(false);
            }
            else
            {
                unlockCoinValue.text = DynamicConstants.CoinsForUnlockingStory(storyNumber).ToString();
                unlockCoinObject.SetActive(true);
                chapterStatusObject.SetActive(false);
            }
        }
    }


}
