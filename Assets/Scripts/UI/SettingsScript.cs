﻿using UnityEngine;
using System.Collections;

public class SettingsScript : MonoBehaviour
{

    public GameObject soundOnObject;

    public GameObject soundOffObject;

    public void Start()
    {
        if (PlayerPrefs.GetInt("SoundStatus") == 1)
        {
            soundOnObject.SetActive(true);
            soundOffObject.SetActive(false);
        }
        else if (PlayerPrefs.GetInt("SoundStatus") == 0)
        {
            soundOnObject.SetActive(false);
            soundOffObject.SetActive(true);
        }
    }

    public void showAchievements()
    {
        Debug.Log("INSIDE ACHIEVEMENTS");
        if (GameConstants.isNetConnected())
        {
            GameServicesHelper.showAchievements();
        }
    }

    public void moreGames()
    {
        if (GameConstants.isNetConnected())
        {
            //FlurryHelper.LogEvent ("Share - Whatsapp");
            /*OLD : Application.OpenURL ("whatsapp://send?text=This game has driven me nuts. Its Rockin’ MAN!Download World Cup Cricket Champs-2015 from the Play Store for FREE and am sure you'll also forget everything else in this world! bit.ly/wccc_andr");*/
            Application.OpenURL("http://www.games2win.com/?utm_source=game&utm_medium=friends_forever_android&utm_campaign=android_more_games");
        }
    }

     public void privacyPolicy()
    {
        if (GameConstants.isNetConnected())
        {
            //FlurryHelper.LogEvent ("Share - Whatsapp");
            /*OLD : Application.OpenURL ("whatsapp://send?text=This game has driven me nuts. Its Rockin’ MAN!Download World Cup Cricket Champs-2015 from the Play Store for FREE and am sure you'll also forget everything else in this world! bit.ly/wccc_andr");*/
            Application.OpenURL("http://www.games2win.com/corporate/privacy-policy-mobile.asp");
        }
    }


    public void shareGame()
    {
        if (GameConstants.isNetConnected())
        {
            //FlurryHelper.LogEvent ("Share - Whatsapp");
            /*OLD : Application.OpenURL ("whatsapp://send?text=This game has driven me nuts. Its Rockin’ MAN!Download World Cup Cricket Champs-2015 from the Play Store for FREE and am sure you'll also forget everything else in this world! bit.ly/wccc_andr");*/
            if (GameConstants.currentLanguage == "French")
            {
#if UNITY_IOS
			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
#else
                Application.OpenURL("whatsapp://send?text=Vivez une aventure délirante avec les Friends Forever où vous êtes la Star ! Choisissez votre chemin et construisez votre histoire. Téléchargez les Friends Forever à partir de Play Store ici bit.ly/ffever_andr");
#endif
            }
            else if (GameConstants.currentLanguage == "Italian")
            {
#if UNITY_IOS
			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
#else
                Application.OpenURL("whatsapp://send?text=Vivi un'incredibile avventura con Friends Forever, in cui tu sei la stella! Scegli il tuo percorso e costruisci la tua storia. Scarica Friends Forever dal Play store bit.ly/ffever_andr");
#endif
            }
            else if (GameConstants.currentLanguage == "German")
            {
#if UNITY_IOS
			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
#else
                Application.OpenURL("whatsapp://send?text=Erlebe verrücktes Abenteuer mit Friends Forever, indem du der Star bist! Wähle deinen Weg und erschaffe deine Geschichte. Lade Friends Forever aus dem Play Store hier herunter bit.ly/ffever_andr");
#endif
            }
            else if (GameConstants.currentLanguage == "Spanish")
            {
#if UNITY_IOS
			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
#else
                Application.OpenURL("whatsapp://send?text=¡Vive una loca aventura con Friends Forever, donde tú eres la estrella! Elige tu camino y construye tu historia. Descarga Friends Forever en Play store aquí bit.ly/ffever_andr");
#endif
            }
            else if (GameConstants.currentLanguage == "Portuguese")
            {
#if UNITY_IOS
			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
#else
                Application.OpenURL("whatsapp://send?text=Vive uma aventura excitante com o Friends Forever, onde tu és a estrela! Escolhe o teu caminho e cria a tua história. Transfere o Friends Forever desta loja Play bit.ly/ffever_andr");
#endif
            }
            else
            {
#if UNITY_IOS
			Application.OpenURL ("whatsapp://send?text=Live%20a%20crazy%20adventure%20with%20Friends%20Forever%2C%20where%20you%20are%20the%20star!%20Choose%20your%20path%20and%20build%20your%20story.%20Download%20Friends%20Forever%20from%20the%20App%20store%20here%20bit.ly%2Ffforever_ios");
#else
                Application.OpenURL("whatsapp://send?text=Live a crazy adventure with Friends Forever, where you are the star! Choose your path and build your story. Download Friends Forever from the Play store here bit.ly/ffever_andr");
#endif
            }
        }
    }

    public void changeSoundStatus()
    {
        if (PlayerPrefs.GetInt("SoundStatus") == 1)
        {
            PlayerPrefs.SetInt("SoundStatus", 0);
            soundOnObject.SetActive(false);
            soundOffObject.SetActive(true);
            Messenger.Broadcast<string>(MessengerConstants.SOUND_CHANGED, "Off");
            G2WGAHelper.LogEvent("Music OFF");
        }
        else if (PlayerPrefs.GetInt("SoundStatus") == 0)
        {
            PlayerPrefs.SetInt("SoundStatus", 1);
            soundOnObject.SetActive(true);
            soundOffObject.SetActive(false);
            Messenger.Broadcast<string>(MessengerConstants.SOUND_CHANGED, "On");
            G2WGAHelper.LogEvent("Music ON");
        }
    }
}
