﻿using UnityEngine;
using System.Collections;
using Prime31;

public class StoreScript : MonoBehaviour {

	public void BuyCoinPack_1 () {
		#if UNITY_EDITOR
		StoreConstants.totalCoins += 100;
		#endif
		G2WGAHelper.LogEvent ("Get Coin Pack 1","AAID",G2WAdHelper.AD_ID,1);
        G2WGAHelper.LogRichEvent("Get Coin Pack 1");
        if (GameConstants.isNetConnected()) {
			IAP.purchaseConsumableProduct (GameConstants.IAP_COINPACK_1, (didSucceed, error) => {
				Debug.Log ("Purchasing: " + GameConstants.IAP_COINPACK_1);

				if (!didSucceed && (error != "" || string.IsNullOrEmpty (error))) {
					Debug.Log ("Purchase Error: " + error);
				} else {
				//	GoogleIAB.consumeProduct(GameConstants.IAP_COINPACK_1);
					if (StoreConstants.totalCoins < DynamicConstants.firstTimeHintPrice) {
						G2WGAHelper.LogEvent ("Coins Exhausted - Video", GameConstants.getStoryName()+": HO " + GameConstants.currentChapterNumber.ToString (),"CoinPack1", 1);
					}
					StoreConstants.totalCoins += StoreConstants.CoinPack_1;
					G2WGAHelper.LogEvent ("Buy Coin Pack 1","Gameplay Count",GameConstants.totalGameplays.ToString(),1);
					G2WGAHelper.LogEvent ("Buy Coin Pack 1", GameConstants.getStoryName() , "HO " + GameConstants.currentChapterNumber.ToString (), 1);
					 G2WGAHelper.LogRichEvent("Buy Coin Pack 1");

				}
			});
		} 
	}

	public void BuyCoinPack_2 () {
		G2WGAHelper.LogEvent ("Get Coin Pack 2","AAID",G2WAdHelper.AD_ID,1);
		 G2WGAHelper.LogRichEvent("Get Coin Pack 2");
		if (GameConstants.isNetConnected()) {
			IAP.purchaseConsumableProduct (GameConstants.IAP_COINPACK_2, (didSucceed, error) => {
				Debug.Log ("Purchasing: " + GameConstants.IAP_COINPACK_2);

				if (!didSucceed && (error != "" || string.IsNullOrEmpty (error))) {
					Debug.Log ("Purchase Error: " + error);
				} else {
					Debug.Log ("Purchase Succeed");
					//GoogleIAB.consumeProduct(GameConstants.IAP_COINPACK_2);
					if (StoreConstants.totalCoins < DynamicConstants.firstTimeHintPrice) {
						G2WGAHelper.LogEvent ("Coins Exhausted - Video", GameConstants.getStoryName()+": HO " + GameConstants.currentChapterNumber.ToString (),"CoinPack2", 1);
					}
					StoreConstants.totalCoins += StoreConstants.CoinPack_2;
					G2WGAHelper.LogEvent ("Buy Coin Pack 2","Gameplay Count",GameConstants.totalGameplays.ToString(),1);
					G2WGAHelper.LogEvent ("Buy Coin Pack 2", GameConstants.getStoryName() , "HO " + GameConstants.currentChapterNumber.ToString (), 1);
					G2WGAHelper.LogRichEvent("Buy Coin Pack 2");

				}
			});
		} else {
			//No Internet Popup
		}
	}

	public void BuyCoinPack_3 () {
		G2WGAHelper.LogEvent ("Get Coin Pack 3","AAID",G2WAdHelper.AD_ID,1);
		 G2WGAHelper.LogRichEvent("Get Coin Pack 3");
		if (GameConstants.isNetConnected()) {
			IAP.purchaseConsumableProduct (GameConstants.IAP_COINPACK_3, (didSucceed, error) => {
				Debug.Log ("Purchasing: " + GameConstants.IAP_COINPACK_3);

				if (!didSucceed && (error != "" || string.IsNullOrEmpty (error))) {
					Debug.Log ("Purchase Error: " + error);
				} else {
					Debug.Log ("Purchase Succeed");
					//GoogleIAB.consumeProduct(GameConstants.IAP_COINPACK_3);
					if (StoreConstants.totalCoins < DynamicConstants.firstTimeHintPrice) {
						G2WGAHelper.LogEvent ("Coins Exhausted - Video", GameConstants.getStoryName()+": HO " + GameConstants.currentChapterNumber.ToString (),"CoinPack3", 1);
					}
					StoreConstants.totalCoins += StoreConstants.CoinPack_3;
					G2WGAHelper.LogEvent ("Buy Coin Pack 3","Gameplay Count",GameConstants.totalGameplays.ToString(),1);
					G2WGAHelper.LogEvent ("Buy Coin Pack 3", GameConstants.getStoryName() , "HO " + GameConstants.currentChapterNumber.ToString (), 1);
					 G2WGAHelper.LogRichEvent("Buy Coin Pack 3");

				}
			});
		} else {
			//No Internet Popup
		}
	}

	public void BuyMovePack_1 () {
		G2WGAHelper.LogEvent ("Get Move Pack 1","AAID",G2WAdHelper.AD_ID,1);
		 G2WGAHelper.LogRichEvent("Get Move Pack 1");
		if (GameConstants.isNetConnected()) {
			IAP.purchaseConsumableProduct (GameConstants.IAP_MOVEPACK_1, (didSucceed, error) => {
				Debug.Log ("Purchasing: " + GameConstants.IAP_MOVEPACK_1);

				if (!didSucceed && (error != "" || string.IsNullOrEmpty (error))) {
					Debug.Log ("Purchase Error: " + error);
				} else {
					Debug.Log ("Purchase Succeed");
				//	GoogleIAB.consumeProduct(GameConstants.IAP_MOVEPACK_1);
					if (StoreConstants.totalMoves <= 0) {
						G2WGAHelper.LogEvent ("Moves Exhausted - Video", GameConstants.getStoryName()+": Chapter " + GameConstants.currentChapterNumber.ToString (),"MovePack1", 1);
					}
					StoreConstants.totalMoves += StoreConstants.MovePack_1;
					G2WGAHelper.LogEvent ("Buy Move Pack 1","Gameplay Count",GameConstants.totalGameplays.ToString(),1);
					G2WGAHelper.LogEvent ("Buy Move Pack 1", GameConstants.getStoryName() , "Chapter " + GameConstants.currentChapterNumber.ToString (), 1);
					 G2WGAHelper.LogRichEvent("Buy Move Pack 1");

				}
			});
		} else {
			//No Internet Popup
		}
	}

	public void BuyMovePack_2 () {
		G2WGAHelper.LogEvent ("Get Move Pack 2","AAID",G2WAdHelper.AD_ID,1);
		 G2WGAHelper.LogRichEvent("Get Move Pack 2");
		if (GameConstants.isNetConnected()) {
			IAP.purchaseConsumableProduct (GameConstants.IAP_MOVEPACK_2, (didSucceed, error) => {
				Debug.Log ("Purchasing: " + GameConstants.IAP_MOVEPACK_2);

				if (!didSucceed && (error != "" || string.IsNullOrEmpty (error))) {
					Debug.Log ("Purchase Error: " + error);
				} else {
					Debug.Log ("Purchase Succeed");
					//GoogleIAB.consumeProduct(GameConstants.IAP_MOVEPACK_2);
					if (StoreConstants.totalMoves <= 0) {
						G2WGAHelper.LogEvent ("Moves Exhausted - Video", GameConstants.getStoryName()+": Chapter " + GameConstants.currentChapterNumber.ToString (),"MovePack2", 1);
					}
					StoreConstants.totalMoves += StoreConstants.MovePack_2;
					G2WGAHelper.LogEvent ("Buy Move Pack 2","Gameplay Count",GameConstants.totalGameplays.ToString(),1);
					G2WGAHelper.LogEvent ("Buy Move Pack 2", GameConstants.getStoryName() , "Chapter " + GameConstants.currentChapterNumber.ToString (), 1);
					 G2WGAHelper.LogRichEvent("Buy Move Pack 2");

				}
			});
		} else {
			//No Internet Popup
		}
	}

	public void BuyMovePack_3 () {
		G2WGAHelper.LogEvent ("Get Move Pack 3","AAID",G2WAdHelper.AD_ID,1);
		 G2WGAHelper.LogRichEvent("Get Move Pack 3");
		if (GameConstants.isNetConnected()) {
			IAP.purchaseConsumableProduct (GameConstants.IAP_MOVEPACK_3, (didSucceed, error) => {
				Debug.Log ("Purchasing: " + GameConstants.IAP_MOVEPACK_3);

				if (!didSucceed && (error != "" || string.IsNullOrEmpty (error))) {
					Debug.Log ("Purchase Error: " + error);
				} else {
					Debug.Log ("Purchase Succeed");
					//GoogleIAB.consumeProduct(GameConstants.IAP_MOVEPACK_3);
					if (StoreConstants.totalMoves <= 0) {
						G2WGAHelper.LogEvent ("Moves Exhausted - Video", GameConstants.getStoryName()+": Chapter " + GameConstants.currentChapterNumber.ToString (),"MovePack3", 1);
					}
					StoreConstants.totalMoves += StoreConstants.MovePack_3;
					G2WGAHelper.LogEvent ("Buy Move Pack 3","Gameplay Count",GameConstants.totalGameplays.ToString(),1);
					G2WGAHelper.LogEvent ("Buy Move Pack 3", GameConstants.getStoryName() , "Chapter " + GameConstants.currentChapterNumber.ToString (), 1);
					 G2WGAHelper.LogRichEvent("Buy Move Pack 3");

				}
			});
		} else {
			//No Internet Popup
		}
	}

	public void watchAVideoCoins()
	{
		//if (GameConstants.isNetConnected ()) {
			G2WAdHelper.ShowRewardedVideo (GameConstants.RVP_StoreCoins,"Store");
			GameServicesHelper.UnlockAchievements(AchievementConstants.AVID_OBSERVER);
		//}
	}

	public void watchAVideoMoves()
	{
		//if (GameConstants.isNetConnected ()) {
			G2WAdHelper.ShowRewardedVideo (GameConstants.RVP_StoreMoves,"Store");
			GameServicesHelper.UnlockAchievements(AchievementConstants.AVID_OBSERVER);
	//	}
	}

	void OnDestroy()
	{
		Debug.Log ("BACK DESTROYED");
		Messenger.Broadcast<string> (MessengerConstants.DISABLE_BACK_PANEL, "Store");
	}


}
