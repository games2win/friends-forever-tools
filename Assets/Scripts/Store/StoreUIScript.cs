﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Fungus;
using Prime31;

public class StoreUIScript : MonoBehaviour
{

	public GameObject movesScreenPanel;

	public GameObject coinsScreenPanel;

	public GameObject storeArrowObject;
	public GameObject coinArrowTransform;
	public GameObject moveArrowTransform;

	public Text movesText;
	public Text coinsText;

	public Text intText;

	public Text coins1AmountText;
	public Text coins2AmountText;
	public Text coins3AmountText;
	public Text coins4AmountText;

	public Text moves1AmountText;
	public Text moves2AmountText;
	public Text moves3AmountText;
	public Text moves4AmountText;

	public Text coins1PriceText;
	public Text coins2PriceText;
	public Text coins3PriceText;

	public Text moves1PriceText;
	public Text moves2PriceText;
	public Text moves3PriceText;



	static StoreUIScript _storeUIScript;

	public static StoreUIScript  storeUIScript {
		get { return _storeUIScript; }
	}

	public GameObject rewardedMoves, rewardedCoins;

	void Start ()
	{
		_storeUIScript = this;
		GameConstants.isStoreLoaded = true;
		#if UNITY_IOS
		if (iCloudBinding.getUbiquityIdentityToken () == null) {
			intText.gameObject.SetActive (true);
		} else {
			intText.gameObject.SetActive (false);
		}
		#else 
		intText.gameObject.SetActive (false);
		#endif

		// Opens either Coins or Moves screen according to the option selected from the previous screen.
		if (StoreConstants.showCoinScreen) {
			openPanel ("coins");
		} else {
			openPanel ("moves");
		}
		//Sets the Coin and Move Amounts dynamically.
		coins1AmountText.text = StoreConstants.CoinPack_Free.ToString ();
		coins2AmountText.text = StoreConstants.CoinPack_1.ToString ();
		coins3AmountText.text = StoreConstants.CoinPack_2.ToString ();
		coins4AmountText.text = StoreConstants.CoinPack_3.ToString ();

		moves1AmountText.text = StoreConstants.MovePack_Free.ToString ();
		moves2AmountText.text = StoreConstants.MovePack_1.ToString ();
		moves3AmountText.text = StoreConstants.MovePack_2.ToString ();
		moves4AmountText.text = StoreConstants.MovePack_3.ToString ();
		//

		//Sets the Coin and Move Prices dynamically.
		coins1PriceText.text = G2WIAPHelper.DynamicPrices [0];
		coins2PriceText.text = G2WIAPHelper.DynamicPrices [1];
		coins3PriceText.text = G2WIAPHelper.DynamicPrices [2];

		moves1PriceText.text = G2WIAPHelper.DynamicPrices [3];
		moves2PriceText.text = G2WIAPHelper.DynamicPrices [4];
		moves3PriceText.text = G2WIAPHelper.DynamicPrices [5];
		//
	}

	public void openPanel (string panelname)
	{
		Debug.Log ("PANEL CHANGE" + panelname + coinsText.color);
		if (panelname == "coins") {
			animateArrow (coinArrowTransform);
			movesScreenPanel.SetActive (false);
			coinsScreenPanel.SetActive (true);
			movesText.color = new Color (0.632f, 0.265f, 0.537f, 1.000f);
			coinsText.color = Color.white;

		} else {
			animateArrow (moveArrowTransform);
			movesScreenPanel.SetActive (true);
			coinsScreenPanel.SetActive (false);
			coinsText.color = new Color (0.632f, 0.265f, 0.537f, 1.000f);
			movesText.color = Color.white;

		}
	}

	void animateArrow (GameObject go)
	{
		Hashtable tweenParams = new Hashtable ();

		tweenParams.Add ("position", go.transform.position);
		tweenParams.Add ("time", 2f);
		tweenParams.Add ("ignoretimescale", true);
		iTween.MoveTo (storeArrowObject, tweenParams);
		//iTween.MoveTo (storeArrowObject,,3f);
	}

	void changeColor (Text newtext)
	{
		Hashtable tweenParams = new Hashtable ();

		tweenParams.Add ("Color", Color.white);
		tweenParams.Add ("time", 2f);
		tweenParams.Add ("ignoretimescale", true);
		iTween.ColorTo (newtext.gameObject, tweenParams);
	}

	public void backButtonHandler ()
	{
		Messenger.Broadcast<string> (MessengerConstants.BACK_BUTTON_TAPPED, "Store");
	}



}
