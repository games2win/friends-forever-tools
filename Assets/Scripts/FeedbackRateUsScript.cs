﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackRateUsScript : MonoBehaviour {

	public static FeedbackRateUsScript feedbackRateUsScript;

	private string uploadurl = "https://s02.appucino.com/surveys/submit";

    public InputField feedbackText;

	public GameObject feedbackRateUsPanel;

    public int rateUsRating = 5;

	 public GameObject feedbackParticleEffect;

	 	public GameObject feedbackPopup;

		public string langToUse = "EN";

		public Button submitButton;


	   void OnEnable ()
	{
		Messenger.AddListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, HideFeedBackRateUsPanel);
		Messenger.MarkAsPermanent (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY);
	}

	void OnDisable ()
	{
		Messenger.RemoveListener<string> (MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, HideFeedBackRateUsPanel);
	}

    void Awake ()
	{
		feedbackRateUsScript = this;

		submitButton.interactable = false;
		print ("good here");
	}

    public void selectedValue(int value)
    {
        rateUsRating = value;
		submitButton.interactable = true;
    }

    public void feedback()
    {
        //FlurryHelper.LogEvent ("Feedback");
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
#if UNITY_IOS //pragma
			sendfeedbackForTranslationRating (feedbackText.text, "friends_forever_ios");
#elif UNITY_ANDROID
            sendfeedbackForTranslationRating(feedbackText.text, "friends_forever_android");
#endif
		feedbackPopup.SetActive (true);
        }
    }

    public string selectLanguage()
    { 
		
                   
                  switch (Application.systemLanguage){
                     case SystemLanguage.French : langToUse = "FR";break;
                     case SystemLanguage.German : langToUse = "DE";break;
                    case SystemLanguage.Italian : langToUse = "IT";break;
                    case SystemLanguage.Portuguese : langToUse = "PT";break;
                    case SystemLanguage.Spanish : langToUse = "ES";break;
                    case SystemLanguage.English : langToUse = "EN";break;
                }
        return langToUse;
    }

    public void sendfeedbackForTranslationRating (/*string emailid ,*/  string feedback, string gameid)
	{
		WWWForm _form = new WWWForm ();
		_form.AddField ("game_id", gameid);
		_form.AddField ("survey_id", GameConstants.currentStoryNumber.ToString());
		_form.AddField ("question_id", "2");
		_form.AddField ("response", rateUsRating.ToString());
		_form.AddField ("language", selectLanguage());
        if (feedbackText.text != "")
        {
            _form.AddField("extras", feedbackText.text);
        }


		Debug.Log("game_idX"+gameid);
        Debug.Log("survey_idX"+GameConstants.currentStoryNumber.ToString());
		Debug.Log("question_idX"+"2");
		Debug.Log("responseX"+rateUsRating.ToString());
		Debug.Log("languageX"+selectLanguage());
        Debug.Log("extrasX"+feedbackText.text);

        WWW w = new WWW (uploadurl, _form);
		StartCoroutine (startupdating (w));
	}

	IEnumerator startupdating (WWW www)
	{
		yield return www;
		
		// check for errors
		if (www.error == null) {
			Debug.Log ("WWW Ok!: " + www.url);

				GameConstants.askedStoryFeedbackRateUsNumber = 1;	
				G2WGAHelper.LogEvent("Rate Story", GameConstants.currentStoryNumber.ToString(), rateUsRating.ToString(), 1);	
        } else {
			Debug.Log ("WWW Error: " + www.error);
		}    
		 HideFeedBackRateUsPanel("Feedback");
		//G2WBackButtonHelper.isFeedBackOn = false;
		//feedbackPanel.SetActive (false);
	}

	public void HideFeedBackRateUsPanel (string s = "")
	{
		//FlurryHelper.LogEvent( "Feedback - Later" );
		//if (AdsCustom.ads != null)
		//	AdsCustom.ads.ShowBanner2 ();
		if (s == "Feedback") {
			G2WBackButtonHelper.isFeedBackOn = false;
			feedbackText.text = "";

			feedbackRateUsPanel.SetActive (false);
            feedbackParticleEffect.SetActive(true);
        }
	}

    public void skipRating(string source)
    { 
		if(source == "language")
		{
 G2WGAHelper.LogEvent("Rate Language - Skip", GameConstants.currentStoryNumber.ToString(), "Seen", 1);
		}
        else
        {
            G2WGAHelper.LogEvent("Rate Story - Skip", GameConstants.currentStoryNumber.ToString(), "Seen", 1);
        }
    }
}
