﻿using UnityEngine;
using System.Collections;
using Prime31;

public class G2WPermissionHelper : MonoBehaviour
{

	// Android permission popup
	public GameObject permissionPopup;

	// Use this for initialization
	void Start ()
	{
		#if UNITY_ANDROID
        askForPermissionExplicit();
		#endif
		G2WGAHelper.LogEvent ("AAID", "ids", G2WAdHelper.AD_ID, 1);
	}
	
	#if UNITY_ANDROID
	
    



#region LOCATION

	
	
    public void askForPermissionExplicit()
    {
        if (GameConstants.isShowingLocationPermission)
        {
            Debug.Log("ASK FOR PERMISSION AT START" + GameConstants.isShowingLocationPermission);
            AskLocationPermission();
            GameConstants.isShowingLocationPermission = false;
        }
    }

    public void ShowPermissionPopup()
    {
        string locationPermission = "android.permission.ACCESS_COARSE_LOCATION";
        if (!EtceteraAndroid.checkSelfPermission(locationPermission) && !PlayerPrefs.HasKey("LocationPermissionAsked"))
        {
            PlayerPrefs.SetInt("LocationPermissionAsked", 1);
            //Debug.Log("-------------------------------------------- permission popup ");
            G2WBackButtonHelper.ispopupOn = true;
            permissionPopup.SetActive(true);
            G2WGAHelper.LogEvent("Ad Permission", "Seen");
        }
        else if (!EtceteraAndroid.checkSelfPermission(locationPermission) && EtceteraAndroid.shouldShowRequestPermissionRationale(locationPermission))
        {
            Debug.Log("-------------------------------------------- NOT permission popup ");
            G2WBackButtonHelper.ispopupOn = true;
            permissionPopup.SetActive(true);
            G2WGAHelper.LogEvent("Ad Permission", "Seen");
        }
        else
        {
            //Debug.Log("-------------------------------------------- TAP TO CONTINUE");
            //TapToContinue ();
        }

        //TapToContinue();

        //Intro.instance.Play ();
    }

    public void RequestPermission()
    {
        G2WBackButtonHelper.ispopupOn = false;
        permissionPopup.SetActive(false);
        //	TapToContinue ();
        Invoke("ShowNativePermissionPopup", 0.5f);

    }

    public void ShowNativePermissionPopup()
    {

        Debug.Log("SHOW NATIVE PERMISSION");
        EtceteraAndroid.requestPermissions(new string[] { "android.permission.ACCESS_COARSE_LOCATION" }, 575757);

        //EtceteraAndroidManager.onRequestPermissionsResultEvent += PermissionRequestResult;
    }




    void AskLocationPermission()
    {
        GameConstants.isShowingLocationPermission = true;
        string[] permissions = new string[] { "android.permission.ACCESS_COARSE_LOCATION" };
        Debug.Log("Location Rationale: " + EtceteraAndroid.shouldShowRequestPermissionRationale(permissions[0]) + " " + EtceteraAndroid.checkSelfPermission(permissions[0]));
        //GAHelper.LogEvent ("Ad Permission - Popup", "Ad Permission Popup Shown");
        if ((!EtceteraAndroid.checkSelfPermission(permissions[0]) && !GameConstants.askLocationPermission && !EtceteraAndroid.shouldShowRequestPermissionRationale(permissions[0])))
        {
            GameConstants.askLocationPermission = true;
            G2WBackButtonHelper.ispopupOn = true;
            //PermissionsPopupScript.popupName = PermissionPopups.Location;
            permissionPopup.SetActive(true);
            G2WGAHelper.LogEvent("Ad Permission", "Seen");
        }
        else if ((!EtceteraAndroid.checkSelfPermission(permissions[0]) && GameConstants.askLocationPermission && EtceteraAndroid.shouldShowRequestPermissionRationale(permissions[0])))
        {
            //PermissionsPopupScript.popupName = PermissionPopups.Location;
            G2WBackButtonHelper.ispopupOn = true;
            permissionPopup.SetActive(true);
            G2WGAHelper.LogEvent("Ad Permission", "Seen");
        }
        else if (!EtceteraAndroid.checkSelfPermission(permissions[0]) && !GameConstants.askLocationPermission && EtceteraAndroid.shouldShowRequestPermissionRationale(permissions[0]))
        {
            //PermissionsPopupScript.popupName = PermissionPopups.Location;
            G2WBackButtonHelper.ispopupOn = true;
            permissionPopup.SetActive(true);
            G2WGAHelper.LogEvent("Ad Permission", "Seen");
        }
    }

    



#endregion

	
	
#endif
}
