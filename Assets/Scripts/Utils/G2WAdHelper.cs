﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
using System;
using Prime31;
using UnityEngine.iOS;

public class G2WAdHelper : MonoBehaviour
{

    private static G2WAdHelper instance = null;
    public static float interstitialTimer = 0f;
    private string advertisingID;
    private bool limitTracking = false;

    public static string AD_ID;

    // USED TO DEBUG BANNER HIDE AND SHOW STATES IN EDITOR
    public static String editorShow = "NO_AD";

#if UNITY_ANDROID
    private Dictionary<string, string[]> _bannerDict = new Dictionary<string, string[]>() {

        { "MoPub", new string[] { GameConstants.mopubBannerPhoneID, GameConstants.mopubBannerTabID, GameConstants.mopubExitAdPhoneID, GameConstants.mopubExitAdTabID } },
    };

     private Dictionary<string, string[]> _interstitialDict = new Dictionary<string, string[]> () {

    	{ "MoPub", new string[] { GameConstants.mopubInterstitialPhoneID, GameConstants.mopubInterstitialTabID } },

     };





#elif UNITY_IPHONE
	private Dictionary<string, string[]> _bannerDict = new Dictionary<string, string[]> () {
		{ "MoPub", new string[] { GameConstants.mopubBannerPhoneID, GameConstants.mopubBannerTabID } },
	};

	private Dictionary<string, string[]> _interstitialDict = new Dictionary<string, string[]> () {
		{ "MoPub", new string[] { GameConstants.mopubInterstitialPhoneID, GameConstants.mopubInterstitialTabID } },
	};

#endif

    void OnEnable()
    {
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
		IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent; 
		IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
		IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
		IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
		IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent; 
		IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

		//IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
		//IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;        
		//IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent; 
		//IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent; 
		//IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
		
		//IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;


        //Empty listeners
       // IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;

        // IronSourceEvents.onRewardedVideoInitSuccessEvent += RewardedVideoInitSuccessEvent;

        // IronSourceEvents.onRewardedVideoInitFailEvent += RewardedVideoInitFailEvent;

        // IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;

        // IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;

        // IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;

        // IronSourceEvents.onVideoAvailabilityChangedEvent += VideoAvailabilityChangedEvent;

        // IronSourceEvents.onVideoStartEvent += VideoStartEvent;

        // IronSourceEvents.onVideoEndEvent += VideoEndEvent;

        // IronSourceEvents.onInterstitialInitSuccessEvent += InterstitialInitSuccessEvent;
        // IronSourceEvents.onInterstitialInitFailedEvent += InterstitialInitFailEvent;
        // IronSourceEvents.onInterstitialReadyEvent += InterstitialReadyEvent;
        // IronSourceEvents.onInterstitialLoadFailedEvent += InterstitialLoadFailedEvent;
        // IronSourceEvents.onInterstitialShowSuccessEvent += InterstitialShowSuccessEvent;
        // IronSourceEvents.onInterstitialShowFailedEvent += InterstitialShowFailEvent;
        // IronSourceEvents.onInterstitialClickEvent += InterstitialAdClickedEvent;
        // IronSourceEvents.onInterstitialOpenEvent += InterstitialAdOpenedEvent;
        // IronSourceEvents.onInterstitialCloseEvent += InterstitialAdClosedEvent;
        Debug.Log("OnEnable.................................");
    }

    void OnDisable()
    {
        Debug.Log("ondisable.................................");
        // IronSourceEvents.onRewardedVideoInitSuccessEvent -= RewardedVideoInitSuccessEvent;

        // IronSourceEvents.onRewardedVideoInitFailEvent -= RewardedVideoInitFailEvent;

        // IronSourceEvents.onRewardedVideoAdOpenedEvent -= RewardedVideoAdOpenedEvent;

        // IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent;

        // IronSourceEvents.onRewardedVideoAdClosedEvent -= RewardedVideoAdClosedEvent;

        // IronSourceEvents.onVideoAvailabilityChangedEvent -= VideoAvailabilityChangedEvent;

        // IronSourceEvents.onVideoStartEvent -= VideoStartEvent;

        // IronSourceEvents.onVideoEndEvent -= VideoEndEvent;

        // IronSourceEvents.onInterstitialInitSuccessEvent -= InterstitialInitSuccessEvent;
        // IronSourceEvents.onInterstitialInitFailedEvent -= InterstitialInitFailEvent;
        // IronSourceEvents.onInterstitialReadyEvent -= InterstitialReadyEvent;
        // IronSourceEvents.onInterstitialLoadFailedEvent -= InterstitialLoadFailedEvent;
        // IronSourceEvents.onInterstitialShowSuccessEvent -= InterstitialShowSuccessEvent;
        // IronSourceEvents.onInterstitialShowFailedEvent -= InterstitialShowFailEvent;
        // IronSourceEvents.onInterstitialClickEvent -= InterstitialAdClickedEvent;
        // IronSourceEvents.onInterstitialOpenEvent -= InterstitialAdOpenedEvent;
        // IronSourceEvents.onInterstitialCloseEvent -= InterstitialAdClosedEvent;
    }

    void Awake()
    {
        if (instance != null)
        {
            Debug.Log("Deleteing AdHelper Object");
            Destroy(gameObject);
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

        Ad_ID();

        var allBannerAdUnits = new string[0];
         var allInterstitialAdUnits = new string[0];


        foreach (var bannerAdUnits in _bannerDict.Values)
        {
            allBannerAdUnits = allBannerAdUnits.Union(bannerAdUnits).ToArray();
        }

         foreach (var interstitialAdUnits in _interstitialDict.Values) {
        	allInterstitialAdUnits = allInterstitialAdUnits.Union (interstitialAdUnits).ToArray ();
         }

#if UNITY_ANDROID
        MoPub.loadBannerPluginsForAdUnits(allBannerAdUnits);
        MoPub.loadInterstitialPluginsForAdUnits(allInterstitialAdUnits);
#elif UNITY_IPHONE
		MoPub.loadPluginsForAdUnits (allBannerAdUnits);
		MoPub.loadPluginsForAdUnits (allInterstitialAdUnits);
#endif
    }

    void OnDestroy()
    {
#if UNITY_5_4 || UNITY_5_5
        SceneManager.activeSceneChanged -= OnLevelLoaded; // unsubscribe
#endif
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("HELLO");
        // if (DeviceSize.isPhone ()) {
        // 	MoPub.requestInterstitialAd (GameConstants.mopubInterstitialPhoneID);
        // } else {
        // 	MoPub.requestInterstitialAd (GameConstants.mopubInterstitialTabID);
        // }
       // PZAndroid.Init("0d4eec55362f14610a2b6a644f3d51d6","a20e6bba2f81e5bce04afd12cde708dee4352e86");

       

        //Set the unique id of your end user.
        Debug.Log("advertisingID " + advertisingID);
        string uniqueUserId = advertisingID;
        string appKey = GameConstants.IRONSOURCE_APPLICATION_KEY;
        IronSource.Agent.setUserId(uniqueUserId);

        //Init Rewarded Video
       IronSource.Agent.init (appKey, IronSourceAdUnits.REWARDED_VIDEO);

        IronSource.Agent.reportAppStarted();
        IronSource.Agent.validateIntegration();
        IronSource.Agent.setAdaptersDebug(true);
        // RequestInterstitial();
        // Invoke("RequestInterstitial",1f);
        // IronSource.Agent.loadInterstitial();

#if UNITY_5_4 || UNITY_5_5
        SceneManager.activeSceneChanged += OnLevelLoaded; // subscribe
#endif
    }

    void Ad_ID()
    {
#if UNITY_ANDROID && !UNITY_EDITOR

        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaClass client = new AndroidJavaClass("com.google.android.gms.ads.identifier.AdvertisingIdClient");
        AndroidJavaObject adInfo = client.CallStatic<AndroidJavaObject>("getAdvertisingIdInfo", currentActivity);

        advertisingID = adInfo.Call<string>("getId").ToString();
        limitTracking = (adInfo.Call<bool>("isLimitAdTrackingEnabled"));



        AD_ID = advertisingID;
#elif UNITY_IOS && !UNITY_EDITOR
		advertisingID = SystemInfo.deviceUniqueIdentifier;
		AD_ID = Device.advertisingIdentifier;
#endif
    }

    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }

    // Update is called once per frame
    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.Escape)) {
        //	if (SceneManager.GetActiveScene ().name == "IntroScene") {
        //		if (DeviceSize.isPhone ())
        //			MoPub.showBanner (GameConstants.mopubExitAdPhoneID, true);
        //		else
        //			MoPub.showBanner (GameConstants.mopubExitAdTabID, true);
        //	}
        //}
    }

#if UNITY_5_3
	void OnLevelWasLoaded(int level) {
		Debug.Log ("OnLevelWasLoaded: " + level);
		if (level == 1 || level == 4) {
			if (DeviceSize.isPhone ()) {
				MoPub.showBanner (GameConstants.mopubBannerPhoneID, false);
				if(!GameConstants.isExitGamePopupActive)
					MoPub.showBanner (GameConstants.mopubExitAdPhoneID, false);
			} else {
				MoPub.showBanner (GameConstants.mopubBannerTabID, false);
				if(!GameConstants.isExitGamePopupActive)
					MoPub.showBanner (GameConstants.mopubExitAdTabID, false);
			}
		} else {
			if (DeviceSize.isPhone ()) {
				MoPub.showBanner (GameConstants.mopubBannerPhoneID, true);
				MoPub.showBanner (GameConstants.mopubExitAdPhoneID, false);
			} else {
				MoPub.showBanner (GameConstants.mopubBannerTabID, true);
				MoPub.showBanner (GameConstants.mopubExitAdTabID, false);
			}
		} 

		if (level == 5) {
			CheckForInterstitial ();
		}
	}
#endif

#if UNITY_EDITOR

    Rect position = new Rect(0, 0, 320, 50);
    Color color = Color.red;

    void OnGUI()
    {
        if (!editorShow.Equals("NO_AD"))
        {
            DrawRectangle(position, editorShow);
        }
    }

    void DrawRectangle(Rect position, String message)
    {
        // We shouldn't draw until we are told to do so.
        if (Event.current.type != EventType.Repaint)
            return;
        GUI.Label(position, message);

        /*
		// Please assign a material that is using position and color.
		if (material == null) {
			Debug.LogError ("You have forgot to set a material.");
			return;
		}

		material.SetPass (0);

		// Optimization hint: 
		// Consider Graphics.DrawMeshNow
		GL.Color (color);
		GL.Begin (GL.QUADS);
		GL.Vertex3 (position.x, position.y, 0);
		GL.Vertex3 (position.x + position.width, position.y, 0);
		GL.Vertex3 (position.x + position.width, position.y + position.height, 0);
		GL.Vertex3 (position.x, position.y + position.height, 0);
		GL.End ();
		*/
    }

#endif

#if UNITY_5_4 || UNITY_5_5
    void OnLevelLoaded(Scene scn0, Scene scn1)
    {
        Debug.Log("Scn0 " + scn0.name);

        Debug.Log("Scn1 " + scn1.name);
        Debug.Log("GameState" + GameConstants.currentGameState);
        editorShow = "NO_AD";

        // Move banner creation to here so that it does not interfere with the GPGS login prompt @ the start of the game
        if (scn1.name == "ChapterSelectionScreen")
        {
            if (DeviceSize.isPhone())
            {
                MoPub.createBanner(GameConstants.mopubBannerPhoneID, MoPubAdPosition.TopCenter);
#if UNITY_ANDROID
                MoPub.createBanner(GameConstants.mopubExitAdPhoneID, MoPubAdPosition.TopCenter);
#endif
            }
            else
            {
                MoPub.createBanner(GameConstants.mopubBannerTabID, MoPubAdPosition.TopCenter);
#if UNITY_ANDROID
                MoPub.createBanner(GameConstants.mopubExitAdTabID, MoPubAdPosition.TopCenter);
#endif
            }
            
        }


        bool bShowBanner = false;
        bool bShowExit = false;

        if ((scn1.name == "IntroScene") && (GameConstants.isExitGamePopupActive))
        {
            bShowExit = true;
        }

        // List scenes where banner ads should NOT be shown
        if (scn1.name != "IntroScene")
        {
            bShowBanner = true;
        }

        if (DeviceSize.isPhone())
        {
            MoPub.showBanner(GameConstants.mopubBannerPhoneID, bShowBanner);
#if UNITY_ANDROID
            MoPub.showBanner(GameConstants.mopubExitAdPhoneID, bShowExit);
#endif
        }
        else
        {
            MoPub.showBanner(GameConstants.mopubBannerTabID, bShowBanner);
#if UNITY_ANDROID
            MoPub.showBanner(GameConstants.mopubExitAdTabID, bShowExit);
#endif
        }

        // For debugging only
#if UNITY_EDITOR
        if (bShowBanner)
            editorShow = "BANNER";
        if (bShowExit)
            editorShow = "EXIT";
        if (bShowExit && bShowBanner)
            editorShow = "AD CONFLICT";
#endif
        Debug.Log("Ad Debug" + editorShow);

        // To show interstitial on level over states
        if (scn1.name == "EndScene")
        {
           // CheckForInterstitialIronSource();
            CheckForInterstitial ();
        }
    }
#endif

    public static void RequestInterstitial()

    {
        Debug.Log ("CheckForInterstitial");
        			if (!GameConstants.isInterstitialRequested && !GameConstants.isInterstitialLoaded) {

        				if (Application.internetReachability != NetworkReachability.NotReachable)
        					GameConstants.isInterstitialRequested = true;

        				Debug.Log ("request interstitial");
        				IronSource.Agent.loadInterstitial ();

        			}
    }

    public static void CheckForInterstitialIronSource()
    {

        Debug.Log("CheckForInterstitial");
        if (!GameConstants.isInterstitialRequested && !GameConstants.isInterstitialLoaded)
        {

            if (Application.internetReachability != NetworkReachability.NotReachable)
                GameConstants.isInterstitialRequested = true;

            Debug.Log("request interstitial");
            IronSource.Agent.loadInterstitial();

        }
        else if (GameConstants.isInterstitialLoaded && IronSource.Agent.isInterstitialReady())
        {
            if (interstitialTimer >= GameConstants.InterstitialTimer)
            {
                Debug.Log("Show interstitial");
                IronSource.Agent.showInterstitial();
            }
            else
            {
                Debug.Log("Timer : " + interstitialTimer);
            }
        }

    }

    public static void CheckForInterstitial()
    {
        Debug.Log("MoPub : CheckForInterstitial in AdHelper");
        if (!GameConstants.isInterstitialRequested && !GameConstants.isInterstitialLoaded)
        {
            Debug.Log("MoPub : No Ad loaded");
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                Debug.Log("MoPub : So request for Ad");
                GameConstants.isInterstitialRequested = true;
            }
            MoPub.requestInterstitialAd(DeviceSize.isPhone() ? GameConstants.mopubInterstitialPhoneID : GameConstants.mopubInterstitialTabID);

        }
        else if (GameConstants.isInterstitialLoaded)
        {
            Debug.Log("MoPub : Ad Was Loaded before");
            if (interstitialTimer >= GameConstants.InterstitialTimer)
            {
                Debug.Log("MoPub : Timer over, show Interstitial");
                editorShow = "INTERSTITIAL_AD";
                MoPub.showInterstitialAd(DeviceSize.isPhone() ? GameConstants.mopubInterstitialPhoneID : GameConstants.mopubInterstitialTabID);
            }
            else
            {
                Debug.Log("MoPub : Timer not over, dont show interstitial");
                Debug.Log("MoPub: Timer : " + interstitialTimer);
            }
        }
    }

    public static void ShowRewardedVideo(string sPlacement, string location = "")
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            Debug.Log("RewardedVideo:Showing Placement" + sPlacement);
            if (IronSource.Agent.isRewardedVideoAvailable())
            {

                /* Pause sounds if necessary if (GameManager.gameManager != null)
					GameManager.gameManager.GetComponent<AudioSource>().enabled = false;
*/
                IronSource.Agent.showRewardedVideo(sPlacement);
            }
            else
            {
#if UNITY_ANDROID
                //TODO 
                EtceteraAndroid.showAlert("No Video Available.", "Please Try Again Later.", "OK");
                Debug.Log("REWARDED : NO VIDEO AVAILABLE");
#elif UNITY_IOS
				var buttons = new string[] { "OK" };
				EtceteraBinding.showAlertWithTitleMessageAndButtons ("No Video Available.", "Please Try Again Later.", buttons);
#endif
                miscGAFunction(sPlacement, location, true);
            }
        }
        else
        {
#if UNITY_ANDROID
            //TODO 
            EtceteraAndroid.showAlert("NO INTERNET CONNECTION.", "YOU ARE NOT CONNECTED TO INTERNET. TRY AGAIN LATER.", "OK");
            Debug.Log("REWARDED: NO INTERNET CONNECTION");
#elif UNITY_IOS
			var buttons = new string[] { "OK" };
			EtceteraBinding.showAlertWithTitleMessageAndButtons ("NO INTERNET CONNECTION.", "YOU ARE NOT CONNECTED TO INTERNET. TRY AGAIN LATER.", buttons);
#endif
miscGAFunction(sPlacement, location, false);
        }

    }

    void FixedUpdate()
    {
        interstitialTimer += Time.deltaTime;
    }


    //Invoked when initialization of RewardedVideo has finished successfully.
    void RewardedVideoInitSuccessEvent()
    {
        Debug.Log("Init rewarded video Success ");
    }

    //Invoked when RewardedVideo initialization process has failed.
    //IronSourceError contains the reason for the failure.
    void RewardedVideoInitFailEvent(IronSourceError error)
    {

        Debug.Log("Init rewarded video error ");

    }

    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy
    //tasks till the video ad will be closed.
    void RewardedVideoAdOpenedEvent()
    {
        Debug.Log("RewardedVideoAdOpenedEvent");
    }

    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    void RewardedVideoAdClosedEvent()
    {
        Debug.Log("RewardedVideoAdClosedEvent");
        /*if(Pause.pause != null)
		{
			Pause.pause.Resume ();
		}

		if (GameManager.gameManager != null)
			GameManager.gameManager.GetComponent<AudioSource>().enabled = true;
	*/

    }

    //Invoked when there is a change in the ad availability status.
	//@param - available - value will change to true when rewarded videos are available. 
	//You can then show the video by calling showRewardedVideo().
	//Value will change to false when no videos are available.
	void RewardedVideoAvailabilityChangedEvent(bool available) {
		Debug.Log("RewardedVideoAvailabilityChangedEvent");
		//Change the in-app 'Traffic Driver' state according to availability.
		bool rewardedVideoAvailability = available;
	}

    //Invoked when the video ad starts playing.
	void RewardedVideoAdStartedEvent() {
	}

	//Invoked when the video ad finishes playing.
	void RewardedVideoAdEndedEvent() {
	}


    //Invoked when the user completed the video and should be rewarded.
    //If using server-to-server callbacks you may ignore this events and wait for
    //the callback from the IronSource server.
    //@param - placement - placement object which contains the reward data
    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {

        interstitialTimer = 0;



        GameConstants.totalVideosWatched++;
        G2WGAHelper.LogEvent("Total videos watched", "Video Watched", GameConstants.totalVideosWatched.ToString(), 1);
        switch (placement.getPlacementName())
        {
            case (GameConstants.RVP_StoreCoins):
                // Watched video in Store to get coins 
                G2WGAHelper.LogEvent("Free Coins", "Video Watched", GameConstants.totalVideosWatched.ToString(), 1);
                G2WGAHelper.LogEvent("Free Coins", "Level", GameConstants.getStoryName() + ": Chapter " + GameConstants.currentChapterNumber.ToString(), 1);
                G2WGAHelper.LogRichEvent("Free Coins");
                if (StoreConstants.totalCoins < DynamicConstants.firstTimeHintPrice)
                {
                    G2WGAHelper.LogEvent("Coins Exhausted - Video", GameConstants.getStoryName() + ": HO " + GameConstants.currentChapterNumber.ToString(), "Store Video", 1);
                }
                StoreConstants.totalCoins += StoreConstants.CoinPack_Free;
                StoreUIScript.storeUIScript.rewardedCoins.SetActive(true);


                break;
            case (GameConstants.RVP_StoreMoves):
                // Watched video in Store to get Moves 
                if (StoreConstants.totalMoves <= 0)
                {
                    G2WGAHelper.LogEvent("Moves Exhausted - Video", GameConstants.getStoryName() + ": Chapter " + GameConstants.currentChapterNumber.ToString(), "Store Video", 1);
                }
                StoreConstants.totalMoves += StoreConstants.MovePack_Free;
                G2WGAHelper.LogEvent("Free Moves", "Video Watched", GameConstants.totalVideosWatched.ToString(), 1);
                G2WGAHelper.LogEvent("Free Moves", "Level", GameConstants.getStoryName() + ": Chapter " + GameConstants.currentChapterNumber.ToString(), 1);
                G2WGAHelper.LogRichEvent("Free Moves");
                StoreUIScript.storeUIScript.rewardedMoves.SetActive(true);

                break;
            case (GameConstants.RVP_InGameMoves):
                // Watched video in-game to get moves 
                if (StoreConstants.totalMoves <= 0)
                {
                    G2WGAHelper.LogEvent("Moves Exhausted - Video", GameConstants.getStoryName() + ": Chapter " + GameConstants.currentChapterNumber.ToString(), "Popup Video", 1);
                }
                StoreConstants.totalMoves += StoreConstants.Rewarded_Moves;
                G2WGAHelper.LogEvent("Out of Moves Popup - Video", "Video Watched", GameConstants.totalVideosWatched.ToString(), 1);
                //Hide th Out Of Moves popup
                LevelSelectionScript.LevelSelection.closeOutOfMoves();
                LevelSelectionScript.LevelSelection.rewardPopup.SetActive(true);

                break;
            case (GameConstants.RVP_InGameCoins):
                // Watched video in-game to get coins 
                if (StoreConstants.totalCoins < DynamicConstants.firstTimeHintPrice)
                {

                    G2WGAHelper.LogEvent("Coins Exhausted - Video", GameConstants.getStoryName() + ": HO " + GameConstants.currentChapterNumber.ToString(), "Popup Video", 1);
                }
                StoreConstants.totalCoins += StoreConstants.Rewarded_Coins;
                G2WGAHelper.LogEvent("Out of Coins Popup - Video", "Video Watched", GameConstants.totalVideosWatched.ToString(), 1);
                //Hide the outofCoinsPopup
                HOUIScript.houScript.hideOutOfCoinsPanel();
                if (HOUIScript.houScript.outOfTimePopup.activeSelf)
                {
                    HOUIScript.houScript.outOfTimeCoinsPopup.SetActive(true);
                    HOUIScript.houScript.outOfTimeNoCoinsPopup.SetActive(false);
                }
                HOUIScript.houScript.rewardPopup.SetActive(true);


                break;
            case (GameConstants.RVP_StoryModeCoins):
                //Watched video in the Story mode to get coins.
                if (StoreConstants.totalCoins < DynamicConstants.CoinsRequiredForDialgoue)
                {
                    G2WGAHelper.LogEvent("Coins Exhausted - Video", GameConstants.getStoryName() + ": Chapter " + GameConstants.currentChapterNumber.ToString(), "Popup Video", 1);
                }
                StoreConstants.totalCoins += StoreConstants.Rewarded_Coins;
                StoryUIScript.storyuiscript.closeCoinsPopup();
        
                break;
        }


        /*icloud*/
        // P31Prefs.setInt (G2WGameConstants.KEY_COINBALANCE, ScoreManager.GetCoinBalance () + G2WGameConstants.RewardVideo_Coins);   
    }

    //Invoked when the Rewarded Video failed to show
	//@param description - string - contains information about the failure.
	void RewardedVideoAdShowFailedEvent (IronSourceError error){
	}

    /* Invoked when Interstitial initialization process completes successfully.
	*/
    void InterstitialInitSuccessEvent()
    {
        Debug.Log("InterstitialInitSuccessEvent");
    }

    	/* 
 * Invoked when the initialization process has failed.
 * @param description - string - contains information about the failure.
 */
	void InterstitialAdLoadFailedEvent (IronSourceError error)
	{
		GameConstants.isInterstitialLoaded = false;
        GameConstants.isInterstitialRequested = false;
		Debug.Log ("InterstitialAdLoadFailedEvent, code: " + error.getCode () + ", description : " + error.getDescription ());
		//		if (error.getDescription ().Contains ("loadInterstitial can't be called before the Interstitial ad unit initialization completed successfully")) {
		//			Debug.Log ("initialization completed successfully");
		//			//IronSource.Agent.initInterstitial (G2WGameConstants.IronSource_APPLICATION_KEY, G2WGameConstants.advertising_ID);
		//		}
	}
//     /* 
//  * Invoked when the initialization process has failed.
//  * @param description - string - contains information about the failure.
//  */
//     void InterstitialLoadFailedEvent(IronSourceError error)
//     {
//         GameConstants.isInterstitialLoaded = false;
//         GameConstants.isInterstitialRequested = false;
//         Debug.Log("InterstitialLoadFailedEvent, code: " + error.getCode() + ", description : " + error.getDescription());
//         if (error.getDescription().Contains("loadInterstitial can't be called before the Interstitial ad unit initialization completed successfully"))
//         {
//             Debug.Log("initialization completed successfully");
//             IronSource.Agent.initInterstitial(GameConstants.IRONSOURCE_APPLICATION_KEY, AD_ID);
//         }
//     }
    /* 
 * Invoked right before the Interstitial screen is about to open.
 */
    void InterstitialShowSuccessEvent()
    {
        Debug.Log("InterstitialShowSuccessEvent");
    }
//     /* 
//  * Invoked when the ad fails to show.
//  * @param description - string - contains information about the failure.
//  */
//     void InterstitialShowFailEvent(IronSourceError error)
//     {
//         GameConstants.isInterstitialLoaded = false;
//         GameConstants.isInterstitialRequested = false;
//         CheckForInterstitialIronSource();
//         Debug.Log("InterstitialShowFailEvent, code :  " + error.getCode() + ", description : " + error.getDescription());
//     }

    	/* 
 * Invoked when the ad fails to show.
 * @param description - string - contains information about the failure.
 */
	void InterstitialAdShowFailedEvent (IronSourceError error)
	{
		GameConstants.isInterstitialLoaded = false;
        GameConstants.isInterstitialRequested = false;
        CheckForInterstitialIronSource();
		Debug.Log ("InterstitialShowFailEvent, code :  " + error.getCode () + ", description : " + error.getDescription ());
	}

    
    /* 
 * Invoked upon ad availability change.
 * @param available - bool - true when interstitial ad is ready to be presented or false
 * otherwise.
 */
    void ISAvailability(bool available)
    {
        //Show the Ad if available value is true
        Debug.Log("Interstitial status " + available);
    }
    /* 
 * Invoked when end user clicked on the interstitial ad
 */
    void InterstitialAdClickedEvent()
    {
        Debug.Log("InterstitialAdClickedEvent");
    }
    /* 
 * Invoked when the interstitial ad closed and the user goes back to the application screen.
 */
    void InterstitialAdClosedEvent()
    {
        interstitialTimer = 0;
        GameConstants.isInterstitialLoaded = false;
        GameConstants.isInterstitialRequested = false;
        CheckForInterstitialIronSource();
        Debug.Log("InterstitialAdClosedEvent");

    }
    /* 
 * Invoked when the interstitial initialization process fails
 */
    void InterstitialInitFailEvent(IronSourceError error)
    {
        Debug.Log("InterstitialInitFailEvent");
    }
//     /*
//  * Invoked when the Interstitial is Ready to shown after load function is called
//  */
//     void InterstitialReadyEvent()
//     {
//         GameConstants.isInterstitialLoaded = true;
//         GameConstants.isInterstitialRequested = false;
//         Debug.Log("InterstitialReadyEvent");
//     }

    /*
	* Invoked when the Interstitial is Ready to shown after load function is called
	*/
	void InterstitialAdReadyEvent ()
	{
		GameConstants.isInterstitialLoaded = true;
        GameConstants.isInterstitialRequested = false;
		Debug.Log ("InterstitialAdReadyEvent");
	}
    /*
 * Invoked when the Interstitial Ad Unit has opened
 */
    void InterstitialAdOpenedEvent()
    {
        Debug.Log("InterstitialAdOpenedEvent");
    }

    	/* 
 * Invoked right before the Interstitial screen is about to open.
 */
	void InterstitialAdShowSucceededEvent ()
	{
		Debug.Log ("InterstitialAdShowSucceededEvent");
	}

    public static void miscGAFunction(string sPlacement, string location, bool isNoADRelated)
    {
        string s = "";
		if(isNoADRelated)
		{
            s = "No Ads Available";
        }
        else
        {
            s = "No Internet";
        }
        if (location == "Story")
        {
            if (sPlacement.Contains("coins"))
            {
G2WGAHelper.LogEvent(s,"Coins",GameConstants.getStoryName(),1);
            }
            else
            {
G2WGAHelper.LogEvent(s,"Moves",GameConstants.getStoryName(),1);
            }
        }
        else
        {
            if (sPlacement.Contains("coins"))
            {
G2WGAHelper.LogEvent(s,"Coins","Store",1);
            }
            else
            {
G2WGAHelper.LogEvent(s,"Moves","Store",1);
            }
        }
    }

    public static bool shouldShowInterstitial ()
	{

			if (GameConstants.isInterstitialLoaded) {
				if (interstitialTimer >= GameConstants.InterstitialTimer) {
					return true;
				}
			}

			//CheckForInterstitialSuperSonic ();
		
		return false;
	}

}
