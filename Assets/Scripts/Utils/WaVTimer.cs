﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class WaVTimer : MonoBehaviour
{

		public Text hour, minute, sec;
		private int mins, secs,hours;
		public static bool bTimer;
		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (PlayerPrefsX.GetBool ("VideoTimer") == true) {
					// Debug.Log("CURRENT MINUTE" + System.DateTime.Now.Minute + " CURRENT SECOND" + System.DateTime.Now.Second + " CURRENT MINUTE PREFS" + PlayerPrefs.GetInt ("CurrentMinute"));
					// 	mins = 59 - (System.DateTime.Now.Minute - PlayerPrefs.GetInt ("CurrentMinute")) % 60;
					// 	// if (mins >= 60) {
					// 	// 		mins = 0;
					// 	// }
					// 	secs = 60 - (System.DateTime.Now.Second);
					// 	// if (secs >= 60) {
					// 	// 		secs = 0;
					// 	// }
					// 	if(System.DateTime.Now.Day == PlayerPrefs.GetInt("CurrentDay"))
					// 	{
					// 	hour.text = string.Format ("{0:00}", ((DynamicConstants.freeMovesTime - 1) - (System.DateTime.Now.Hour - PlayerPrefs.GetInt ("CurrentHour")))) + ": ";
					// 	}
					// 	//hour.text = (3 - (System.DateTime.Now.Hour - PlayerPrefs.GetInt ("CurrentHour"))).ToString() + ":";
					// 	minute.text = string.Format ("{0:00}", mins) + ": ";
					// 	//minute.text = (mins).ToString () + ":";
					// 	sec.text = string.Format ("{0:00}", secs);
					// 	//sec.text = (secs).ToString();
					// 			Debug.Log (System.DateTime.Now.Hour);
					// 			Debug.Log (PlayerPrefs.GetInt ("CurrentHour").ToString());


DateTime savedTime = new DateTime(PlayerPrefs.GetInt ("CurrentYear"), PlayerPrefs.GetInt ("CurrentMonth"), PlayerPrefs.GetInt ("CurrentDay"), PlayerPrefs.GetInt ("CurrentHour") , PlayerPrefs.GetInt ("CurrentMinute"), PlayerPrefs.GetInt("CurrentSeconds"));
 DateTime currentTime = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, System.DateTime.Now.Hour, System.DateTime.Now.Minute, System.DateTime.Now.Second);
 TimeSpan travelTime = currentTime - savedTime;  
 Debug.Log("travelTime: " + travelTime );  
hours = (DynamicConstants.freeMovesTime - 1) - travelTime.Hours;
 hour.text =  string.Format ("{0:00}",hours.ToString()+ ": ");
mins = 59 - travelTime.Minutes;
minute.text = string.Format ("{0:00}",mins.ToString()+ ": ");
	secs = 60 - travelTime.Seconds;
sec.text =string.Format ("{0:00}",secs.ToString()); 

  TimeSpan ts = TimeSpan.FromSeconds(10);
ts = ts.Subtract(TimeSpan.FromSeconds(1));
string s = string.Format("{0:D2}h:{1:D2}m:{2:D2}s", ts.Hours, ts.Minutes, ts.Seconds);

Debug.Log("STRING S" + s);
if(hours < 0 )
{
	Messenger.Broadcast<string>(MessengerConstants.AWARD_TIMER,"");
}


				}
		}
}
