﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameTimer : MonoBehaviour {
	bool isRunning = false;
	//int limit = 120;
	
	int lastTime = 0;
	float timer = 0;

	public UnityEngine.UI.Text text;
	
	public Dictionary<int, System.Action> callbacks = new Dictionary<int, System.Action>();
	void Start()
	{
		//g_PauseButton.SetActive (true);
	}
	void Update() {
		if (HOGameplayScript.CurrentLevel.CurState == HOGameplayScript.GameState.Playing && isRunning) {
			timer -= Time.deltaTime;
			if (timer < lastTime - 1) {
				SetText();
				if (callbacks.ContainsKey(lastTime)) {
					callbacks[lastTime]();
					callbacks.Remove(lastTime);
					if (lastTime <= 0) {
						isRunning = false;
					}
				}
			}
		}
	}
	
	void SetText() {
		lastTime = (int)Mathf.Ceil(timer);
		//text.text = lastTime.ToString();
		text.text = string.Format ("TIME: {0:00}:{1:00}", lastTime / 60, lastTime % 60);
	}
	
	public void StartTimer(int startFrom, UnityEngine.UI.Text text) {
		timer = lastTime = startFrom;
		isRunning = true;
		this.text = text;
		SetText();
	}
	
	public void PauseTime() {
		isRunning = false;
	}
	
	public void ResumeTime() {
		isRunning = true;
	}
	
	public void AddCallback(int time, System.Action callback) {
		callbacks.Add(time, callback);
	}
	
	public void SubtractTime(float time) {
		Debug.Log ("TIME SUBTRACT" + timer);
		timer -= time;
		if (timer < 0 ) {
			timer = 0;
		}
		
	}
	
	public float GetSeconds() {
		return timer;
	}
}
