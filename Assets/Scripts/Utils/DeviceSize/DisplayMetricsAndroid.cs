// Programmer: Harshal Kolambe 
// Date and Time: 07/08/2013 - 3:35 PM
// Class type and Name: Singleton class :: DisplayMetricsAndroid
using UnityEngine;
     
    public class DisplayMetricsAndroid 
{
    public static double androidDevSize;
	private float screenWidth;
    private float screenHeight;
	
    public static string model { get; protected set; }
	
	public static string product { get; protected set; }
	
	public static string device { get; protected set; }
   
    public static float Density { get; protected set; }
     
    public static int DensityDPI { get; protected set; }
     
    public static int HeightPixels { get; protected set; }
     
    public static int WidthPixels { get; protected set; }
     
    public static float ScaledDensity { get; protected set; }
     
    public static float XDPI { get; protected set; }
     
    public static float YDPI { get; protected set; }
     
    static DisplayMetricsAndroid() 
	{
	    // Early out if we're not on an Android device
	    if (Application.platform != RuntimePlatform.Android) 
		{
	    return;
	    }
	     
	    using (
	    AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"),
	    metricsClass = new AndroidJavaClass("android.util.DisplayMetrics")
	    ) 
			{
		    using (
		    AndroidJavaObject metricsInstance = new AndroidJavaObject("android.util.DisplayMetrics"),
		    activityInstance = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"),
		    windowManagerInstance = activityInstance.Call<AndroidJavaObject>("getWindowManager"),
		    displayInstance = windowManagerInstance.Call<AndroidJavaObject>("getDefaultDisplay")
		    ) 
				{
					displayInstance.Call("getMetrics", metricsInstance);
					Density = metricsInstance.Get<float>("density");
					DensityDPI = metricsInstance.Get<int>("densityDpi");
					HeightPixels = metricsInstance.Get<int>("heightPixels");
					WidthPixels = metricsInstance.Get<int>("widthPixels");
					ScaledDensity = metricsInstance.Get<float>("scaledDensity");
					XDPI = metricsInstance.Get<float>("xdpi");
					YDPI = metricsInstance.Get<float>("ydpi");
			    }
			
			using( 
				AndroidJavaClass buildclass = new AndroidJavaClass("android.os.Build")
				)
			{
				
				using(
					AndroidJavaObject buildObj = new AndroidJavaObject("android.os.Build")
					)
				{
					model = buildObj.GetStatic<string>("MODEL");
					product = buildObj.GetStatic<string>("PRODUCT");
					device = buildObj.GetStatic<string>("DEVICE");
				
				}
				
			}
			
		    }
	   }
		
    }