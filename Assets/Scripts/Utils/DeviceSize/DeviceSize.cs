﻿using UnityEngine;
using System.Collections;

public class DeviceSize : MonoBehaviour
{

	private static float screenWidth;
	private static float screenHeight;
	public static double devSize;

	public enum ScreenAspect
	{
		ASPECT_3_2,
		ASPECT_4_3,
		ASPECT_5_3,
		ASPECT_16_9,
		ASPECT_16_10,
		ASPECT_80_47,
		ASPECT_128_69,
		ASPECT_128_75,
		ASPECT_427_240,
	}

	public static ScreenAspect m_curAspectRatio;

	static bool isAspectRatio (int W, int H)
	{
		if (Mathf.Abs ((W - Camera.main.aspect * H) / H) <= 0.02f)
			return true;
		return false;
	}

	void detectAspectRatio ()
	{
		if (isAspectRatio (3, 2))
			m_curAspectRatio = ScreenAspect.ASPECT_3_2;//1.5
		if (isAspectRatio (4, 3))
			m_curAspectRatio = ScreenAspect.ASPECT_4_3;//1.33
		if (isAspectRatio (5, 3))
			m_curAspectRatio = ScreenAspect.ASPECT_5_3;//1.66
		if (isAspectRatio (16, 9))
			m_curAspectRatio = ScreenAspect.ASPECT_16_9;//1.77
		if (isAspectRatio (16, 10))
			m_curAspectRatio = ScreenAspect.ASPECT_16_10;//1.6
		if (isAspectRatio (80, 47))
			m_curAspectRatio = ScreenAspect.ASPECT_80_47;//1.7
		if (isAspectRatio (128, 69))
			m_curAspectRatio = ScreenAspect.ASPECT_128_69;//1.85
		if (isAspectRatio (128, 75))
			m_curAspectRatio = ScreenAspect.ASPECT_128_75;//1.7
		if (isAspectRatio (427, 240))
			m_curAspectRatio = ScreenAspect.ASPECT_427_240;//1.77
		
	}

	public static bool isPhone ()
	{
#if UNITY_ANDROID
		screenWidth = DisplayMetricsAndroid.WidthPixels / DisplayMetricsAndroid.XDPI;
		screenHeight = DisplayMetricsAndroid.HeightPixels / DisplayMetricsAndroid.YDPI;
		devSize = Mathf.Sqrt ((Mathf.Pow (screenWidth, 2)) + (Mathf.Pow (screenHeight, 2)));
		if (devSize > 6.5)
			return false;
		else
			return true;
#else
		if((UnityEngine.iOS.Device.generation.ToString()).IndexOf("iPad") > -1){
			return false;
		}else return true;

#endif

	}

	void Awake ()
	{
		DontDestroyOnLoad (this.gameObject);
		detectAspectRatio ();
	}
	// Use this for initialization
	void Start ()
	{
		Debug.Log ("Aspect Ratio: " + m_curAspectRatio);
	}
}
