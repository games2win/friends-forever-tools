﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateValue : MonoBehaviour
{

	public enum ValueType
	{
		Coin,
		Hint,
		Passes
	}

	public ValueType type;

	private string s_type;

	void OnEnable ()
	{
		if (type == ValueType.Passes) {
			this.gameObject.GetComponent<Text> ().text = StoreConstants.totalMoves.ToString ();
			s_type = "passes";
		}
		if (type == ValueType.Coin) {
			this.gameObject.GetComponent<Text> ().text = StoreConstants.totalCoins.ToString ();
			s_type = "coins";
		}
		if (type == ValueType.Hint) {
			this.gameObject.GetComponent<Text> ().text = StoreConstants.totalAvailableHints.ToString ();
			s_type = "hints";
		}
		Messenger.AddListener<string, int> (MessengerConstants.VALUE_UPDATE, updateValue);
		Messenger.MarkAsPermanent (MessengerConstants.VALUE_UPDATE);
	}

	void OnDisable ()
	{
		Messenger.RemoveListener<string, int> (MessengerConstants.VALUE_UPDATE, updateValue);
	}

	void updateValue (string stringtype, int value)
	{
		if (stringtype == s_type) {
			this.gameObject.GetComponent<Text> ().text = value.ToString ();
		}
	}
}
