﻿using UnityEngine;
using System.Collections;
using Prime31;

public class G2WIAPHelper : MonoBehaviour {

	private static G2WIAPHelper instance;

	void Awake ()
	{
		if (instance != null)
			Destroy (gameObject);

		instance = this;
		DontDestroyOnLoad (gameObject);
	}

	void OnEnable()
	{
		#if UNITY_ANDROID
		// Listen to all events for illustration purposes
		GoogleIABManager.billingSupportedEvent += billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent += billingNotSupportedEvent;
		#endif
	}

	void OnDisable()
	{
		#if UNITY_ANDROID
		// Remove all event handlers
		GoogleIABManager.billingSupportedEvent -= billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		#endif
	}
		
	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		GoogleIAB.init ("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAs/stJKp8xRhXubBr4H+iGLPVO/HB4qm4Re+dCgICtJD0/b7ACxqT5w4dIz7yz0zlUWzqJA5YElW+h95hdFzQUfV7mg69TWJAd/Wgvr/bvIcE7ZImKGHWb3YXeXS6jV0Xe5OApPJdGQIIU63Ui/O1MpVofDZ8aK9k7FX+P4CxjoygkW9kmZhq2RQ3bN0N1wBDIGOkpglDs//6E9OxyT4Lc+cUzkWPyK7lpI0F2RnB2ZjDGZFrMWvA7N3eY4pMfxYMKJTQgfic8BInQwOxvf+HyHlkQEz17nFQh2epuDecolISt3V9UR/KyU0XK1xsSwLeleXtQj3z6o3NeCYgloWJDQIDAQAB");
		GoogleIAB.enableLogging(true);
		#elif UNITY_IOS 
		QueryProducts ();
		#endif
	}

	void billingSupportedEvent()
	{
		Debug.Log( "billingSupportedEvent" );

		QueryProducts ();
	}

	public static string[] DynamicPrices = new string[]{ "$0.99","$1.99", "$2.99","$0.99", "$1.99", "$2.99"};

	void QueryProducts ()
	{
		var skus = new string [] {
			GameConstants.IAP_COINPACK_1,
			GameConstants.IAP_COINPACK_2,
			GameConstants.IAP_COINPACK_3,
			GameConstants.IAP_MOVEPACK_1,
			GameConstants.IAP_MOVEPACK_2,
			GameConstants.IAP_MOVEPACK_3
		};

		IAP.requestProductData (skus, skus, productList => {
			Utils.logObject (productList);
			if (productList != null) {
				Debug.Log ("Products List Count: " + productList.Count);
				string[] dynamPrices = new string[6];
				string currencycode = "";

				for (int i = 0; i < productList.Count; i++) {
					currencycode = productList [0].currencyCode;

					if (currencycode == "USD") {
						//currencycode = "$";
					} else if (currencycode == "INR") {
						#if UNITY_IOS
						currencycode="Rs.";
						#elif UNITY_ANDROID
						currencycode = "";
						#endif
					}
					switch (productList [i].productId) {

					case "com.games2win.ff.coinpack_1":
						dynamPrices [0] = currencycode + productList [i].price;
						break;
					case "com.games2win.ff.coinpack_2":
						dynamPrices [1] = currencycode + productList [i].price;
						break;
					case "com.games2win.ff.coinpack_3":
						dynamPrices [2] = currencycode + productList [i].price;
						break;
					case "com.games2win.ff.movepack_1":
						dynamPrices [3] = currencycode + productList [i].price;
						break;
					case "com.games2win.ff.movepack_2":
						dynamPrices [4] = currencycode + productList [i].price;
						break;
					case "com.games2win.ff.movepack_3":
						dynamPrices [5] = currencycode + productList [i].price;
						break;
					}
				}
				DynamicPrices = dynamPrices;
			} else {
				Debug.Log ("Product List Null!");
			}
		});
	}

	void billingNotSupportedEvent( string error )
	{
		Debug.Log( "billingNotSupportedEvent: " + error );
	}


	// Update is called once per frame
	void Update () {
	
	}
}
