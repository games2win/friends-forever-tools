﻿using UnityEngine;
using System.Collections;
using System;

public class DateMaster : MonoBehaviour
{
	DateTime currentDate;
	DateTime oldDate;

	public static DateMaster instance;
	long temp = 0;

	void Awake()
	{
		if (instance != null)
			Destroy(gameObject);

		instance = this;
		DontDestroyOnLoad(gameObject);
	}
	void Start()
	{
		//Store the current time when it starts
		currentDate = System.DateTime.Now;

		//Grab the old time from the player prefs as a long
		//        if(PlayerPrefs.HasKey("DateString"))
		//            temp = Convert.ToInt64(PlayerPrefs.GetString("DateString", "0"));
		//		else
		//			PlayerPrefs.SetString("DateString", "0");

		//Convert the old time from binary to a DataTime variable
		oldDate = DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString("DateString", System.DateTime.Now.ToBinary().ToString())));
		//print("oldDate: " + oldDate);

		//Use the Subtract method and store the result as a timespan variable
		TimeSpan difference = currentDate.Subtract(oldDate);
	//	print("Difference: " + difference);

	}

	public double returnDays () 
	{
		double ReturnDate =  (currentDate - oldDate).TotalDays;
		return (int)ReturnDate + 1;
	}

	void OnApplicationQuit()
	 {
	// 	//Savee the current system time as a string in the player prefs class
		PlayerPrefs.SetString("DateString", System.DateTime.Now.ToBinary().ToString());

	// 	//print("Saving this date to prefs: " + System.DateTime.Now);
	 }

	 /// <summary>
	/// Callback sent to all game objects when the player pauses.
	/// </summary>
	/// <param name="pauseStatus">The pause state of the application.</param>
	void OnApplicationPause(bool pauseStatus)
	{
		PlayerPrefs.SetString("DateString", System.DateTime.Now.ToBinary().ToString());
	}

}