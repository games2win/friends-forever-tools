using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class G2WGAHelper
{
	public static void StartSession ()
	{
		#if !UNITY_EDITOR
		GoogleAnalyticsV4.instance.StartSession ();
		#endif
	}
		
	public static void LogEvent (string eventCategory, string eventAction = "Tapped")
	{
		#if !UNITY_EDITOR
		GoogleAnalyticsV4.instance.LogEvent (new EventHitBuilder ()
		                                    .SetEventCategory (eventCategory)
		                                    .SetEventAction (eventAction));

		#endif
	//	Debug.Log ("Google Analytics Event: " + eventCategory + " - " + eventAction);
	}

	public static void LogEvent (string eventCategory, string eventAction, string eventLabel, long eventValue)
	{
		#if !UNITY_EDITOR
		GoogleAnalyticsV4.instance.LogEvent (new EventHitBuilder ()
		                                     .SetEventCategory (eventCategory)
		                                     .SetEventAction (eventAction)
		                                     .SetEventLabel (eventLabel)
		                                     .SetEventValue (eventValue));
		#endif
		
	//Debug.Log ("Google Analytics Event: " + eventCategory + " - " + eventAction + " - " + eventLabel + " - " + eventValue);
	}

	public static void LogScreen (string screenTitle)
	{
		#if !UNITY_EDITOR
		GoogleAnalyticsV4.instance.LogScreen (screenTitle);
		
		#endif
		//Debug.Log ("Google Analytics Screen: " + screenTitle);
	}

	public static void LogRichEvent (string eventName, params string[] properties)
	{
		Dictionary<string,string> props = new Dictionary<string, string> ();


		for (int i = 0; i < properties.Length; i += 2) {
			props.Add (properties [i], properties [i + 1]);
		}
		AppsFlyer.trackRichEvent (eventName, props);
// 		#if UNITY_IPHONE
		
// 		#endif

// 		#if UNITY_ANDROID
// 		FlurryAnalytics.logEvent (eventName, pars);
// 		#endif
// //		foreach (KeyValuePair<string, string> temp in pars) {
// //			Debug.Log ("Flurry Event: " + eventName + ". Properties: " + temp.Key.ToString () + ", Value: " + temp.Value.ToString ());
// //		} 
	}

	public static void LogTiming (string timingCategory, long timing, string name, string label)
	{

		GoogleAnalyticsV4.instance.LogTiming (timingCategory, timing, name, label);
		//Debug.Log (String.Format ("GAEvent: {0} , {1} , {2}, {3}", timingCategory, timing, name, label));
	}

	
}

