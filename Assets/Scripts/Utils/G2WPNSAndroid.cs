﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using Prime31;

/// <summary>
/// GCM based PNS script.
/// </summary>
public class G2WPNSAndroid : MonoBehaviour
{
	private static G2WPNSAndroid instance = null;

	#if UNITY_ANDROID
	private string regURL;
	private const string host = "androidpns.games2win.com/android_devices/add";

	//PNS ID provided by the publishing team
	private string pnsID = "104";
	string message = "";

	//TODO PNS Environment = should be 'production' in the release build
	private const string pnsEnvironment = "production";

	private const string pnsAppVersion = "1.7";


	void Awake()
    {
        if (instance)
            Destroy(gameObject);

        instance = this;
        DontDestroyOnLoad(gameObject);

    }

	void Start ()
	{
		GoogleCloudMessaging.checkForNotifications ();
	
		//DO NOT change the GCM id - its Games2win's GCM sender id
		GoogleCloudMessaging.register ("637939466018");
	}

	void OnEnable ()
	{
		GoogleCloudMessagingManager.registrationSucceededEvent += onRegistrationReceived;
		GoogleCloudMessagingManager.notificationReceivedEvent += onNotificationReceived;
		GoogleCloudMessagingManager.registrationFailedEvent += onRegFailed;
	}

	void OnDisable ()
	{
		GoogleCloudMessagingManager.notificationReceivedEvent -= onNotificationReceived;
		GoogleCloudMessagingManager.registrationSucceededEvent -= onRegistrationReceived;
		GoogleCloudMessagingManager.registrationFailedEvent -= onRegFailed;
	}

	void OnApplicationFocus (bool focus)
	{
		if (!focus) {
			GoogleCloudMessagingManager.registrationSucceededEvent += onRegistrationReceived;
			GoogleCloudMessagingManager.notificationReceivedEvent += onNotificationReceived;
			GoogleCloudMessagingManager.registrationFailedEvent += onRegFailed;

			Debug.Log ("On App Focus PNS");
		}
	}

	void onNotificationReceived (Dictionary<string,object> jsonDict)
	{
		Debug.Log ("JSONDict: " + jsonDict.ToString ());
		int badgeNum = int.Parse (jsonDict ["badge"].ToString ());
		message = jsonDict ["message"].ToString ();

		string openURL = jsonDict ["open_url"].ToString ();

		Debug.Log ("Badge: " + badgeNum + " URL: " + openURL);
		string urlTemp;
		if (!string.IsNullOrEmpty (openURL) && !GameConstants.isPNSURLOpened) {
			GameConstants.isPNSURLOpened = true;
			urlTemp = openURL;
			Application.OpenURL (urlTemp);
		}
		badgeNum = 0;
		openURL = "";
		urlTemp = "";
	}
		
	void onRegistrationReceived (string regID)
	{
		Debug.Log ("Registration Recieved " + regID);
		StringBuilder urlString = new StringBuilder ();
		urlString.AppendFormat ("?registration_id={0}", WWW.EscapeURL (regID));
		urlString.AppendFormat ("&app_id={0}", WWW.EscapeURL (pnsID));
		urlString.AppendFormat ("&app_version={0}", WWW.EscapeURL (pnsAppVersion));
		urlString.AppendFormat ("&apns_env={0}", WWW.EscapeURL (pnsEnvironment));
		regURL = urlString.ToString ();

		StartCoroutine (RegDeviceForPush ());
	}

	void onRegFailed (string error)
	{
		Debug.Log ("Register Failed: " + error);
	}


	IEnumerator RegDeviceForPush ()
	{
		WWW w = new WWW ("http://" + (host + regURL), UTF8Encoding.UTF8.GetBytes (regURL));
		yield return w;
		Debug.Log ("Credentials: " + regURL + " ");
	}
	#endif
}
