﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiscAchievementsScript
{


    // Achievements unlocked for completing inbetween stories.
    public static void midStoryAchievements()
    {
        if (GameConstants.currentStoryNumber == 2)
        {
            if (GameConstants.currentChapterNumber == 5)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.FASTEST_FIVE);
            }
            else if (GameConstants.currentChapterNumber == 15)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.BLOSSOMING_ROMANCE);
            }
        }
        if (GameConstants.currentStoryNumber == 3)
        {
            if (GameConstants.currentChapterNumber == 10)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.TANGO_EXPERT);
            }
            else if (GameConstants.currentChapterNumber == 29)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.HERO_OF_THE_DAY);
            }

        }
        if (GameConstants.currentStoryNumber == 4)
        {
            if (GameConstants.currentChapterNumber == 5)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.LAB_ROMANCE);
            }
            else if (GameConstants.currentChapterNumber == 10)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.SWEET_ENCOUNTER);
            }
            else if (GameConstants.currentChapterNumber == 15)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.LIVING_IT_UP);
            }
            else if (GameConstants.currentChapterNumber == 21)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.A_FRIEND_IN_NEED);
            }

        }
        if (GameConstants.currentStoryNumber == 5)
        {
            if (GameConstants.currentChapterNumber == 5)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.SIGNS_SPECIALIST);
            }

            if (GameConstants.currentChapterNumber == 20)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.REAL_ENCOUNTER);
            }
        }
        if (GameConstants.currentStoryNumber == 6)
        {
            if (GameConstants.currentChapterNumber == 5)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.TAKES_TWO_TO_TANGO);
            }

            if (GameConstants.currentChapterNumber == 10)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.CURIOUS_CHOICES);
            }
            if (GameConstants.currentChapterNumber == 15)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.ON_THE_FENCE);
            }


        }
        if (GameConstants.currentStoryNumber == 7)
        {
            if (GameConstants.currentChapterNumber == 3)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.THE_SWITCHEROO);
            }

            if (GameConstants.currentChapterNumber == 9)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.ROMANCING_IN_RAINS);
            }
            if (GameConstants.currentChapterNumber == 15)
            {
                GameServicesHelper.UnlockAchievements(AchievementConstants.MAGICAL_LOVE);
            }


        }
    }
}
