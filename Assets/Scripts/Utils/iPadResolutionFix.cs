﻿using UnityEngine;
using System.Collections;
using Fungus;

public class iPadResolutionFix : MonoBehaviour {
	private View currentView;

	public bool isIphone = false;

	public float viewSize = 0f;
	// Use this for initialization
	void OnEnable () {
		Debug.Log ("CURRENT VIEW BEFORE" + viewSize);
		currentView = this.gameObject.GetComponent<View> ();
		if (SystemInfo.deviceModel.Contains ("iPad")) {
			currentView.ViewSize = viewSize;
		}
		#if UNITY_EDITOR
		if(!isIphone)
		currentView.ViewSize = viewSize;
		#endif
		Debug.Log ("CURRENT VIEW AFTER" + viewSize);
	}
	

}
