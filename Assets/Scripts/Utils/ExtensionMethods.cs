﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using UnityEngine;



public static class ExtensionMethods
{
public static GameObject FindObject(this GameObject parent, string name)
{
		Transform[] trs= parent.GetComponentsInChildren(typeof(Transform), true) as Transform[];
	foreach(Transform t in trs){
		if(t.name == name){
			return t.gameObject;
		}
	}
	return null;
}

public static string ToTitleCase(this string s) {
    return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(s.ToLower());
}

public static T GetCopyOf<T>(this Component comp, T other) where T : Component
 {
     Type type = comp.GetType();
     if (type != other.GetType()) return null; // type mis-match
     BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
     PropertyInfo[] pinfos = type.GetProperties(flags);
     foreach (var pinfo in pinfos) {
         if (pinfo.CanWrite) {
             try {
                 pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
             }
             catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
         }
     }
     FieldInfo[] finfos = type.GetFields(flags);
     foreach (var finfo in finfos) {
         finfo.SetValue(comp, finfo.GetValue(other));
     }
     return comp as T;
 }

 public static int[] ShuffleStory<T>(this T[] arr,int storyNumber)
        {
        int[] arr1 = arr.Cast<int>().ToArray();
        List<int> list1 = arr1.Cast<int>().ToList();
        list1.Remove(storyNumber);
        list1.Insert(0,storyNumber);
        arr1 = list1.ToArray();
        return arr1;
    }
}