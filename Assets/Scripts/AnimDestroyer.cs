﻿using UnityEngine;
using System.Collections;

public class AnimDestroyer : MonoBehaviour {
	public void DestroyObject() {
		Destroy(gameObject);
	}
}
