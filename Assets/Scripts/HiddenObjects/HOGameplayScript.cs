﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using Fungus;
using BitBenderGames;

public class HOGameplayScript : MonoBehaviour
{

	public enum GameState : int
	{
		Briefing = 0,
		Playing = 1,
		Paused = 2,
		TimeOver = 3
	}

	static HOGameplayScript _currentLevel;

	public static HOGameplayScript CurrentLevel {
		get { return _currentLevel; }
	}

	GameObject hintGlowEffect;

	//Game timer
	private GameTimer timer;

	// Game state
	public GameState _curState = GameState.Briefing;

	public GameState CurState {
		set { _curState = value; }
		get { return _curState; }
	}

	// Level data
	int objectsToFind = 6;
	int timeLimit = 120;

	public bool isRunning;

	public Text timerText;

	public List<GameObject> mainObject;
	List<GameObject> allHiddenObjects;
	List<int> selectedHiddenObjects;
	// Indexes of objects that player must find to win
	int[] activeHiddenObjects = new int[3];
	// Indexes of objects that are clickable right now

	// UI reference
	HOUIScript uiScript;

	private int objectFound = 0;

	public bool isHintActive;

	int objectsClicked = 0;

	public GameObject hintObjectPrefab;

	public Camera mainCamera;

	public GameObject bugPanel;

	void Awake ()
	{
		GameConstants.currentGameState = GameStates.HIDDEN_OBJECT_MODE;
		mainCamera.GetComponent<MobileTouchCamera>().enabled = false;
		_currentLevel = this;
		uiScript = GetComponent<HOUIScript> ();
		timer = GameObject.Find ("GameTimer").GetComponent<GameTimer> ();

	}

	void Start ()
	{
		G2WGAHelper.LogEvent ("Start Screen", "Seen");
		G2WGAHelper.LogRichEvent("Start Screen");
		G2WGAHelper.LogEvent (GameConstants.getStoryName() + ": Start HO " + GameConstants.currentChapterNumber.ToString());
		GameConstants.timerUsedInLevel = 0;
		GameConstants.hintsUsedInLevel = 0;
		setupObjects ();
	}



	public void objectClickResult (bool result)
	{
		if(_curState == GameState.Playing)
		{
		Debug.Log ("BOOL RESULT" + result);
		if (result) {
			//objectFound++;
			//uiScript.IncrObjectFind (objectFound, 6);
		} else {
			timer.SubtractTime (3.0f);
		}
		}
	}

	void setupObjects ()
	{
		allHiddenObjects = new List<GameObject> (GameObject.FindGameObjectsWithTag ("HiddenObject"));
		selectedHiddenObjects = new List<int> (objectsToFind);

		int[] ids = new int[allHiddenObjects.Count];
		// Inside-out Fisher-Yates shuffle for all objects
		for (int i = 0; i < ids.Length; i++) {
			int j = Random.Range (0, i + 1);
			if (j != i) {
				ids [i] = ids [j];
			}
			ids [j] = i;
		}

		// Add main objects
		//for (int i = 0; i < mainObject.Count; i++) {
			//if (mainObject [i].activeSelf) {
		selectedHiddenObjects.Add (allHiddenObjects.IndexOf (mainObject [GameConstants.currentHiddenObjectLevelNumber - 1]));
			//}
		//}

		// Add random objects from ids list if not already present
		int index = 0;
		while (selectedHiddenObjects.Count < objectsToFind) {
			if (!selectedHiddenObjects.Contains (ids [index])) {
				selectedHiddenObjects.Add (ids [index]);
			}
			index++;
		}
		// Shuffle finally selected objects
		int startId = 0;
//		if (levelNumber == 23 - 1) {
//			startId = 2;
//		}
		for (int i = startId; i < selectedHiddenObjects.Count - 1; i++) {
			int j = Random.Range (i, selectedHiddenObjects.Count);
			int tmp = selectedHiddenObjects [i];
			selectedHiddenObjects [i] = selectedHiddenObjects [j];
			selectedHiddenObjects [j] = tmp;
		}

		// Set UI
		for (int i = 0; i < 3; i++) {
			activeHiddenObjects [i] = selectedHiddenObjects [i];
			allHiddenObjects [activeHiddenObjects [i]].SendMessage ("Activate", activeHiddenObjects [i]);
			string objName = allHiddenObjects [activeHiddenObjects [i]].GetComponent<HiddenObject> ().objName;
			if (string.IsNullOrEmpty (objName)) {
				objName = allHiddenObjects [activeHiddenObjects [i]].name;
			}
			uiScript.SetObjectToFind (i, objName);
			selectedHiddenObjects [i] = -1;
		}
	}



	public void onPlayButtonClicked ()
	{
		StartPlaying ();
		Invoke ("HideBugPanel", 0.5f);
	}




	/// <summary>
	/// Start playing the level.
	/// </summary>
	public void StartPlaying ()
	{
		ChangeState (GameState.Playing);
		mainCamera.GetComponent<MobileTouchCamera>().enabled = true;
		//lastObjectPickedTime = timer.GetSeconds();
		//lastClickedAt = timer.GetSeconds();
		isRunning = true;
	}

	public void HideBugPanel()
	{
		bugPanel.SetActive (false);
	}


	public void ChangeState (GameState newState)
	{
		switch (newState) {
		case GameState.Briefing:
			break;
		case GameState.Paused:
			timer.PauseTime ();
			break;
		case GameState.Playing:
			Debug.Log ("IN CHANGE STATE");
			if (!isRunning) {
				Debug.Log ("IN CHANGE STATE 2");
				timer.StartTimer(timeLimit, timerText);
				timer.AddCallback(0, TimeOver);
			} else {
				timer.ResumeTime();
			}
			break;
		case GameState.TimeOver:
			//uiScript.ShowTimeOver(objectsToFind - objectsClicked, 30);
			break;
		}
		_curState = newState;
	}





	/// <summary>
	/// Called back when time is over.
	/// </summary>
	void TimeOver ()
	{
		//EditorDebug.Log("TimeOver");
		uiScript.showTimerOverPanel();
		ChangeState (GameState.TimeOver);
	}

	/// <summary>
	/// Correct object tapped by user.
	/// </summary>
	/// <param name="id">Identifier.</param>
	public void ObjectClicked (int id)
	{
		Debug.Log ("OBJECT CLICKED");
		if (_curState != GameState.Playing)
			return;
		isHintActive = false;
		if (hintGlowEffect != null)
			hintGlowEffect.SendMessage ("DestroyObject");
		hintGlowEffect = null;
		objectsClicked += 1;
		int prevPos = -1;
		for (int i = 0; i < activeHiddenObjects.Length; i++) {
			if (id == activeHiddenObjects [i]) {
				prevPos = i;
				break;
			}
		}
		if (objectsClicked + 2 < objectsToFind) {
			int next = selectedHiddenObjects [2 + objectsClicked];
			allHiddenObjects [next].SendMessage ("Activate", next);
			activeHiddenObjects [prevPos] = next;
			uiScript.SetObjectToFind (prevPos, allHiddenObjects [activeHiddenObjects [prevPos]].GetComponent<HiddenObject> ().objName);
		} else {
			uiScript.SetObjectToFind (prevPos, "");
		}
		uiScript.IncrObjectFind (objectsClicked, objectsToFind);
		//point implementation
		float objectPickedAt = timer.GetSeconds ();
//		if (Mathf.Abs(objectPickedAt - lastObjectPickedTime) <= 5) {
//			streakCoins += LevelData.levels[levelNumber].BonusCoinHigh;
//			streakPoints += LevelData.levels[levelNumber].BonusPointHign;
//		} else {
//			streakCoins += LevelData.levels[levelNumber].BonusCoinLow;
//			streakPoints += LevelData.levels[levelNumber].BonusPointLow;
//		}
//		lastObjectPickedTime = objectPickedAt;
//		//correct
//		Instantiate(correctObjectPrefab, allHiddenObjects[id].transform.position, Quaternion.identity);
//		if (mainObject.Contains(allHiddenObjects[id])) {
//			uiScript.ShowExpressionBubble(1);
//		} else if (objectsClicked % 2 == 0) {
//			uiScript.ShowExpressionBubble(3);
//		} 
		checkForWin();

	}

	void checkForWin()
	{
		if (objectsClicked == objectsToFind) {
            // Game Win
            //OnLevelComplete(true);
            //SceneManager.LoadScene("TempLevelSelection");
            SceneManager.LoadScene ("EndScene");
        }
	}



	int hintsUsedThisLevel = 0;

	/// <summary>
	/// Raises the hit click event.
	/// </summary>
	public void OnHintClick ()
	{
		GameConstants.coinsUsedForHints = uiScript.coinsRequiredForHint;
		for (int i = 0; i < activeHiddenObjects.Length; i++) {
			if (allHiddenObjects [activeHiddenObjects [i]].activeSelf) {
				HiddenObject hiddenObject = allHiddenObjects [activeHiddenObjects [i]].GetComponent<HiddenObject> ();
				Vector3 hintPos = allHiddenObjects [activeHiddenObjects [i]].transform.localPosition;
				hintPos += hiddenObject.hintOffset;
				hintGlowEffect = Instantiate (hintObjectPrefab, Vector3.zero, Quaternion.identity) as GameObject;
				hintGlowEffect.transform.SetParent (allHiddenObjects [activeHiddenObjects [i]].transform.parent, true);
				hintGlowEffect.transform.localPosition = hintPos;
				if (hiddenObject.mirrorHint) {
					hintGlowEffect.transform.localScale = new Vector3 (-hintGlowEffect.transform.localScale.x, hintGlowEffect.transform.localScale.y, hintGlowEffect.transform.localScale.z);
				}
				//hintGlowEffect.transform.parent = transform.GetChild(0);
				isHintActive = true;
				mainCamera.GetComponent<FocusCameraOnItem> ().FocusCameraOnTarget (hintGlowEffect.transform.position);
				//iTween.MoveTo (mainCamera.gameObject, hintGlowEffect.transform.position, 1f);
				//transform.GetChild(0).GetComponent<BackgroundDrag>().BringObjectIntoView(hintGlowEffect.transform.position);
				break;
			}
		}
			GameServicesHelper.UnlockAchievements(AchievementConstants.CLUE_CATCHER);
	
		hintsUsedThisLevel += 1;
		GameConstants.hintsUsedInLevel++;
	}




	public void reset ()
	{
		SceneManager.LoadScene ("Blank");
		SceneManager.LoadScene ("HiddenObjectScene");
	}





	public void backToMenu ()
	{
		SceneManager.LoadScene ("ChapterSelectionScreen");
	}

	public void addTime()
	{
		//_curState = GameState.Playing;
		GameConstants.coinsUsedForTimer = uiScript.coinsRequiredForHint;
		uiScript.hintAlreadyUsed = true;
		StoreConstants.totalCoins -= uiScript.coinsRequiredForHint;
		uiScript.coinsRequiredForHint = DynamicConstants.hintAlreadyUsedPrice;
		uiScript.hintsCoinText.text = uiScript.coinsRequiredForHint.ToString ();
		timer.StartTimer(25, timerText);
		timer.AddCallback(0, TimeOver);
		GameConstants.timerUsedInLevel++;
		uiScript.outOfTimePopup.SetActive (false);
	}

	public void pauseTime()
	{
		ChangeState(GameState.Paused);
	}

	public void resumeTime()
	{
		ChangeState (GameState.Playing);
	}
}
