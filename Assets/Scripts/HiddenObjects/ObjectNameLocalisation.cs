﻿using UnityEngine;
using System.Collections;

public class ObjectNameLocalisation : MonoBehaviour {

	// Use this for initialization
	 /// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	void OnEnable()
    {
        this.gameObject.GetComponent<TextLocalize>().key = "" + GameConstants.currentHiddenObjectLevelNumber;
    }
}
