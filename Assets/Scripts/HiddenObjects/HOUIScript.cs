﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Unity.Linq;
using UnityEngine.SceneManagement;
using BitBenderGames;

public class HOUIScript : MonoBehaviour
{

	public GameObject parentCanvas;

	public GameObject[] objectsArray = new GameObject[3];

	public GameObject[] selectionLineArray = new GameObject[3];

	public Text objectCountText;

	public GameObject outofCoinsPopup, outOfTimePopup;

	public GameObject outOfTimeNoCoinsPopup, outOfTimeCoinsPopup;

	public GameObject rewardPopup;

	public bool hintAlreadyUsed = false;

	public int coinsRequiredForHint;

	public Text hintsCoinText;

	public Text timerCoinText;

	public GameObject pauseObject;

	public Text gameStartText;

	public GameObject playPopup;

	public GameObject tutorial;

	public static bool isTimerNoCoinsPopOn = false;

	public static bool isTimerCoinsPopOn = false;

	static HOUIScript _houiScript;

	public static HOUIScript  houScript{
		get { return _houiScript; }
	}

	void OnEnable()
	{
		gameStartText.gameObject.SetActive(false);
		gameStartText.GetComponent<TextLocalize>().key = "hiddentext" + GameConstants.currentHiddenObjectLevelNumber.ToString();
		gameStartText.gameObject.SetActive(true);
		Messenger.AddListener<string>(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, showPause);
		Messenger.MarkAsPermanent(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY);

		Messenger.AddListener<string>(MessengerConstants.TUTORIAL_FINISHED, showPlayPopup);
		Messenger.MarkAsPermanent(MessengerConstants.TUTORIAL_FINISHED);

		Messenger.AddListener<string>(MessengerConstants.BACK_FROM_STORE, checkPopupStatus);
		Messenger.MarkAsPermanent(MessengerConstants.BACK_FROM_STORE);
	}

	void OnDisable()
	{
		Messenger.RemoveListener<string>(MessengerConstants.BACK_BUTTON_REVERSE_NOTIFY, showPause);
		Messenger.RemoveListener<string>(MessengerConstants.TUTORIAL_FINISHED, showPlayPopup);
		Messenger.AddListener<string>(MessengerConstants.BACK_FROM_STORE, checkPopupStatus);
	}

	void Start()
	{
		_houiScript = this;
		if (GameConstants.currentHiddenObjectLevelNumber == 1 || GameConstants.currentHiddenObjectLevelNumber == 7 || GameConstants.currentHiddenObjectLevelNumber == 13 || GameConstants.currentHiddenObjectLevelNumber == 19) {
			G2WBackButtonHelper.isTutorialOn = true;
			tutorial.SetActive (true);
			playPopup.SetActive (false);
		}
		coinsRequiredForHint = DynamicConstants.firstTimeHintPrice;
		hintsCoinText.text = coinsRequiredForHint.ToString();
		timerCoinText.text = coinsRequiredForHint.ToString ();
	}




	/// <summary>
	/// Change the count 
	/// </summary>
	/// <param name="objFound">Object found.</param>
	/// <param name="maxObj">Max object.</param>
	public void IncrObjectFind (int objFound, int maxObj)
	{
		objectCountText.text = objFound + "/" + maxObj;
	}

	/// <summary>
	/// Sets the name of the object to find in the label.
	/// </summary>
	/// <param name="index">Index.</param>
	/// <param name="objName">Object name.</param>
	public void SetObjectToFind (int index, string objName)
	{
		if (string.IsNullOrEmpty (objName)) {
			Image image = selectionLineArray [index].GetComponent<Image> ();
			//image.enabled = true;
			image.gameObject.SetActive (true);
		} else {
			Text label = objectsArray [index].GetComponent<Text> ();
			label.gameObject.SetActive(false);
            objectsArray[index].GetComponent<TextLocalize>().key = objName;
			 objectsArray[index].GetComponent<TextLocalize>().allCaps = true;
			label.gameObject.SetActive(true);
            Debug.Log("OBJ NAME SELECT TO FIND" + objectsArray[index].GetComponent<TextLocalize>().key + " " + objName);
		}
	}

	/// <summary>
	/// Callback handler for hint button
	/// </summary>
	public void OnClickOnUseHintBtn ()
	{
		//update hints
		if (HOGameplayScript.CurrentLevel.CurState == HOGameplayScript.GameState.Playing && !HOGameplayScript.CurrentLevel.isHintActive) {
			
			if (StoreConstants.totalCoins >= coinsRequiredForHint) {
				HOGameplayScript.CurrentLevel.OnHintClick ();
				StoreConstants.totalCoins -= coinsRequiredForHint;
				StoreConstants.totalAvailableHints--;
				//hintCountText.text = StoreConstants.totalAvailableHints.ToString ();
				//if (hintAlreadyUsed) {
					coinsRequiredForHint = DynamicConstants.hintAlreadyUsedPrice;
					hintsCoinText.text = coinsRequiredForHint.ToString ();
					timerCoinText.text = coinsRequiredForHint.ToString ();
				//} else {

					hintAlreadyUsed = true;
				//}

			} else {
				//SHOW POPUP
				onPauseClicked();
				HOGameplayScript.CurrentLevel.pauseTime();
				outofCoinsPopup.SetActive(true);
				G2WGAHelper.LogEvent("Out of Coins Popup - Shown",GameConstants.getStoryName()+"- Chapter ",GameConstants.currentChapterNumber.ToString(),1);
			}


		}
	}

	public void closeCoinsPopup()
	{
		G2WGAHelper.LogEvent("Out of Coins Popup - Close");
		outofCoinsPopup.SetActive(false);
	}


	public void showTimerOverPanel()
	{
		outOfTimePopup.SetActive(true);
		onPauseClicked ();
		if (hintAlreadyUsed) {
			
			hintsCoinText.text = coinsRequiredForHint.ToString ();
			timerCoinText.text = coinsRequiredForHint.ToString ();
		}
		if (StoreConstants.totalCoins > coinsRequiredForHint) {
			outOfTimeCoinsPopup.SetActive(true);
			outOfTimeNoCoinsPopup.SetActive (false);
		} else {
			isTimerNoCoinsPopOn = true;
			outOfTimeNoCoinsPopup.SetActive (true);
			outOfTimeCoinsPopup.SetActive (false);
		}

	}

	public void onPauseClicked()
	{
		if (HOGameplayScript.CurrentLevel._curState != HOGameplayScript.GameState.Briefing) {
			G2WBackButtonHelper.isPauseOn = true;
			HOGameplayScript.CurrentLevel.mainCamera.GetComponent<MobileTouchCamera> ().enabled = false;
			HOGameplayScript.CurrentLevel.pauseTime ();
			HOGameplayScript.CurrentLevel.bugPanel.SetActive (true);
			pauseObject.SetActive (true);
		}
	}

	public void onResumeClicked()
	{
		if (HOGameplayScript.CurrentLevel._curState != HOGameplayScript.GameState.Briefing) {
			G2WBackButtonHelper.isPauseOn = false;
			HOGameplayScript.CurrentLevel.mainCamera.GetComponent<MobileTouchCamera> ().enabled = true;
			HOGameplayScript.CurrentLevel.resumeTime ();
			Invoke ("HideBugPanel", 0.5f);
			pauseObject.SetActive (false);
		}
	}

	public void HideBugPanel()
	{
		HOGameplayScript.CurrentLevel.bugPanel.SetActive (false);
	}

	public void hideOutOfTimePanel()
	{
		outOfTimePopup.SetActive (false);	
	}

	public void hideOutOfCoinsPanel()
	{
		outofCoinsPopup.SetActive (false);
	}

	void OnApplicationPause (bool pauseStatus)
	{

		if (pauseStatus == true) {
			onPauseClicked ();
		}
	}

	public void showPause(string s)
	{
		if (s == "Pause") {
			onPauseClicked ();
		} else if (s == "Resume") {
			onResumeClicked ();
		}
	}

	public void showPlayPopup(string s)
	{
		playPopup.SetActive (true);
	}

	public void watchAVideoCoinsPopup()
	{
		//if (GameConstants.isNetConnected ()) {
			G2WAdHelper.ShowRewardedVideo (GameConstants.RVP_InGameCoins,"Story");
			GameServicesHelper.UnlockAchievements(AchievementConstants.AVID_OBSERVER);
		//}
	}

	public void watchAVideoTimerPopup()
	{
	//	if (GameConstants.isNetConnected ()) {
			G2WAdHelper.ShowRewardedVideo (GameConstants.RVP_InGameCoins,"Story");
			GameServicesHelper.UnlockAchievements(AchievementConstants.AVID_OBSERVER);
		//}
	}

	public void checkPopupStatus(string s)
	{
		if (StoreConstants.totalCoins > coinsRequiredForHint) {
			isTimerNoCoinsPopOn = false;
			outOfTimeCoinsPopup.SetActive(true);
			outOfTimeNoCoinsPopup.SetActive (false);
		} else {
			isTimerNoCoinsPopOn = true;
			outOfTimeNoCoinsPopup.SetActive (true);
			outOfTimeCoinsPopup.SetActive (false);
		}
	}



}
