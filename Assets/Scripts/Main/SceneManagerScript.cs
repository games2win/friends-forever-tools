﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Prime31;


public class SceneManagerScript : MonoBehaviour
{

    // Used for blocking the buttons in ChapterSelectionScreen when Store is added additively as a hack because buttons in the ChapterSelectionScreen are getting clicked in Store (Unity bug).
    public GameObject panel;

    public GameObject outOfCoinPopup;

    static SceneManagerScript _sceneManagerScript;


    void OnEnable()
    {
        Messenger.AddListener<string>(MessengerConstants.DISABLE_BACK_PANEL, hidePanel);
        Messenger.MarkAsPermanent(MessengerConstants.DISABLE_BACK_PANEL);
    }

    void OnDisable()
    {
        Messenger.RemoveListener<string>(MessengerConstants.DISABLE_BACK_PANEL, hidePanel);
    }


    void Start()
    {
        // StoreConstants.totalMoves = 0;
        _sceneManagerScript = this;
        GameConstants.currentGameState = GameStates.CHAPTER_SELECT;
        G2WGAHelper.LogEvent("AAID", "ids", G2WAdHelper.AD_ID, 1);

    }

    public static SceneManagerScript sceneManagerScript
    {
        get { return _sceneManagerScript; }
    }

    public void loadLevelSelection(int storyNumber)
    {
        if (GameConstants.checkIfStoryUnlocked(storyNumber))
        {
            GameConstants.currentGameState = GameStates.LEVEL_SELECT;
            GameConstants.currentStoryNumber = storyNumber;
            GameConstants.currentPlayingStoryNumber = storyNumber;
            G2WGAHelper.LogEvent("Select" + SelectedStoryName(storyNumber));
            SceneManager.LoadScene("LevelSelectionScreen");
        }
        else
        {
            storyStatus(storyNumber);
        }

    }

    public void storyStatus(int storyNumber)
    {
        if (StoreConstants.totalCoins >= DynamicConstants.CoinsForUnlockingStory(storyNumber))
        {
            StoreConstants.totalCoins -= DynamicConstants.CoinsForUnlockingStory(storyNumber);
            StoreConstants.totalMoves += DynamicConstants.MovesGivenForUnlockingStory(storyNumber);
            P31Prefs.setBool("Story" + storyNumber + "Unlocked", true);
            G2WGAHelper.LogEvent("Unlock Chapter " + storyNumber.ToString(), "Coin Balance", StoreConstants.totalCoins.ToString(), 1);
            G2WGAHelper.LogEvent("Unlock Chapter " + storyNumber.ToString(), "Level", GameConstants.getStoryName() + " : " + GameConstants.currentChapterNumber.ToString(), 1);
            loadLevelSelection(storyNumber);
        }
        else
        {
            showOutOfCoinsPopup();
        }
    }

    public void loadStore(string type)
    {
        G2WGAHelper.LogEvent("Open Store", "Source", "Story Selection Screen", 1);
        if (type == "coins")
        {
            StoreConstants.showCoinScreen = true;
        }
        else
        {
            StoreConstants.showCoinScreen = false;
        }
        panel.SetActive(true);
        SceneManager.LoadScene("Store", LoadSceneMode.Additive);

    }

    //Hides the panel used as hack for store bug.
    public void hidePanel(string s)
    {
        panel.SetActive(false);
    }

    public void showOutOfCoinsPopup()
    {
        outOfCoinPopup.SetActive(true);
    }

    public void hideOutofCoinsPopup()
    {
        outOfCoinPopup.SetActive(false);
    }

    public string SelectedStoryName(int storynumber)
    {
        switch (storynumber)
        {
            case 1:
                return "Crazy Teen Life";
                break;
            case 2:
                return "Latte. Love. Life.";
                break;
            case 3:
                return "The Gifted";
                break;
            case 4:
                return "Strangers & Second Chances";
                break;
            case 5:
                return "They're Watching...";
                break;
            case 6:
                return "Oh Hack No!";
                break;
            case 7:
                return "The Switch";
                break;
        }
        return "";
    }

    public void showAchievements()
    {
        if (GameConstants.isNetConnected())
        {
            GameServicesHelper.showAchievements();
        }
    }

    public void addValues(string s)
    {
        if (s == "coins")
        {
            StoreConstants.totalCoins += 100;
        }
        else
        {
            StoreConstants.totalMoves += 2;
            // StoreConstants.totalMoves = 0;
        }
    }

    public void startLandscape()
    {
        SceneManager.LoadScene("Story4Scene_Landscape");
    }

    public void comingSoonForGA()
    {

    }


}
