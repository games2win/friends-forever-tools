﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Prime31;

public class IntroScript : MonoBehaviour
{
    public GameObject exitGamePanel;

    void Start()
    {
        Debug.Log("GPGS Start Intro!");
        P31Prefs.setBool("Story2Unlocked", true);
        GameServicesHelper.authenticate();
        EtceteraAndroid.checkForNotifications();
        EtceteraAndroidManager.notificationReceivedEvent += OnNotificationReceived;
        forGA();
        if (!PlayerPrefs.HasKey("SoundStatus"))
        {
            PlayerPrefs.SetInt("SoundStatus", 1);
        }
        //		GameConstants.currentChapterNumber;
        /*if (!P31Prefs.hasKey ("isFirstTime")) {
			P31Prefs.setInt ("isFirstTime", 1);
			GameConstants.currentChapterNumber= 1;

           
			//StoreConstants.totalCoins = DynamicConstants.CoinsAwardedAtStart;
			//StoreConstants.totalMoves = DynamicConstants.MovesAwardedAtStart;
			//StoreConstants.totalMoves = 0;
			//Saving the state of each chapters where 0 is chapter not played and 1 will be chapter already played.
			for (int k = 1; k <= 2; k++) {
				for (int i = 1; i <= 30; i++) {
					//PlayerPrefs.SetInt ("Story" + k + "Chapter" + i, 0);
				}
			}
		}*/
        //REMOVE THIS
        // StoreConstants.totalCoins = 1000;
        //
#if UNITY_IOS
		//TODO :- Change the ID to iOS ID.
		FlurryAnalytics.startSession ("PKBXNB559M45WD457Y4G");
		FlurryAnalytics.logEvent ("Start Game", false);
#endif
        AppsFlyerInit();
    }

    void AppsFlyerInit()
    {

        //Mandatory - set your AppsFlyer’s Developer key.
        AppsFlyer.setAppsFlyerKey("KX7CkPaYvNVz9BhyMiaY7g");
        // For detailed logging
       // AppsFlyer.setIsDebug (true);
#if UNITY_IOS
//Mandatory - set your apple app ID
   //AppsFlyer.setAppID ("YOUR_APP_ID_HERE");
  //AppsFlyer.trackAppLaunch ();
#elif UNITY_ANDROID
        //Mandatory - set your Android package name
       AppsFlyer.setAppID("com.games2win.friendsforever");
        AppsFlyer.loadConversionData("AppsFlyerTrackerCallbacks");
#endif
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (exitGamePanel.activeInHierarchy)
            {
                Debug.Log("Exit : activeInHierarchy");
                GameConstants.isExitGamePopupActive = false;
                exitGamePanel.SetActive(false);
                G2WAdHelper.editorShow = "NO_AD";
                if (DeviceSize.isPhone())
                {
                    MoPub.showBanner(GameConstants.mopubBannerPhoneID, false);
                    MoPub.showBanner(GameConstants.mopubExitAdPhoneID, false);
                }
                else
                {
                    MoPub.showBanner(GameConstants.mopubBannerTabID, false);
                    MoPub.showBanner(GameConstants.mopubExitAdTabID, false);
                }
            }
            else
            {
                Debug.Log("Exit : InactiveInHierarchy");
                GameConstants.isExitGamePopupActive = true;
                exitGamePanel.SetActive(true);
                G2WAdHelper.editorShow = "EXIT_AD";
                if (DeviceSize.isPhone())
                {
                    MoPub.showBanner(GameConstants.mopubBannerPhoneID, false);
                    MoPub.showBanner(GameConstants.mopubExitAdPhoneID, true);
                }
                else
                {
                    MoPub.showBanner(GameConstants.mopubBannerTabID, false);
                    MoPub.showBanner(GameConstants.mopubExitAdTabID, true);
                }
            }
        }
#endif
    }

    public void ExitGameNo()
    {
#if UNITY_ANDROID
        exitGamePanel.SetActive(false);
        GameConstants.isExitGamePopupActive = false;
        if (DeviceSize.isPhone())
        {
            MoPub.showBanner(GameConstants.mopubBannerPhoneID, false);
            MoPub.showBanner(GameConstants.mopubExitAdPhoneID, false);
        }
        else
        {
            MoPub.showBanner(GameConstants.mopubBannerTabID, false);
            MoPub.showBanner(GameConstants.mopubExitAdTabID, false);
        }
#endif
    }

    public void ExitGameYes()
    {
        Application.Quit();
    }

    public void loadLevelSelection()
    {
        GameConstants.currentGameState = GameStates.CHAPTER_SELECT;
        StartCoroutine("loadChapter");
        
       //SceneManager.LoadScene("Story1Scene");
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            G2WGAHelper.LogEvent("Internet Off");
        }
        else
        {
            G2WGAHelper.LogEvent("Internet On");
        }
        G2WGAHelper.LogEvent("Start Game");
        G2WGAHelper.LogRichEvent("Start Game");
    }

    IEnumerator loadChapter()
    {
       yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("ChapterSelectionScreen");
    }

    public void loadScene(string storyNumber)
    {
        SceneManager.LoadScene("Story" + storyNumber + "Scene");
    }


    //To Segregate users according to the versions.
    // For updated users remove the old playerprefs key and put in the new key with latest version number. Users with old key will be Updated Users and one's without would be New Users.
    public void forGA()
    {
        if (!PlayerPrefs.HasKey("FF_1.6"))
        {
            if (PlayerPrefs.HasKey("FF_1.1") || PlayerPrefs.HasKey("FF_1.0") || PlayerPrefs.HasKey("FF_1.2") || PlayerPrefs.HasKey("FF_1.3") || PlayerPrefs.HasKey("FF_1.4")|| PlayerPrefs.HasKey("FF_1.5"))
            {
                G2WGAHelper.LogEvent("Updated Users");
            }
            else
            {
                G2WGAHelper.LogEvent("New Users");
            }
            PlayerPrefs.SetInt("FF_1.6", 1);

        }
        PlayerPrefs.Save();

        if (GameConstants.currentLanguage == "French") {
			G2WGAHelper.LogEvent("Game Language","Language",GameConstants.currentLanguage,1);
		} else if (GameConstants.currentLanguage == "Italian") {
			G2WGAHelper.LogEvent("Game Language","Language",GameConstants.currentLanguage,1);
		} else if (GameConstants.currentLanguage == "German") {
			G2WGAHelper.LogEvent("Game Language","Language",GameConstants.currentLanguage,1);
		} else if (GameConstants.currentLanguage == "Spanish") {
			G2WGAHelper.LogEvent("Game Language","Language",GameConstants.currentLanguage,1);
		}  else if (GameConstants.currentLanguage == "Portuguese") {
			G2WGAHelper.LogEvent("Game Language","Language",GameConstants.currentLanguage,1);
		} else {
			G2WGAHelper.LogEvent("Game Language","Language",GameConstants.currentLanguage,1);
		}

    }

    public void exitAdLink()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.games2win.drivetodate&referrer=utm_source%3Dbackfill%26utm_campaign%3Dexitad_in_ff");
    }

    public void OnNotificationReceived(string extras)
    {
        G2WGAHelper.LogEvent("App Open PNS","Level",GameConstants.getStoryName () + ": Chapter " + GameConstants.currentChapterNumber.ToString (), 1);	
    }
}
