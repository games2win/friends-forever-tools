﻿using UnityEngine;
using System.Collections;
using Prime31;

public class StoreConstants
{

	public static bool showCoinScreen = true;

	public static int totalCoins {
		get {
			

			return P31Prefs.getInt ("totalCoins", 0);

		}
		set {
			Debug.Log ("coins are not there"+ value);
			P31Prefs.setInt ("totalCoins", value);
			if (value < DynamicConstants.firstTimeHintPrice) {
				G2WGAHelper.LogEvent ("Coins Exhausted", GameConstants.getStoryName (), "HO " + GameConstants.currentChapterNumber.ToString (), 1);

			}
			Messenger.Broadcast<string,int> (MessengerConstants.VALUE_UPDATE, "coins", value);

			PlayerPrefs.Save ();
		}

	}

	public static int totalMoves {
		get {
			
			return P31Prefs.getInt ("totalMoves", 0);
		}
		set {
			
			P31Prefs.setInt ("totalMoves", value);
			if (value <= 0) {
				G2WGAHelper.LogEvent ("Moves Exhausted", GameConstants.getStoryName (), "Chapter " + GameConstants.currentChapterNumber.ToString (), 1);
			}
			Messenger.Broadcast<string,int> (MessengerConstants.VALUE_UPDATE, "passes", value);
			Debug.Log ("......settotalMoves " + value);
			PlayerPrefs.Save ();
		}

	}

	public static int totalAvailableHints {
		get {
			return PlayerPrefs.GetInt ("totalAvailableHints", 5);
		}
		set {
			PlayerPrefs.SetInt ("totalAvailableHints", value);
			Messenger.Broadcast<string,int> (MessengerConstants.VALUE_UPDATE, "hints", PlayerPrefs.GetInt ("totalAvailableHints"));
			PlayerPrefs.Save ();
		}

	}

	public static int CoinPack_Free {
		get {
			return PlayerPrefs.GetInt ("CoinPack_Free", 25);
		}
		set {
			PlayerPrefs.SetInt ("CoinPack_Free", value);
			PlayerPrefs.Save ();
		}
	}


	public static int CoinPack_1 {
		get {
			return PlayerPrefs.GetInt ("CoinPack_1", 250);
		}
		set {
			PlayerPrefs.SetInt ("CoinPack_1", value);
			PlayerPrefs.Save ();
		}
	}

	public static int CoinPack_2 {
		get {
			return PlayerPrefs.GetInt ("CoinPack_2", 600);
		}
		set {
			PlayerPrefs.SetInt ("CoinPack_2", value);
			PlayerPrefs.Save ();
		}
	}

	public static int CoinPack_3 {
		get {
			return PlayerPrefs.GetInt ("CoinPack_3", 1500);
		}
		set {
			PlayerPrefs.SetInt ("CoinPack_3", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovePack_Free {
		get {
			return PlayerPrefs.GetInt ("MovePack_Free", 2);
		}
		set {
			PlayerPrefs.SetInt ("MovePack_Free", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovePack_1 {
		get {
			return PlayerPrefs.GetInt ("MovePack_1", 15);
		}
		set {
			PlayerPrefs.SetInt ("MovePack_1", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovePack_2 {
		get {
			return PlayerPrefs.GetInt ("MovePack_2", 35);
		}
		set {
			PlayerPrefs.SetInt ("MovePack_2", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovePack_3 {
		get {
			return PlayerPrefs.GetInt ("MovePack_3", 75);
		}
		set {
			PlayerPrefs.SetInt ("MovePack_3", value);
			PlayerPrefs.Save ();
		}
	}

	public static int Rewarded_Moves {
		get {
			return PlayerPrefs.GetInt ("Rewarded_Moves", 2);
		}
		set {
			PlayerPrefs.SetInt ("Rewarded_Moves", value);
			PlayerPrefs.Save ();
		}
	}

	public static int Rewarded_Coins {
		get {
			return PlayerPrefs.GetInt ("Rewarded_Coins", 25);
		}
		set {
			PlayerPrefs.SetInt ("Rewarded_Coins", value);
			PlayerPrefs.Save ();
		}
	}

	


}
