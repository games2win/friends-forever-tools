﻿using UnityEngine;
using System.Collections;


public class MessengerConstants  {

///For Disabling the Back Panel for the Store Hack
	public static string DISABLE_BACK_PANEL = "DisableBackPanel";

///Notifies when the hidden object is clicked
	public static string OBJECT_CLICKED = "OjbectClicked";

///Notifies when there is a change in the value of Coin or Moves.
	public static string VALUE_UPDATE = "ValueUpdate";

///Notifies when the back button is tapped.
	public static string BACK_BUTTON_TAPPED = "BackButtonTapped";

///Notifies when the back button is tapped and we want to send a message from the BackHandler to any other object.
	public static string BACK_BUTTON_REVERSE_NOTIFY = "BackButtonReverse";

//Notifies HiddenObject UI Script when the tutorial is done.
	public static string TUTORIAL_FINISHED = "TutorialFinished";

//Notifies when the user has come back from the Store screen using the Timer out of coins popup
	public static string BACK_FROM_STORE = "BackFromStore";
//Broadcasts a message when the user taps on the sound button.
	public static string SOUND_CHANGED = "SOUND_CHANGED";

    public static string START_TIMER = "TimerStart";

	public static string AWARD_TIMER = "AwardTimer";
}
