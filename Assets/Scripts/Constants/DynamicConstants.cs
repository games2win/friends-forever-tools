﻿using UnityEngine;
using System.Collections;

public class DynamicConstants  {


	/// <summary>
	/// Gets or sets the price of the hint when used for the first time in level.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int firstTimeHintPrice {
		get {
			return PlayerPrefs.GetInt ("firstTimeHintPrice", 10);
		}
		set {
			PlayerPrefs.SetInt ("firstTimeHintPrice", value);
			PlayerPrefs.Save ();
		}
	}


	/// <summary>
	/// Gets or sets the price of the hint when used for the 2nd time or more in a level.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int hintAlreadyUsedPrice {
		get {
			return PlayerPrefs.GetInt ("hintAlreadyUsedPrice", 10);
		}
		set {
			PlayerPrefs.SetInt ("hintAlreadyUsedPrice", value);
			PlayerPrefs.Save ();
		}
	}


	/// <summary>
	/// Gets or sets the amount of coins awarded at level end.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int levelEndCoinsAwarded {
		get {
			return PlayerPrefs.GetInt ("levelEndCoinsAwarded", 5);
		}
		set {
			PlayerPrefs.SetInt ("levelEndCoinsAwarded", value);
			PlayerPrefs.Save ();
		}
	}

	/// <summary>
	/// Gets or sets the amount of coins awarded at the start for the first time.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int CoinsAwardedAtStart {
		get {
			return PlayerPrefs.GetInt ("CoinsAwardedAtStart", 25);
		}
		set {
			PlayerPrefs.SetInt ("CoinsAwardedAtStart", value);
			PlayerPrefs.Save ();
		}
	}

	/// <summary>
	/// Gets or sets the amount of moves awarded at the start for the first time.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int MovesAwardedAtStart {
		get {
			return PlayerPrefs.GetInt ("MovesAwardedAtStart", 15);
		}
		set {
			PlayerPrefs.SetInt ("MovesAwardedAtStart", value);
			PlayerPrefs.Save ();
		}
	}

	/// <summary>
	/// Gets or sets the amount required for unlocking a dialouge.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int CoinsRequiredForDialgoue {
		get {
			return PlayerPrefs.GetInt ("CoinsRequiredForDialgoue", 50);
		}
		set {
			PlayerPrefs.SetInt ("CoinsRequiredForDialgoue", value);
			PlayerPrefs.Save ();
		}
	}

	/// <summary>
	/// Coins for unlocking new story.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int CoinsForUnlockingStory1 {
		get {
			return PlayerPrefs.GetInt ("CoinsForUnlockingStory1", 100);
		}
		set {
			PlayerPrefs.SetInt ("CoinsForUnlockingStory1", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovesGivenForUnlockingStory1 {
		get {
			return PlayerPrefs.GetInt ("MovesGivenForUnlockingStory1", 4);
		}
		set {
			PlayerPrefs.SetInt ("MovesGivenForUnlockingStory1", value);
			PlayerPrefs.Save ();
		}
	}

	/// <summary>
	/// Coins for unlocking new story.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int CoinsForUnlockingStory2 {
		get {
			return PlayerPrefs.GetInt ("CoinsForUnlockingStory2", 0);
		}
		set {
			PlayerPrefs.SetInt ("CoinsForUnlockingStory2", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovesGivenForUnlockingStory2 {
		get {
			return PlayerPrefs.GetInt ("MovesGivenForUnlockingStory2", 4);
		}
		set {
			PlayerPrefs.SetInt ("MovesGivenForUnlockingStory2", value);
			PlayerPrefs.Save ();
		}
	}

	public static int CoinsForUnlockingStory3 {
		get {
			return PlayerPrefs.GetInt ("CoinsForUnlockingStory3", 140);
		}
		set {
			PlayerPrefs.SetInt ("CoinsForUnlockingStory3", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovesGivenForUnlockingStory3 {
		get {
			return PlayerPrefs.GetInt ("MovesGivenForUnlockingStory3", 4);
		}
		set {
			PlayerPrefs.SetInt ("MovesGivenForUnlockingStory3", value);
			PlayerPrefs.Save ();
		}
	}

		public static int CoinsForUnlockingStory4 {
		get {
			return PlayerPrefs.GetInt ("CoinsForUnlockingStory4", 120);
		}
		set {
			PlayerPrefs.SetInt ("CoinsForUnlockingStory4", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovesGivenForUnlockingStory4 {
		get {
			return PlayerPrefs.GetInt ("MovesGivenForUnlockingStory4", 4);
		}
		set {
			PlayerPrefs.SetInt ("MovesGivenForUnlockingStory4", value);
			PlayerPrefs.Save ();
		}
	}

		public static int CoinsForUnlockingStory5 {
		get {
			return PlayerPrefs.GetInt ("CoinsForUnlockingStory5", 160);
		}
		set {
			PlayerPrefs.SetInt ("CoinsForUnlockingStory5", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovesGivenForUnlockingStory5{
		get {
			return PlayerPrefs.GetInt ("MovesGivenForUnlockingStory5", 4);
		}
		set {
			PlayerPrefs.SetInt ("MovesGivenForUnlockingStory5", value);
			PlayerPrefs.Save ();
		}
	}

		public static int CoinsForUnlockingStory6 {
		get {
			return PlayerPrefs.GetInt ("CoinsForUnlockingStory6", 180);
		}
		set {
			PlayerPrefs.SetInt ("CoinsForUnlockingStory6", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovesGivenForUnlockingStory6{
		get {
			return PlayerPrefs.GetInt ("MovesGivenForUnlockingStory6", 4);
		}
		set {
			PlayerPrefs.SetInt ("MovesGivenForUnlockingStory6", value);
			PlayerPrefs.Save ();
		}
	}

		public static int CoinsForUnlockingStory7 {
		get {
			return PlayerPrefs.GetInt ("CoinsForUnlockingStory7", 70);
		}
		set {
			PlayerPrefs.SetInt ("CoinsForUnlockingStory7", value);
			PlayerPrefs.Save ();
		}
	}

	public static int MovesGivenForUnlockingStory7{
		get {
			return PlayerPrefs.GetInt ("MovesGivenForUnlockingStory7", 4);
		}
		set {
			PlayerPrefs.SetInt ("MovesGivenForUnlockingStory7", value);
			PlayerPrefs.Save ();
		}
	}


	public static int freeMovesTime {
		get {
			return PlayerPrefs.GetInt ("freeMovesTime", 4);
		}
		set {
			PlayerPrefs.SetInt ("freeMovesTime", value);
			PlayerPrefs.Save ();
		}
	}

	public static int timerMovesAward {
		get {
			return PlayerPrefs.GetInt ("timerMovesAward", 4);
		}
		set {
			PlayerPrefs.SetInt ("timerMovesAward", value);
			PlayerPrefs.Save ();
		}
	}

	public static string timerText {
		get {
			return PlayerPrefs.GetString ("timerText", "Your free moves have been added!");
		}
		set {
			PlayerPrefs.SetString ("timerText", value);
			PlayerPrefs.Save ();
		}
	}

    public static int MovesGivenForUnlockingStory(int storynumber)
    {
        return PlayerPrefs.GetInt("MovesGivenForUnlockingStory" + storynumber.ToString());
    }

	 public static int CoinsForUnlockingStory(int storynumber)
    {
        return PlayerPrefs.GetInt("CoinsForUnlockingStory" + storynumber.ToString());
    }

	/// <summary>
	/// Gets or sets the amount required for unlocking an accessory.
	/// </summary>
	/// <value>Coin amount.</value>
	public static int CoinsRequiredForAccessory {
		get {
			return PlayerPrefs.GetInt ("CoinsRequiredForAccessory", 50);
		}
		set {
			PlayerPrefs.SetInt ("CoinsRequiredForAccessory", value);
			PlayerPrefs.Save ();
		}
}

public static int offlinePlaysAllowed {
		get {
			return PlayerPrefs.GetInt ("offlinePlaysAllowed", 4);
		}
		set {
			PlayerPrefs.SetInt ("offlinePlaysAllowed", value);
			PlayerPrefs.Save ();
		}
	}

}
