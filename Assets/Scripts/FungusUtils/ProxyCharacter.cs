﻿using UnityEngine;
using System.Collections;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ProxyCharacter : StoredCharacter {

	public string ProxiedIdentifier;
	private StoredCharacter _original;
	public StoredCharacter Original
	{
		get { return _original; }
		internal set
		{
			_original = value;
			CopyValuesFromOriginal();
		}
	}
	protected override void OnEnable()
	{
		if (string.IsNullOrEmpty(ProxiedIdentifier))
		{
			return;
		}
		foreach (StoredCharacter result in activeCharacters.OfType<StoredCharacter>())
		{
			if (result.Identifier == ProxiedIdentifier)
			{
				this.Original = result;
				Debug.Log ("PROXY CHARACTER 2" + nameText + Original.NameText + _original.nameText);
			}
		}
		base.OnEnable();
	}
	private void CopyValuesFromOriginal()
	{
		nameText = Original.nameText;
		nameColor = Original.nameColor;
		soundEffect = Original.soundEffect;
		//profileSprite = Original.profileSprite;
		portraits = Original.portraits;
		portraitsFace = Original.portraitsFace;
		//state = Original.state;

		setSayDialog = Original.setSayDialog;

		Debug.Log ("PROXY CHARACTER" + nameText + Original.NameText + _original.nameText);
		//description = Original.GetDescription;
	}
	#if UNITY_EDITOR
	[CustomEditor(typeof(ProxyCharacter))]
	public class ProxyCharacterEditor : UnityEditor.Editor
	{
		override public void OnInspectorGUI()
		{
			ProxyCharacter myTarget = (ProxyCharacter)target;
			myTarget.ProxiedIdentifier = EditorGUILayout.TextField("Proxied Identifier", myTarget.ProxiedIdentifier);
			myTarget.Identifier = EditorGUILayout.TextField("Self Identifier", myTarget.Identifier);
		}
	}
	#endif
}
