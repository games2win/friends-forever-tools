﻿using UnityEngine;
using System.Collections;
using Fungus;
using System.Linq;

public class StoredCharacter : Fungus.Character {

	public string Identifier;

    public string Identifier2;

    protected override void OnEnable()
	{
		base.OnEnable();
		if (string.IsNullOrEmpty(Identifier))
		{
			return;
		}

		if(Identifier2!=null)
		{
            // Identifier2.Replace("-S2_proxy", "_proxy");
            Identifier2 = Identifier;
        }
		
		foreach (ProxyCharacter result in activeCharacters.OfType<ProxyCharacter>())
		{
			if (result.ProxiedIdentifier == Identifier)
			{
                result.Original = this;
			}
			else if (result.ProxiedIdentifier == Identifier2)
			{
				result.Original = this;
			}
		}
	}
}
