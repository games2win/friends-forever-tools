﻿// This code is part of the Fungus library (http://fungusgames.com) maintained by Chris Gregan (http://twitter.com/gofungus).
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace Fungus.EditorUtils
{
	[CustomEditor (typeof(NewSortMultiLayerCommand))]
	public class NewSortMultiLayerEditor : CommandEditor
	{
		public static bool showTagHelp;
		public Texture2D blackTex;

		public static void DrawTagHelpLabel()
		{
			//            string tagsText = TextTagParser.GetTagHelp();
			//
			//            if (CustomTag.activeCustomTags.Count > 0)
			//            {
			//                tagsText += "\n\n\t-------- CUSTOM TAGS --------";
			//                List<Transform> activeCustomTagGroup = new List<Transform>();
			//                foreach (CustomTag ct in CustomTag.activeCustomTags)
			//                {
			//                    if(ct.transform.parent != null)
			//                    {
			//                        if (!activeCustomTagGroup.Contains(ct.transform.parent.transform))
			//                        {
			//                            activeCustomTagGroup.Add(ct.transform.parent.transform);
			//                        }
			//                    }
			//                    else
			//                    {
			//                        activeCustomTagGroup.Add(ct.transform);
			//                    }
			//                }
			//                foreach(Transform parent in activeCustomTagGroup)
			//                {
			//                    string tagName = parent.name;
			//                    string tagStartSymbol = "";
			//                    string tagEndSymbol = "";
			//                    CustomTag parentTag = parent.GetComponent<CustomTag>();
			//                    if (parentTag != null)
			//                    {
			//                        tagName = parentTag.name;
			//                        tagStartSymbol = parentTag.TagStartSymbol;
			//                        tagEndSymbol = parentTag.TagEndSymbol;
			//                    }
			//                    tagsText += "\n\n\t" + tagStartSymbol + " " + tagName + " " + tagEndSymbol;
			//                    foreach(Transform child in parent)
			//                    {
			//                        tagName = child.name;
			//                        tagStartSymbol = "";
			//                        tagEndSymbol = "";
			//                        CustomTag childTag = child.GetComponent<CustomTag>();
			//                        if (childTag != null)
			//                        {
			//                            tagName = childTag.name;
			//                            tagStartSymbol = childTag.TagStartSymbol;
			//                            tagEndSymbol = childTag.TagEndSymbol;
			//                        }
			//                            tagsText += "\n\t      " + tagStartSymbol + " " + tagName + " " + tagEndSymbol;
			//                    }
			//                }
			//            }
			//            tagsText += "\n";
			//            float pixelHeight = EditorStyles.miniLabel.CalcHeight(new GUIContent(tagsText), EditorGUIUtility.currentViewWidth);
			//            EditorGUILayout.SelectableLabel(tagsText, GUI.skin.GetStyle("HelpBox"), GUILayout.MinHeight(pixelHeight));
		}

		protected SerializedProperty characterProp;
		protected SerializedProperty characterProp2;
		protected SerializedProperty characterProp3;
		protected SerializedProperty characterProp4;
		protected SerializedProperty characterProp5;
		protected SerializedProperty characterProp6;
		protected SerializedProperty characterProp7;
		protected SerializedProperty characterProp8;
		protected SerializedProperty characterProp9;
		protected SerializedProperty characterProp10;
		protected SerializedProperty characterProp11;
		protected SerializedProperty characterProp12;

		protected virtual void OnEnable()
		{
			if (NullTargetCheck()) // Check for an orphaned editor instance
				return;

			characterProp = serializedObject.FindProperty("character");
			characterProp2 = serializedObject.FindProperty("character2");
			characterProp3 = serializedObject.FindProperty("character3");
			characterProp4 = serializedObject.FindProperty("character4");
			characterProp5 = serializedObject.FindProperty("character5");
			characterProp6 = serializedObject.FindProperty("character6");
			characterProp7 = serializedObject.FindProperty("character7");
			characterProp8 = serializedObject.FindProperty("character8");
			characterProp9 = serializedObject.FindProperty("character9");
			characterProp10 = serializedObject.FindProperty("character10");
			characterProp11 = serializedObject.FindProperty("character11");
			characterProp12 = serializedObject.FindProperty("character12");


			if (blackTex == null)
			{
				blackTex = CustomGUI.CreateBlackTexture();
			}
		}

		protected virtual void OnDisable()
		{
			DestroyImmediate(blackTex);
		}

		public override void DrawCommandGUI() 
		{
			serializedObject.Update();

			bool showPortraits = false;
			CommandEditor.ObjectField<Character>(characterProp,
				new GUIContent("Character", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp2,
				new GUIContent("Character2", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp2.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp2.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp3,
				new GUIContent("Character3", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp3.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp3.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp4,
				new GUIContent("Character4", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp4.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp4.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp5,
				new GUIContent("Character5", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp5.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp5.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp6,
				new GUIContent("Character6", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp6.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp6.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp7,
				new GUIContent("Character7", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp7.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp7.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp8,
				new GUIContent("Character8", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp8.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp8.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp9,
				new GUIContent("Character9", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp9.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp9.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp10,
				new GUIContent("Character10", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp10.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp10.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp11,
				new GUIContent("Character11", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp11.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp11.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();

			CommandEditor.ObjectField<Character>(characterProp12,
				new GUIContent("Character12", "Character that is speaking"),
				new GUIContent("<None>"),
				Character.ActiveCharacters);

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(" ");
			characterProp12.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp12.objectReferenceValue, typeof(Character), true);
			EditorGUILayout.EndHorizontal();


			serializedObject.ApplyModifiedProperties();
		}
	}    
}