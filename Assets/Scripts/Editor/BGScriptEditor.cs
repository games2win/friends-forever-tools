﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BGScript))]
[CanEditMultipleObjects]
public class BGScriptEditor : Editor
{
    SerializedProperty bgnameProp;

	 /// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	void OnEnable()
	{
        bgnameProp = serializedObject.FindProperty("bgName");
		
    }
    public override void OnInspectorGUI()
    {
		serializedObject.Update ();
        DrawDefaultInspector();
        
        BGScript myScript = (BGScript)target;
        if(GUILayout.Button("Copy BGName"))
        {
             myScript.copyName();
        }

		//  if(GUILayout.Button("LoadBG"))
        // {
        //     myScript.loadBG();
        // }

		 serializedObject.ApplyModifiedProperties ();
    }
}
