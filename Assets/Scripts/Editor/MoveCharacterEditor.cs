// This code is part of the Fungus library (http://fungusgames.com) maintained by Chris Gregan (http://twitter.com/gofungus).
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace Fungus.EditorUtils
{
    [CustomEditor (typeof(MoveCharacterCommand))]
    public class MoveCharacterEditor : CommandEditor
    {
       
        protected SerializedProperty characterProp;
		protected SerializedProperty targetObjectProp;
		protected SerializedProperty durationObjectProp;
		protected SerializedProperty easeTypeObjectProp;
		protected SerializedProperty loopTypeObjectProp;
		protected SerializedProperty stopPreviousTweensObjectProp;
		protected SerializedProperty waitUntilFinishedObjectProp;
		protected SerializedProperty toTransformObjectProp;
		protected SerializedProperty toPositionObjectProp;
		protected SerializedProperty isLocalObjectProp;
		protected SerializedProperty totalCharactersInSceneProp;
		protected SerializedProperty characterPositionInSceneProp;

        protected virtual void OnEnable()
        {
            if (NullTargetCheck()) // Check for an orphaned editor instance
                return;

            characterProp = serializedObject.FindProperty("character");
			targetObjectProp = serializedObject.FindProperty("_targetObject");
			durationObjectProp = serializedObject.FindProperty("_duration");
			easeTypeObjectProp = serializedObject.FindProperty("easeType");
			loopTypeObjectProp = serializedObject.FindProperty("loopType");
			stopPreviousTweensObjectProp = serializedObject.FindProperty("stopPreviousTweens");
			waitUntilFinishedObjectProp = serializedObject.FindProperty("waitUntilFinished");
			toTransformObjectProp = serializedObject.FindProperty("_toTransform");
			toPositionObjectProp = serializedObject.FindProperty("_toPosition");
			isLocalObjectProp = serializedObject.FindProperty("isLocal");
			totalCharactersInSceneProp = serializedObject.FindProperty ("totalCharactersInScene");
			characterPositionInSceneProp = serializedObject.FindProperty ("characterPositionInScene");
        }
        
        protected virtual void OnDisable()
        {

        }

        public override void DrawCommandGUI() 
        {
            serializedObject.Update();

            CommandEditor.ObjectField<Character>(characterProp,
                                                new GUIContent("Character", "Character that is speaking"),
                                                new GUIContent("<None>"),
                                                Character.ActiveCharacters);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(" ");
            characterProp.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp.objectReferenceValue, typeof(Character), true);
            EditorGUILayout.EndHorizontal();

//			CommandEditor.ObjectField<Character>(targetObjectProp,
//				new GUIContent("Target Object", "Character that has to be moved"),
//				new GUIContent("<None>"),
//				Character.ActiveCharacters);

//			EditorGUILayout.BeginHorizontal();
//			EditorGUILayout.PrefixLabel("TargetObject");
//			targetObjectProp.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp.objectReferenceValue, typeof(Character), true);
//			EditorGUILayout.EndHorizontal();

			//EditorGUILayout.PropertyField(targetObjectProp);
			EditorGUILayout.PropertyField(durationObjectProp);
			EditorGUILayout.PropertyField(easeTypeObjectProp);
			EditorGUILayout.PropertyField(loopTypeObjectProp);
			EditorGUILayout.PropertyField(stopPreviousTweensObjectProp);
			EditorGUILayout.PropertyField(waitUntilFinishedObjectProp);
			EditorGUILayout.PropertyField(toTransformObjectProp);
			EditorGUILayout.PropertyField(toPositionObjectProp);
			EditorGUILayout.PropertyField(isLocalObjectProp);
			EditorGUILayout.PropertyField (totalCharactersInSceneProp);
			EditorGUILayout.PropertyField (characterPositionInSceneProp);
			//EditorGUILayout.PropertyField(secondaryanimatorProp);
			//string[] displayLabels = StringFormatter.FormatEnumNames(t.,"<None>");

//			CommandEditor.ObjectField<>(primaryanimatorProp,
//				new GUIContent("PrimaryAnimType", "Character that is speaking"),
//				new GUIContent("<None>"),
//				SetAnimationCommand.getPrimaryAnimTypes());

//			EditorGUILayout.BeginHorizontal();
//			EditorGUILayout.PrefixLabel(" ");
//			characterProp.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp.objectReferenceValue, typeof(Character), true);
//			EditorGUILayout.EndHorizontal();


          
            serializedObject.ApplyModifiedProperties();
        }
    }    
}