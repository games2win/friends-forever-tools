// This code is part of the Fungus library (http://fungusgames.com) maintained by Chris Gregan (http://twitter.com/gofungus).
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace Fungus.EditorUtils
{
    [CustomEditor(typeof(SetAnimationCommand))]
    public class SetAnimationEditor : CommandEditor
    {

        protected SerializedProperty characterProp;
        protected SerializedProperty usemultipleanimationsProp;
        protected SerializedProperty primaryanimatorProp;
        protected SerializedProperty secondaryanimatorProp;
        protected SerializedProperty durationProp;
        protected SerializedProperty isflippedProp;

        protected virtual void OnEnable()
        {
            if (NullTargetCheck()) // Check for an orphaned editor instance
                return;

            characterProp = serializedObject.FindProperty("character");
            usemultipleanimationsProp = serializedObject.FindProperty("useMultipleAnimation");
            primaryanimatorProp = serializedObject.FindProperty("primaryAnimation");
            secondaryanimatorProp = serializedObject.FindProperty("secondaryAnimation");
            durationProp = serializedObject.FindProperty("_duration");
            isflippedProp = serializedObject.FindProperty("isFlipped");
        }

        protected virtual void OnDisable()
        {

        }

        public override void DrawCommandGUI()
        {
            serializedObject.Update();

            CommandEditor.ObjectField<Character>(characterProp,
                                                new GUIContent("Character", "Character that is speaking"),
                                                new GUIContent("<None>"),
                                                Character.ActiveCharacters);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(" ");
            characterProp.objectReferenceValue = (Character)EditorGUILayout.ObjectField(characterProp.objectReferenceValue, typeof(Character), true);
            EditorGUILayout.EndHorizontal();


            SetAnimationCommand t = target as SetAnimationCommand;

            EditorGUILayout.PropertyField(usemultipleanimationsProp);
            if (t.UseMultipleAnimation)
            {
                EditorGUILayout.PropertyField(primaryanimatorProp);
            }
            EditorGUILayout.PropertyField(secondaryanimatorProp);
            EditorGUILayout.PropertyField(durationProp);
            EditorGUILayout.PropertyField(isflippedProp);
            //string[] displayLabels = StringFormatter.FormatEnumNames(t.,"<None>");

            //			CommandEditor.ObjectField<>(primaryanimatorProp,
            //				new GUIContent("PrimaryAnimType", "Character that is speaking"),
            //				new GUIContent("<None>"),
            //				SetAnimationCommand.getPrimaryAnimTypes());

            //			EditorGUILayout.BeginHorizontal();
            //			EditorGUILayout.PrefixLabel(" ");
            //			characterProp.objectReferenceValue = (Character) EditorGUILayout.ObjectField(characterProp.objectReferenceValue, typeof(Character), true);
            //			EditorGUILayout.EndHorizontal();



            serializedObject.ApplyModifiedProperties();
        }
    }
}