﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class YoyoHeadImage : MonoBehaviour {

	public GameObject yoyoObject;

	RawImage yoyoImage;

	string yoyoClickURL;
	void Start () {
		yoyoImage = gameObject.GetComponent<RawImage>();
		if (YoYocaching.yoyoLoaded)//if (YoyoImageCache.bUrlLoad)
			{
				//yoyoImage.texture = YoyoImageCache.wwwUrl.texture;
				//yoyoObject.renderer.material.mainTexture = yoyoImage;
				
			if (YoYocaching.yoyoLoaded)//if (YoyoImageCache.bUrlLoad)
				showYoYo ();
				// new YOYO (piyush)
				//yoyoImage.texture = YoyoImageCache.wwwUrl.texture;
				else yoyoObject.gameObject.SetActive (false);

			if(yoyoImage.texture == null)
				yoyoObject.SetActive(false);
			}
			else yoyoObject.transform.GetComponentInChildren<Transform>().gameObject.SetActive (false);

	}

	public void OpenURL()
	{
		var dict = new Dictionary<string,string>();
		dict.Add( "Properties", yoyoClickURL);
		if (!externalLinkclicked) {
		//	FlurryHelper.LogEvent ("Promo Clicked", dict);
			int currentYoYo = PlayerPrefs.GetInt ("currentyoyocount", 0);
			if (currentYoYo > PlayerPrefs.GetInt ("yoyoCount", 0) - 1)
				currentYoYo = 0;
			string yoyoimageurl = PlayerPrefs.GetString ("yoyo_image_url" + currentYoYo.ToString ());
			G2WGAHelper.LogEvent ("Promo Clicked", "URL", yoyoClickURL, 1);
			G2WGAHelper.LogRichEvent("YoYo");
			externalLinkclicked=true;
		}
		Application.OpenURL(yoyoClickURL);

	}


	bool externalLinkclicked = false;
	void OnEnable()
	{
		externalLinkclicked = false;
	}
	
	void OnApplicationFocus (bool focus)
	{
		if (!focus) 
		{
			externalLinkclicked = true;
		} 
		else
		{
			externalLinkclicked = false;
		}
	}

	public void showYoYo ()
	{
		int currentYoYo = PlayerPrefs.GetInt ("currentyoyocount", 0);

		//		Debug.Log ("yoyoCount " + PlayerPrefs.GetInt ("yoyoCount", 0));
		//		Debug.Log ("currentyoyocount " + PlayerPrefs.GetInt ("currentyoyocount", 0));

		if (currentYoYo > PlayerPrefs.GetInt ("yoyoCount", 0) - 1)
			currentYoYo = 0;

		byte[] yoyoData = File.ReadAllBytes (Application.persistentDataPath + "/yoyoImg_" + currentYoYo.ToString () + ".yoyo");

		Texture2D yoyoTex = new Texture2D(1,1);
		yoyoTex.LoadImage (yoyoData);
		yoyoImage.texture = yoyoTex;

		yoyoClickURL = PlayerPrefs.GetString ("yoyo_click_url" + currentYoYo.ToString ());// PlayerPrefs.SetString ("yoyo_click_url" + i.ToString (), yoyo_ ["click_url"].ToString ());
		currentYoYo++;
		PlayerPrefs.SetInt ("currentyoyocount", currentYoYo);
	}
}