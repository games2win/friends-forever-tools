﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;
using Fungus;

[CommandInfo("G2W",
    "Broadcast",
    "Broadcasts a message")]
[AddComponentMenu("")]
[ExecuteInEditMode]
public class BroadcastMessengerCommand : Command
{

    [Tooltip("Name of the message")]
    [SerializeField]
    public string msgName = "";
    [SerializeField]
    public string listenerName = "WearDress";

    

    public override void OnEnter()
    {
        Messenger.Broadcast<string>(listenerName, msgName);
        Continue();
    }

	 public override string GetSummary()
        {
            return msgName;
        }

}
