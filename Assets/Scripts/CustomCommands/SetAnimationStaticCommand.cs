﻿using UnityEngine;
using System.Collections;
using Fungus;
using System.Collections.Generic;
using System.Linq;

[CommandInfo("G2W",
    "Set Anim Static",
    "Sets a trigger parameter on an Animator component to control a Unity animation edited for using multiple animations at once")]
[AddComponentMenu("")]
[ExecuteInEditMode]
public class SetAnimationStaticCommand : Command
{

    [Tooltip("Character to display")]
    [SerializeField]
	public Character character;

    private AnimatorData _animator;

    private StringData _parameterNamePrimary;

    private StringData _parameterNameSecondary;

    [Tooltip("Check if using multiple animations")]
    [SerializeField]
	public bool useMultipleAnimation = true;

    public virtual bool UseMultipleAnimation { get { return useMultipleAnimation; } }

    [Tooltip("The shape of the easing curve applied to the animation")]
    [SerializeField]
    public PrimaryAnimTypes primaryAnimation = PrimaryAnimTypes.MouthSpeaking;

    public virtual PrimaryAnimTypes PrimaryAnimation { get { return primaryAnimation; } }

    [Tooltip("The shape of the easing curve applied to the animation")]
    [SerializeField]
	public SecondaryAnimTypes secondaryAnimation = SecondaryAnimTypes.Idle1;

    [Tooltip("Flip character.")]
    [SerializeField]
    protected bool isFlipped = false;

    [Tooltip("Duration to wait for")]
    [SerializeField]
	public FloatData _duration = new FloatData(0.5f);


    public virtual SecondaryAnimTypes SecondaryAnimation { get { return secondaryAnimation; } }

    /// <summary>
    /// Character to display.
    /// </summary>
    public virtual Character _Character { get { return character; } set { character = value; } }



    #region Public members

    public enum PrimaryAnimTypes
    {
        MouthAnger,
        MouthBored,
        MouthDisgust,
        MouthHappiness,
        MouthNone,
        MouthSpeaking,
        MouthSpeakingShort,
        MouthExtraShortSpeaking
    }

    public enum SecondaryAnimTypes
    {

         Anger,
		AgreeGirl_ArroganceBoy,
        Arrogance,
        Blushing,
		Blushing2Girl_EmbarrasedBoy,
		BoredBoy,
		Disgust,
		EyeWink,
		Idle1,
		Idle2,
		IdleGirl_ThinkingBoy_Static,
		EmbarassmentGirl_Static,
		Happiness,
		EmbarrassedBoy_Static,
		RaisedEyebrow_Static,
		ThinkingGirl_Static,
		EnjoyingMusic_Static,
		RaisedEyebrow,
		Laughter,
        Greeting,
     	IrritatedGirl_IgnoreBoy,
        Mischief,
		Sadness,
        Smiling,
        SurpriseBoy,
		Talking,
		Surprise2Girl,
		Victorious,
        Walking,
		Fainting,
		GiveSomething,
		TakeSomething,
		GiveKiss,
		TakeKiss,
        GiveCoffee,
        GiveHD,
		GiveMap,
		GivePendulum,
		GivePendant,
		GiveAmulet,
		GiveHealthBar,
		GiveVoodooDoll,
 		GiveChips,
		GiveRingBox,
		GiveSwitch,
 		TakeCoffee,
 		TakeHD,
		TakeChips,
		TakeAmulet,
		TakePendant,
		TakePendulum,
		TakeMap,
		TakeHealthBar,
		TakeRingBox,
		FoldedHand,
		HappyNod,
		GoofedupNod,
		HandOnChin,
		Sitting,
		StandUp,
		GiveFoodPacket,
		GiveSkinnySalad,
		GiveOrangeJuice,
		GiveGifts,
		GivePapers,
		TakeFoodPacket,
		TakeSkinnySalad,
		TakeOrangeJuice,
		TakeGifts,
		TakePapers,
		Takephone,
		TakePics,
		TakeAssignmentPprs,
		GivePhone,
		GivePics,
		GiveAssignment


    }

    #endregion

    void GetSecondaryAnimFunction()
    {
        switch (secondaryAnimation)
        {
            case SecondaryAnimTypes.Anger:
                _parameterNameSecondary.Value = "Anger";
                break;
            case SecondaryAnimTypes.Arrogance:
                _parameterNameSecondary.Value = "Arrogance";
                break;
            case SecondaryAnimTypes.Blushing:
                _parameterNameSecondary.Value = "Blushing";
                break;
		case SecondaryAnimTypes.BoredBoy:
                _parameterNameSecondary.Value = "Bored";
                break;
            case SecondaryAnimTypes.EmbarrassedBoy_Static:
                _parameterNameSecondary.Value = "Confusion";
                break;
            case SecondaryAnimTypes.AgreeGirl_ArroganceBoy:
                _parameterNameSecondary.Value = "Disagree";
                break;
            case SecondaryAnimTypes.Disgust:
                _parameterNameSecondary.Value = "Disgust";
                break;
            case SecondaryAnimTypes.Blushing2Girl_EmbarrasedBoy:
                _parameterNameSecondary.Value = "Embarrassment";
                break;
            case SecondaryAnimTypes.EyeWink:
                _parameterNameSecondary.Value = "EyeWink";
                break;
            case SecondaryAnimTypes.EmbarassmentGirl_Static:
                _parameterNameSecondary.Value = "FearWorry";
                break;
            case SecondaryAnimTypes.Greeting:
                _parameterNameSecondary.Value = "Greeting";
                break;
            case SecondaryAnimTypes.Happiness:
                _parameterNameSecondary.Value = "Happiness";
                break;
            case SecondaryAnimTypes.Idle1:
                _parameterNameSecondary.Value = "Idle1";
                break;
            case SecondaryAnimTypes.Idle2:
                _parameterNameSecondary.Value = "Idle2";
                break;
            case SecondaryAnimTypes.IrritatedGirl_IgnoreBoy:
                _parameterNameSecondary.Value = "Irritated";
                break;
            case SecondaryAnimTypes.Laughter:
                _parameterNameSecondary.Value = "Laughter";
                break;
            case SecondaryAnimTypes.IdleGirl_ThinkingBoy_Static:
                _parameterNameSecondary.Value = "Listening";
                break;
            case SecondaryAnimTypes.Mischief:
                _parameterNameSecondary.Value = "Mischief";
                break;
            case SecondaryAnimTypes.RaisedEyebrow_Static:
                _parameterNameSecondary.Value = "Questioning";
                break;
            case SecondaryAnimTypes.RaisedEyebrow:
                _parameterNameSecondary.Value = "RaisedEyebrow";
                break;
            case SecondaryAnimTypes.Sadness:
                _parameterNameSecondary.Value = "Sadness";
                break;
            case SecondaryAnimTypes.Smiling:
                _parameterNameSecondary.Value = "Smiling";
                break;
            case SecondaryAnimTypes.SurpriseBoy:
                _parameterNameSecondary.Value = "Surprise";
                break;
            case SecondaryAnimTypes.Talking:
                _parameterNameSecondary.Value = "Talking";
                break;
            case SecondaryAnimTypes.Victorious:
                _parameterNameSecondary.Value = "Victorious";
                break;
			case SecondaryAnimTypes.Walking:
                _parameterNameSecondary.Value = "Walking";
                break;
			case SecondaryAnimTypes.ThinkingGirl_Static:
			_parameterNameSecondary.Value = "PhoneTalking";
			break;
            case SecondaryAnimTypes.Fainting:
			_parameterNameSecondary.Value = "Fainting";
			break;
			case SecondaryAnimTypes.EnjoyingMusic_Static:
			_parameterNameSecondary.Value = "Dancing";
			break;
			case SecondaryAnimTypes.Surprise2Girl:
			_parameterNameSecondary.Value = "Surprise2";
			break;
			case SecondaryAnimTypes.GiveSomething:
			_parameterNameSecondary.Value = "GiveSomething";
			break;
			case SecondaryAnimTypes.TakeSomething:
			_parameterNameSecondary.Value = "TakeSomething";
			break;
			case SecondaryAnimTypes.GiveKiss:
			_parameterNameSecondary.Value = "GiveKiss";
			break;
			case SecondaryAnimTypes.TakeKiss:
			_parameterNameSecondary.Value = "TakeKiss";
			break;
       case SecondaryAnimTypes.GiveChips:
                _parameterNameSecondary.Value = "GiveSomethingChips";
                break;
                 case SecondaryAnimTypes.GiveCoffee:
                _parameterNameSecondary.Value = "GiveSomethingCoffee";
                break;
                 case SecondaryAnimTypes.GiveHD:
                _parameterNameSecondary.Value = "GiveSomethingHD";
                break;
                case SecondaryAnimTypes.TakeChips:
                _parameterNameSecondary.Value = "TakeSomethingChips";
                break;
                case SecondaryAnimTypes.TakeCoffee:
                _parameterNameSecondary.Value = "TakeSomethingCoffee";
                break;
                case SecondaryAnimTypes.TakeHD:
                _parameterNameSecondary.Value = "TakeSomethingHD";
                break;
		case SecondaryAnimTypes.GiveMap:
			_parameterNameSecondary.Value = "GiveMap";
			break;
		case SecondaryAnimTypes.GivePendant:
			_parameterNameSecondary.Value = "GivePendant";
			break;
		case SecondaryAnimTypes.GivePendulum:
			_parameterNameSecondary.Value = "GivePendulum";
			break;
		case SecondaryAnimTypes.GiveAmulet:
			_parameterNameSecondary.Value = "GiveAmulet";
			break;
		case SecondaryAnimTypes.GiveVoodooDoll:
			_parameterNameSecondary.Value = "GiveVoodooDoll";
			break;
		case SecondaryAnimTypes.GiveHealthBar:
			_parameterNameSecondary.Value = "GiveHealthBar";
			break;
		case SecondaryAnimTypes.TakeAmulet:
			_parameterNameSecondary.Value = "TakeAmulet";
			break;
		case SecondaryAnimTypes.TakeHealthBar:
			_parameterNameSecondary.Value = "TakeHealthBar";
			break;
		case SecondaryAnimTypes.TakePendulum:
			_parameterNameSecondary.Value = "TakePendulum";
			break;
		case SecondaryAnimTypes.TakePendant:
			_parameterNameSecondary.Value = "TakePendant";
			break;
		case SecondaryAnimTypes.TakeMap:
			_parameterNameSecondary.Value = "TakeMap";
			break;
		case SecondaryAnimTypes.TakeRingBox:
			_parameterNameSecondary.Value = "Take RingBox";
			break;
		case SecondaryAnimTypes.GiveRingBox:
			_parameterNameSecondary.Value = "Give RingBox";
			break;
		case SecondaryAnimTypes.GiveSwitch:
			_parameterNameSecondary.Value = "Give switch";
			break;
		case SecondaryAnimTypes.FoldedHand:
			_parameterNameSecondary.Value = "Folded hand";
			break;
		case SecondaryAnimTypes.HappyNod:
			_parameterNameSecondary.Value = "Happy nod";
			break;
		case SecondaryAnimTypes.GoofedupNod:
			_parameterNameSecondary.Value = "Goofed up nod";
			break;
		case SecondaryAnimTypes.HandOnChin:
			_parameterNameSecondary.Value = "Hand on chin";
			break;
		case SecondaryAnimTypes.Sitting:
			_parameterNameSecondary.Value = "Sitting";
			break;
		case SecondaryAnimTypes.StandUp:
			_parameterNameSecondary.Value = "Stand up";
			break;
		case SecondaryAnimTypes.GiveFoodPacket:
			_parameterNameSecondary.Value = "Give Food Packet";
			break;
		case SecondaryAnimTypes.GiveSkinnySalad:
			_parameterNameSecondary.Value = "Give Skinny Salad";
			break;
		case SecondaryAnimTypes.GiveOrangeJuice:
			_parameterNameSecondary.Value = "Give Orange Juice";
			break;
		case SecondaryAnimTypes.GiveGifts:
			_parameterNameSecondary.Value = "Give Gifts";
			break;
		case SecondaryAnimTypes.GivePapers:
			_parameterNameSecondary.Value = "Give Papers";
			break;
		case SecondaryAnimTypes.TakeFoodPacket:
			_parameterNameSecondary.Value = "Take Food Packet";
			break;
		case SecondaryAnimTypes.TakeSkinnySalad:
			_parameterNameSecondary.Value = "Take Skinny Salad";
			break;
		case SecondaryAnimTypes.TakeOrangeJuice:
			_parameterNameSecondary.Value = "Take Orange Juice";
			break;
		case SecondaryAnimTypes.TakeGifts:
			_parameterNameSecondary.Value = "Take Gifts";
			break;
		case SecondaryAnimTypes.TakeAssignmentPprs:
			_parameterNameSecondary.Value = "Take Assignment";
			break;
		case SecondaryAnimTypes.Takephone:
			_parameterNameSecondary.Value = "Take Phone";
			break;
		case SecondaryAnimTypes.TakePics:
			_parameterNameSecondary.Value = "Take Pics";
			break;
		case SecondaryAnimTypes.GivePhone:
			_parameterNameSecondary.Value = "Give Phone";
			break;
		case SecondaryAnimTypes.GivePics:
			_parameterNameSecondary.Value = "Give Pics";
			break;
		case SecondaryAnimTypes.GiveAssignment:
			_parameterNameSecondary.Value = "Give Assignment";
			break;
		


        }
    }

    void GetPrimaryAnimFunction()
    {
        switch (primaryAnimation)
        {
            case PrimaryAnimTypes.MouthAnger:
                _parameterNamePrimary.Value = "MouthAnger";
                break;
            case PrimaryAnimTypes.MouthBored:
                _parameterNamePrimary.Value = "MouthBored";
                break;
            case PrimaryAnimTypes.MouthDisgust:
                _parameterNamePrimary.Value = "MouthDisgust";
                break;
            case PrimaryAnimTypes.MouthHappiness:
                _parameterNamePrimary.Value = "MouthHappiness";
                break;
            case PrimaryAnimTypes.MouthNone:
                _parameterNamePrimary.Value = "MouthNone";
                break;
            case PrimaryAnimTypes.MouthSpeaking:
                _parameterNamePrimary.Value = "MouthSpeaking";
                break;
            case PrimaryAnimTypes.MouthSpeakingShort:
                _parameterNamePrimary.Value = "MouthSpeakingShort";
                break;
                case PrimaryAnimTypes.MouthExtraShortSpeaking:
			_parameterNamePrimary.Value = "MouthExtraShortSpeaking";
                break;
        }
    }

    protected virtual void OnEnable()
    {
        if (character != null)
        {
            _animator.Value = character.gameObject.GetComponentInChildren<Animator>();
        }

    }

    public override void OnEnter()
    {
        if (character != null)
        {
            _animator.Value = character.gameObject.GetComponentInChildren<Animator>();
        }
        if (isFlipped)
        {
            character.gameObject.transform.localScale = new Vector3(-(character.gameObject.transform.localScale.x), character.gameObject.transform.localScale.y, character.gameObject.transform.localScale.z);
        }
        Invoke("OnWaitComplete", _duration.Value);

        Continue();
    }

    protected virtual void OnWaitComplete()
    {
        if (_animator.Value != null)
        {
            if (useMultipleAnimation)
            {
                GetPrimaryAnimFunction ();
				Debug.Log ("PRIMARY ANIMATION" + _parameterNamePrimary.Value + GameConstants.currentStoryNumber);
				if(GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2)
				{
				_animator.Value.SetTrigger (_parameterNamePrimary.Value);
				}
				else 
				{
				_animator.Value.Play(_parameterNamePrimary.Value, -1, 0);
				}
            }

            GetSecondaryAnimFunction ();
			Debug.Log ("SECONDARY ANIMATION" + _parameterNameSecondary.Value + GameConstants.currentStoryNumber);
			if(GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2)
				{
			_animator.Value.SetTrigger(_parameterNameSecondary.Value);
			}
				else 
				{
				_animator.Value.Play(_parameterNameSecondary.Value, -1, 0);
				}
        }


    }

    public override string GetSummary()
    {
        //		if (_animator.Value == null)
        //		{
        //			return "Error: No animator selected";
        //}
        string namePrefix = "";
        if (character != null)
        {
            namePrefix = character.NameText + ": ";

            if (character.NameText == "")
            {
                namePrefix = character.gameObject.name + ": ";
            }
        }

        string prim_animname = "";
        if (useMultipleAnimation)
        {
            prim_animname = primaryAnimation.ToString();
        }

        return namePrefix + " (" + prim_animname + " " + secondaryAnimation.ToString() + ")";

    }

    public override Color GetButtonColor()
    {
        Color32 c = new Color32();

        if (useMultipleAnimation)
        {
            c = new Color32(155, 255, 212, 255);
        }
        else
        {
            c = new Color32(235, 244, 88, 255);
        }

        return c;
    }



}
