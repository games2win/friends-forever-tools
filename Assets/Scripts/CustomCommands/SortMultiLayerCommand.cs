﻿using UnityEngine;
using System.Collections;
using Fungus;
using Unity.Linq;

/// <summary>
/// Sorts Multiple Objects in single layer.
/// </summary>
[CommandInfo ("G2W", 
	"SortMultiLayer", 
	"Sorts Multiple Objects in single layer.")]
[AddComponentMenu ("")]
public class SortMultiLayerCommand : Command
{

	[Tooltip ("Character to display")]
	[SerializeField] protected Character character;

	[Tooltip ("Character 2 to display")]
	[SerializeField] protected Character character2;

	[Tooltip ("Character 3 to display")]
	[SerializeField] protected Character character3;

	[Tooltip ("Character 4 to display")]
	[SerializeField] protected Character character4;

	[Tooltip ("Character 5 to display")]
	[SerializeField] protected Character character5;

	[Tooltip ("Character 6 to display")]
	[SerializeField] protected Character character6;

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character { get { return character; } set { character = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character2 { get { return character2; } set { character2 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character3 { get { return character3; } set { character3 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character4 { get { return character4; } set { character4 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character5 { get { return character5; } set { character5 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character6 { get { return character6; } set { character6 = value; } }


	public override void OnEnter ()
	{
		//base.OnEnter ();

		if (character != null) {
			foreach (var item in character.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_6";
				}
			}
		}
		if (character2 != null) {
			foreach (var item in character2.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_5";
				}
			}
		}
		if (character3 != null) {
			foreach (var item in character3.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_4";
				}
			}
		}
		if (character4 != null) {
			foreach (var item in character4.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_3";
				}
			}
		}
		if (character5 != null) {
			foreach (var item in character5.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_2";
				}
			}
		}
		if (character6 != null) {
			foreach (var item in character6.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_1_Top";
				}
			}
		}
		Continue ();
	}

	//	public override string GetSummary()
	//	{
	//		return character.name + "-" +character2.name + "-" +character3.name + "-" +character4.name + "-" +character5.name + "-" +character6.name;
	//	}
}

