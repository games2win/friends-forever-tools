﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Reflection;
using Fungus;

[CommandInfo("G2W",
	"Fungus Single BG",
	"Load a BG")]
[AddComponentMenu("")]
[ExecuteInEditMode]
public class FungusBGCommand_Single : Command
{

	[Tooltip("Name of the BackGround")]
	[SerializeField]
	public string bgname = "";

	[SerializeField] public  SpriteRenderer spriteRenderer;

	//[SerializeField] public  SpriteRenderer spriteRenderer2;

	[SerializeField] public bool isOpaque;

	public bool commonData = true;

	public override void OnEnter()
	{
		if (bgname != "")
		{
			// this.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(bgName);
			// this.GetComponent<SpriteRenderer>().sprite = GetSpriteFromResources(bgName);
			//this.GetComponent<SpriteRenderer>().material.mainTexture = GetTextureFromResources(bgName);
			// bgName = this.GetComponent<SpriteRenderer>().sprite.name;
			if (commonData)
			{
				//spriteRenderer2 = GameObject.Find("CommonBg2").GetComponent<SpriteRenderer>();
				if (isOpaque) {
					spriteRenderer = GameObject.Find ("CommonBgOpaque").GetComponent<SpriteRenderer> ();
				} else {
					GameObject.Find ("CommonBg1").GetComponent<SpriteRenderer> ().enabled = false;
					GameObject.Find("CommonBg2").GetComponent<SpriteRenderer>().enabled = false;
					spriteRenderer = GameObject.Find ("CommonBgSingle").GetComponent<SpriteRenderer> ();
					spriteRenderer.enabled = true;
				}
			}


			spriteRenderer.sprite = GetSpriteFromResourcesNew(bgname);
			//spriteRenderer2.sprite = GetSpriteFromResourcesNew(bgname+"_1");

		}
		Continue();
	}


	Sprite GetSpriteFromResourcesNew (string name)
	{
		//Texture2D tempTex = Resources.Load(name) as Texture2D;

		TextAsset tmp = Resources.Load (name, typeof(TextAsset)) as TextAsset;

		if (tmp == null) {
			return Resources.Load<Sprite> (name);

		} else {
			string imageStr = Convert.ToBase64String (tmp.bytes); 
			//PlayerPrefs.SetString("SecondaryImage_ByteString", imageStr);

			Texture2D tempTex = new Texture2D (1, 1, TextureFormat.ARGB32, false);

			tempTex.LoadImage (Convert.FromBase64String (imageStr));

			Sprite tempAvatar = Sprite.Create (tempTex, new Rect (0, 0, tempTex.width, tempTex.height), new Vector2 (.5f, .5f));

			//tempTex.LoadImage(tmp.bytes);

			//Sprite tempAvatar = Sprite.Create( tempTex, new Rect(0, 0, tempTex.width, tempTex.height), new Vector2(tempTex.width/2, tempTex.height/2), 100f);


			return tempAvatar;
		}
	}

	public override string GetSummary()
	{
		return (isOpaque ? "IsOpaque: " : "") + bgname;
	}

}
