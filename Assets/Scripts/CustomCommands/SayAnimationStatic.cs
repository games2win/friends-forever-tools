// This code is part of the Fungus library (http://fungusgames.com) maintained by Chris Gregan (http://twitter.com/gofungus).
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;
using Fungus;

/// <summary>
/// Writes text in a dialog box.
/// </summary>
[CommandInfo ("G2W",
	"Dialogue Static",
	"Writes text in a dialog box and sets an animation.")]
[AddComponentMenu ("")]
public class SayAnimationStatic : Command, ILocalizable
{
	// Removed this tooltip as users's reported it obscures the text box
	[TextArea (5, 10)]
	[SerializeField]
	public string storyText = "";

	[Tooltip ("Notes about this story text for other authors, localization, etc.")]
	[SerializeField]
	protected string description = "";

	[Tooltip ("Character that is speaking")]
	[SerializeField]
	public Character character;

	[Tooltip ("Portrait that represents speaking character")]
	[SerializeField]
	protected Sprite portrait;

	[Tooltip ("Voiceover audio to play when writing the text")]
	[SerializeField]
	protected AudioClip voiceOverClip;

	[Tooltip ("Always show this Say text when the command is executed multiple times")]
	[SerializeField]
	protected bool showAlways = true;

	[Tooltip ("Number of times to show this Say text when the command is executed multiple times")]
	[SerializeField]
	protected int showCount = 1;

	[Tooltip ("Type this text in the previous dialog box.")]
	[SerializeField]
	protected bool extendPrevious = false;

	[Tooltip ("Fade out the dialog box when writing has finished and not waiting for input.")]
	[SerializeField]
	protected bool fadeWhenDone = true;

	[Tooltip ("Wait for player to click before continuing.")]
	[SerializeField]
	protected bool waitForClick = true;

	[Tooltip ("Stop playing voiceover when text finishes writing.")]
	[SerializeField]
	protected bool stopVoiceover = true;

	[Tooltip ("Sets the active Say dialog with a reference to a Say Dialog object in the scene. All story text will now display using this Say Dialog.")]
	[SerializeField]
	public SayDialog setSayDialog;

	protected int executionCount;

	private AnimatorData _animator;

	private StringData _parameterNamePrimary;

	private StringData _parameterNameSecondary;

	[Tooltip ("The shape of the easing curve applied to the animation")]
	[SerializeField]
	public PrimaryAnimTypes primaryAnimation = PrimaryAnimTypes.MouthSpeaking;

	public virtual PrimaryAnimTypes PrimaryAnimation { get { return primaryAnimation; } }

	[Tooltip ("The shape of the easing curve applied to the animation")]
	[SerializeField]
	public SecondaryAnimTypes secondaryAnimation = SecondaryAnimTypes.Idle1;

	[Tooltip ("Flip character.")]
	[SerializeField]
	protected bool isFlipped = false;

	[SerializeField] public IntegerData totalCharactersInScene;

	[SerializeField] public IntegerData characterPositionInScene;

	#region Public members

	#region Public members

	public enum PrimaryAnimTypes
	{
		MouthAnger,
		MouthBored,
		MouthDisgust,
		MouthHappiness,
		MouthNone,
		MouthSpeaking,
		MouthSpeakingShort,
		MouthExtraShortSpeaking
	}

	public enum SecondaryAnimTypes
	{

		Anger,
		AgreeGirl_ArroganceBoy,
		Arrogance,
		Blushing,
		Blushing2Girl_EmbarrasedBoy,
		BoredBoy,
		Disgust,
		EyeWink,
		Idle1,
		Idle2,
		IdleGirl_ThinkingBoy_Static,
		EmbarassmentGirl_Static,
		Happiness,
		EmbarrassedBoy_Static,
		RaisedEyebrow_Static,
		ThinkingGirl_Static,
		EnjoyingMusic_Static,
		RaisedEyebrow,
		Laughter,
		Greeting,
		IrritatedGirl_IgnoreBoy,
		Mischief,
		Sadness,
		Smiling,
		SurpriseBoy,
		Talking,
		Surprise2Girl,
		Victorious,
		Walking,
		Fainting,
		GiveSomething,
		TakeSomething,
		GiveKiss,
		TakeKiss,
		GiveCoffee,
		GiveHD,
		GiveChips,
		GiveSwitch,
		TakeCoffee,
		TakeHD,
		TakeChips,
		GiveMap,
		GivePendant,
		GivePendulum,
		GiveVoodooDoll,
		GiveAmulet,
		GiveHealthBar,
		GiveRingBox,
		TakeMap,
		TakePendant,
		TakePendulum,
		TakeAmulet,
		TakeHealthBar,
		TakeRingBox,
		FoldedHand,
		HappyNod,
		GoofedupNod,
		HandOnChin,
		Sitting,
		StandUp,
		GiveFoodPacket,
		GiveSkinnySalad,
		GiveOrangeJuice,
		GiveGifts,
		GivePapers,
		TakeFoodPacket,
		TakeSkinnySalad,
		TakeOrangeJuice,
		TakeGifts,
		TakePapers






	}

	#endregion

	void GetSecondaryAnimFunction ()
	{
		switch (secondaryAnimation) {
		case SecondaryAnimTypes.Anger:
			_parameterNameSecondary.Value = "Anger";
			break;
		case SecondaryAnimTypes.Arrogance:
			_parameterNameSecondary.Value = "Arrogance";
			break;
		case SecondaryAnimTypes.Blushing:
			_parameterNameSecondary.Value = "Blushing";
			break;
		case SecondaryAnimTypes.BoredBoy:
			_parameterNameSecondary.Value = "Bored";
			break;
		case SecondaryAnimTypes.EmbarrassedBoy_Static:
			_parameterNameSecondary.Value = "Confusion";
			break;
		case SecondaryAnimTypes.AgreeGirl_ArroganceBoy:
			_parameterNameSecondary.Value = "Disagree";
			break;
		case SecondaryAnimTypes.Disgust:
			_parameterNameSecondary.Value = "Disgust";
			break;
		case SecondaryAnimTypes.Blushing2Girl_EmbarrasedBoy:
			_parameterNameSecondary.Value = "Embarrassment";
			break;
		case SecondaryAnimTypes.EyeWink:
			_parameterNameSecondary.Value = "EyeWink";
			break;
		case SecondaryAnimTypes.EmbarassmentGirl_Static:
			if (GameConstants.currentStoryNumber > 2) {
				_parameterNameSecondary.Value = "FearWorry2";
			} else {
				_parameterNameSecondary.Value = "FearWorry";
			}
			break;
		case SecondaryAnimTypes.Greeting:
			_parameterNameSecondary.Value = "Greeting";
			break;
		case SecondaryAnimTypes.Happiness:
			_parameterNameSecondary.Value = "Happiness";
			break;
		case SecondaryAnimTypes.Idle1:
			_parameterNameSecondary.Value = "Idle1";
			break;
		case SecondaryAnimTypes.Idle2:
			_parameterNameSecondary.Value = "Idle2";
			break;
		case SecondaryAnimTypes.IrritatedGirl_IgnoreBoy:
			_parameterNameSecondary.Value = "Irritated";
			break;
		case SecondaryAnimTypes.Laughter:
			_parameterNameSecondary.Value = "Laughter";
			break;
		case SecondaryAnimTypes.IdleGirl_ThinkingBoy_Static:
			_parameterNameSecondary.Value = "Listening";
			break;
		case SecondaryAnimTypes.Mischief:
			_parameterNameSecondary.Value = "Mischief";
			break;
		case SecondaryAnimTypes.RaisedEyebrow_Static:
			_parameterNameSecondary.Value = "Questioning";
			break;
		case SecondaryAnimTypes.RaisedEyebrow:
			_parameterNameSecondary.Value = "RaisedEyebrow";
			break;
		case SecondaryAnimTypes.Sadness:
			_parameterNameSecondary.Value = "Sadness";
			break;
		case SecondaryAnimTypes.Smiling:
			_parameterNameSecondary.Value = "Smiling";
			break;
		case SecondaryAnimTypes.SurpriseBoy:
			_parameterNameSecondary.Value = "Surprise";
			break;
		case SecondaryAnimTypes.Talking:
			_parameterNameSecondary.Value = "Talking";
			break;
		case SecondaryAnimTypes.Victorious:
			_parameterNameSecondary.Value = "Victorious";
			break;
		case SecondaryAnimTypes.Walking:
			_parameterNameSecondary.Value = "Walking";
			break;
		case SecondaryAnimTypes.ThinkingGirl_Static:
			_parameterNameSecondary.Value = "PhoneTalking";
			break;
		case SecondaryAnimTypes.Fainting:
			_parameterNameSecondary.Value = "Fainting";
			break;
		case SecondaryAnimTypes.EnjoyingMusic_Static:
			_parameterNameSecondary.Value = "Dancing";
			break;
		case SecondaryAnimTypes.Surprise2Girl:
			_parameterNameSecondary.Value = "Surprise2";
			break;
		case SecondaryAnimTypes.GiveSomething:
			_parameterNameSecondary.Value = "GiveSomething";
			break;
		case SecondaryAnimTypes.GiveChips:
			_parameterNameSecondary.Value = "GiveSomethingChips";
			break;
		case SecondaryAnimTypes.GiveCoffee:
			_parameterNameSecondary.Value = "GiveSomethingCoffee";
			break;
		case SecondaryAnimTypes.GiveHD:
			_parameterNameSecondary.Value = "GiveSomethingHD";
			break;
		case SecondaryAnimTypes.TakeSomething:
			_parameterNameSecondary.Value = "TakeSomething";
			break;
		case SecondaryAnimTypes.TakeChips:
			_parameterNameSecondary.Value = "TakeSomethingChips";
			break;
		case SecondaryAnimTypes.TakeCoffee:
			_parameterNameSecondary.Value = "TakeSomethingCoffee";
			break;
		case SecondaryAnimTypes.TakeHD:
			_parameterNameSecondary.Value = "TakeSomethingHD";
			break;
		case SecondaryAnimTypes.GiveKiss:
			_parameterNameSecondary.Value = "GiveKiss";
			break;
		case SecondaryAnimTypes.TakeKiss:
			_parameterNameSecondary.Value = "TakeKiss";
			break;
		case SecondaryAnimTypes.GiveMap:
			_parameterNameSecondary.Value = "GiveMap";
			break;
		case SecondaryAnimTypes.GivePendant:
			_parameterNameSecondary.Value = "GivePendant";
			break;
		case SecondaryAnimTypes.GivePendulum:
			_parameterNameSecondary.Value = "GivePendulum";
			break;
		case SecondaryAnimTypes.GiveAmulet:
			_parameterNameSecondary.Value = "GiveAmulet";
			break;
		case SecondaryAnimTypes.GiveVoodooDoll:
			_parameterNameSecondary.Value = "GiveVoodooDoll";
			break;
		case SecondaryAnimTypes.GiveHealthBar:
			_parameterNameSecondary.Value = "GiveHealthBar";
			break;
		case SecondaryAnimTypes.TakeAmulet:
			_parameterNameSecondary.Value = "TakeAmulet";
			break;
		case SecondaryAnimTypes.TakeHealthBar:
			_parameterNameSecondary.Value = "TakeHealthBar";
			break;
		case SecondaryAnimTypes.TakePendulum:
			_parameterNameSecondary.Value = "TakePendulum";
			break;
		case SecondaryAnimTypes.TakePendant:
			_parameterNameSecondary.Value = "TakePendant";
			break;
		case SecondaryAnimTypes.TakeMap:
			_parameterNameSecondary.Value = "TakeMap";
			break;
		case SecondaryAnimTypes.TakeRingBox:
			_parameterNameSecondary.Value = "Take RingBox";
			break;
		case SecondaryAnimTypes.GiveRingBox:
			_parameterNameSecondary.Value = "Give RingBox";
			break;
		case SecondaryAnimTypes.GiveSwitch:
			_parameterNameSecondary.Value = "Give switch";
			break;
		case SecondaryAnimTypes.FoldedHand:
			_parameterNameSecondary.Value = "Folded hand";
			break;
		case SecondaryAnimTypes.HappyNod:
			_parameterNameSecondary.Value = "Happy nod";
			break;
		case SecondaryAnimTypes.GoofedupNod:
			_parameterNameSecondary.Value = "Goofed up nod";
			break;
		case SecondaryAnimTypes.HandOnChin:
			_parameterNameSecondary.Value = "Hand on chin";
			break;
		case SecondaryAnimTypes.Sitting:
			_parameterNameSecondary.Value = "Sitting";
			break;
		case SecondaryAnimTypes.StandUp:
			_parameterNameSecondary.Value = "Stand up";
			break;
		case SecondaryAnimTypes.GiveFoodPacket:
			_parameterNameSecondary.Value = "Give Food Packet";
			break;
		case SecondaryAnimTypes.GiveSkinnySalad:
			_parameterNameSecondary.Value = "Give Skinny Salad";
			break;
		case SecondaryAnimTypes.GiveOrangeJuice:
			_parameterNameSecondary.Value = "Give Orange Juice";
			break;
		case SecondaryAnimTypes.GiveGifts:
			_parameterNameSecondary.Value = "Give Gifts";
			break;
		case SecondaryAnimTypes.GivePapers:
			_parameterNameSecondary.Value = "Give Papers";
			break;
		case SecondaryAnimTypes.TakeFoodPacket:
			_parameterNameSecondary.Value = "Take Food Packet";
			break;
		case SecondaryAnimTypes.TakeSkinnySalad:
			_parameterNameSecondary.Value = "Take Skinny Salad";
			break;
		case SecondaryAnimTypes.TakeOrangeJuice:
			_parameterNameSecondary.Value = "Take Orange Juice";
			break;
		case SecondaryAnimTypes.TakeGifts:
			_parameterNameSecondary.Value = "Take Gifts";
			break;
		case SecondaryAnimTypes.TakePapers:
			_parameterNameSecondary.Value = "Take Papers";
			break;

		}
	}

	void GetPrimaryAnimFunction ()
	{
		switch (primaryAnimation) {
		case PrimaryAnimTypes.MouthAnger:
			_parameterNamePrimary.Value = "MouthAnger";
			break;
		case PrimaryAnimTypes.MouthBored:
			_parameterNamePrimary.Value = "MouthBored";
			break;
		case PrimaryAnimTypes.MouthDisgust:
			_parameterNamePrimary.Value = "MouthDisgust";
			break;
		case PrimaryAnimTypes.MouthHappiness:
			_parameterNamePrimary.Value = "MouthHappiness";
			break;
		case PrimaryAnimTypes.MouthNone:
			_parameterNamePrimary.Value = "MouthNone";
			break;
		case PrimaryAnimTypes.MouthSpeaking:
			_parameterNamePrimary.Value = "MouthSpeaking";
			break;
		case PrimaryAnimTypes.MouthSpeakingShort:
			_parameterNamePrimary.Value = "MouthSpeakingShort";
			break;
		case PrimaryAnimTypes.MouthExtraShortSpeaking:
			_parameterNamePrimary.Value = "MouthExtraShortSpeaking";
			break;
		}
	}

	/// <summary>
	/// Character that is speaking.
	/// </summary>
	public virtual Character _Character { get { return character; } }

	/// <summary>
	/// Portrait that represents speaking character.
	/// </summary>
	public virtual Sprite Portrait { get { return portrait; } set { portrait = value; } }

	/// <summary>
	/// Type this text in the previous dialog box.
	/// </summary>
	public virtual bool ExtendPrevious { get { return extendPrevious; } }

	public override void OnEnter ()
	{
		if (!showAlways && executionCount >= showCount) {
			Continue ();
			return;
		}


//		if (characterPositionInScene != -1) {
//
//			if (characterPositionInScene == 0 && totalCharactersInScene > 1) {		//If character is the first in the scene, he/she would look towards the center unless they are the only one in the frame
//				character.gameObject.transform.localRotation = Quaternion.Euler (0, 180, 0);
//			}
//			if (characterPositionInScene == (totalCharactersInScene - 1)) {			//If character is the last in the scene, he/she would look towards the center
//				character.gameObject.transform.localRotation = Quaternion.Euler (0, 0, 0);
//			}
//		}
		// For flipping characters
		foreach (Character c in Character.ActiveCharacters) {
			
			if (c is ProxyCharacter) {

				if (c.gameObject.transform.localScale.x < 0) { // if the character has been flipped from the flowchart for any reason, which is by scaling 'x', and translating it to rotation so that it does not conflict with the rotation flipping done through script
					c.gameObject.transform.localScale = new Vector3 (-(c.gameObject.transform.localScale.x), c.gameObject.transform.localScale.y, c.gameObject.transform.localScale.z);
					if (c.gameObject.transform.localRotation.eulerAngles.y == 180) {
						c.gameObject.transform.localRotation = Quaternion.Euler (0, 0, 0);
					} else if (c.gameObject.transform.localRotation.eulerAngles.y == 0) {
						c.gameObject.transform.localRotation = Quaternion.Euler (0, 180, 0);
					}
				}

				if (c.characterPositionInScene != -1) {
					if (c.characterPositionInScene < characterPositionInScene && c.characterPositionInScene <= totalCharactersInScene) {
						c.gameObject.transform.localRotation = Quaternion.Euler (0, 180, 0);
						Debug.Log (c.gameObject.transform.localRotation.eulerAngles.y);
					} else if (c.characterPositionInScene > characterPositionInScene && c.characterPositionInScene > 0) {
						c.gameObject.transform.localRotation = Quaternion.Euler (0, 0, 0);
					}
				}
			}
		}
		// 
		character.characterPositionInScene = characterPositionInScene;
		executionCount++;

		// Override the active say dialog if needed
		if (character != null && character.SetSayDialog != null) {
			SayDialog.ActiveSayDialog = character.SetSayDialog;
		}

		if (setSayDialog != null) {
			SayDialog.ActiveSayDialog = setSayDialog;
		}

		var sayDialog = SayDialog.GetSayDialog ();
		if (sayDialog == null) {
			Continue ();
			return;
		}

		var flowchart = GetFlowchart ();

		sayDialog.SetActive (true);

		sayDialog.SetCharacter (character);
		sayDialog.SetCharacterImage (portrait);

		string displayText = storyText;

		var activeCustomTags = CustomTag.activeCustomTags;
		for (int i = 0; i < activeCustomTags.Count; i++) {
			var ct = activeCustomTags [i];
			displayText = displayText.Replace (ct.TagStartSymbol, ct.ReplaceTagStartWith);
			if (ct.TagEndSymbol != "" && ct.ReplaceTagEndWith != "") {
				displayText = displayText.Replace (ct.TagEndSymbol, ct.ReplaceTagEndWith);
			}
		}

		string subbedText = flowchart.SubstituteVariables (displayText);

		sayDialog.Say (subbedText, !extendPrevious, waitForClick, fadeWhenDone, stopVoiceover, voiceOverClip, delegate {
			Continue ();
		});
		if (character != null) {
			_animator.Value = character.gameObject.GetComponentInChildren<Animator> ();
		}
//        if (isFlipped)
//        {
//            character.gameObject.transform.localScale = new Vector3(-(character.gameObject.transform.localScale.x), character.gameObject.transform.localScale.y, character.gameObject.transform.localScale.z);
//        }

		if (_animator.Value != null) {
			//	if (useMultipleAnimation) {
			GetPrimaryAnimFunction ();
			Debug.Log ("PRIMARY ANIMATION" + _parameterNamePrimary.Value + GameConstants.currentStoryNumber);
			if (GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2) {
				_animator.Value.SetTrigger (_parameterNamePrimary.Value);
			} else {
				_animator.Value.Play (_parameterNamePrimary.Value, -1, 0);
			}
			//	}
			GetSecondaryAnimFunction ();
			Debug.Log ("SECONDARY ANIMATION" + _parameterNameSecondary.Value);
			if (GameConstants.currentStoryNumber == 1 || GameConstants.currentStoryNumber == 2) {
				_animator.Value.SetTrigger (_parameterNameSecondary.Value);
			} else {
				_animator.Value.Play (_parameterNameSecondary.Value, -1, 0);
			}

		}
	}

	public override string GetSummary ()
	{
		string namePrefix = "";
		if (character != null) {
			namePrefix = character.NameText + ": ";
			if (character.NameText == "") {
				namePrefix = character.name + ": ";
			}
		}
		if (extendPrevious) {
			namePrefix = "EXTEND" + ": ";
		}
		//	Debug.Log ("SAY ANIMATION CHARACTER NAME" + character.nameText + "/ /" + character.name);
		return namePrefix + primaryAnimation.ToString () + "-" + secondaryAnimation.ToString () + " \"" + storyText + "\"";
	}

	public override Color GetButtonColor ()
	{
		return new Color32 (184, 210, 235, 255);
	}

	public override void OnReset ()
	{
		executionCount = 0;
	}

	public override void OnStopExecuting ()
	{
		var sayDialog = SayDialog.GetSayDialog ();
		if (sayDialog == null) {
			return;
		}

		sayDialog.Stop ();
	}

	#endregion

	#region ILocalizable implementation

	public virtual string GetStandardText ()
	{
		return storyText;
	}

	public virtual void SetStandardText (string standardText)
	{
		storyText = standardText;
	}

	public virtual string GetDescription ()
	{
		return description;
	}

	public virtual string GetStringId ()
	{
		// String id for Say commands is SAY.<Localization Id>.<Command id>.[Character Name]
		string stringId = "SAY." + GetFlowchartLocalizationId () + "." + itemId + ".";

		// if (character != null)
		// {
		//     stringId += character.NameText;
		// }


		return stringId;
	}

	#endregion

}