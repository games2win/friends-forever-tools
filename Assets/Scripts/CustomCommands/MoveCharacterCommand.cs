﻿using UnityEngine;
using System.Collections;
using Fungus;
using UnityEngine.Serialization;
using System;


[CommandInfo ("G2W", 
	"Character Move", 
	"Moves a Character to a specified position over time. The position can be defined by a transform in another object (using To Transform) or by setting an absolute position (using To Position, if To Transform is set to None).")]
[AddComponentMenu ("")]
[ExecuteInEditMode]
public class MoveCharacterCommand : iTweenCommand
{

	[Tooltip ("Character to display")]
	[SerializeField] public Character character;

	[Tooltip ("Target transform that the GameObject will move to")]
	[SerializeField] public TransformData _toTransform;

	[Tooltip ("Target world position that the GameObject will move to, if no From Transform is set")]
	[SerializeField] public Vector3Data _toPosition;

	[Tooltip ("Whether to animate in world space or relative to the parent. False by default.")]
	[SerializeField] public bool isLocal;

	[SerializeField] public IntegerData totalCharactersInScene;

	[SerializeField] public IntegerData characterPositionInScene;



	#region Public members

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character { get { return character; } set { character = value; } }

	public override void DoTween ()
	{
        character.characterPositionInScene = characterPositionInScene;
        Hashtable tweenParams = new Hashtable ();
		tweenParams.Add ("name", _tweenName.Value);
		if (_toTransform.Value == null) {
			tweenParams.Add ("position", _toPosition.Value);
		} else {
			tweenParams.Add ("position", _toTransform.Value);
		}
		tweenParams.Add ("time", _duration.Value);
		tweenParams.Add ("easetype", easeType);
		tweenParams.Add ("looptype", loopType);
		tweenParams.Add ("isLocal", isLocal);
		tweenParams.Add ("oncomplete", "OniTweenComplete");
		tweenParams.Add ("oncompletetarget", gameObject);
		tweenParams.Add ("oncompleteparams", this);
		iTween.MoveTo (_targetObject.Value, tweenParams);


		if (GameConstants.currentStoryNumber > 8) {  // so that the changes dont reflect on the previous stories

			if (character.gameObject.transform.localScale.x < 0) {		// if the character has been flipped from the flowchart for any reason, which is by scaling 'x', and translating it to rotation so that it does not conflict with the rotation flipping done through script
				character.gameObject.transform.localScale = new Vector3 (-(character.gameObject.transform.localScale.x), character.gameObject.transform.localScale.y, character.gameObject.transform.localScale.z);
				if (character.gameObject.transform.localRotation.eulerAngles.y == 180) {
					character.gameObject.transform.localRotation = Quaternion.Euler (0, 0, 0);
				} else if (character.gameObject.transform.localRotation.eulerAngles.y == 0) {
					character.gameObject.transform.localRotation = Quaternion.Euler (0, 180, 0);
				}
			}

			if ((characterPositionInScene == 0 || characterPositionInScene == -1) && totalCharactersInScene > 1) {		//If character is the first in the scene, he/she would look towards the center unless they are the only one in the frame
				character.gameObject.transform.localRotation = Quaternion.Euler (0, 180, 0);
			}else if (characterPositionInScene == (totalCharactersInScene - 1)) {										//If character is the last in the scene, he/she would look towards the center
				character.gameObject.transform.localRotation = Quaternion.Euler (0, 0, 0);
			}
		}
	}

	#endregion

	#region Backwards compatibility

	[HideInInspector] [FormerlySerializedAs ("toTransform")] public Transform toTransformOLD;
	[HideInInspector] [FormerlySerializedAs ("toPosition")] public Vector3 toPositionOLD;

	protected override void OnEnable ()
	{
		base.OnEnable ();

		try {
			_targetObject.gameObjectVal = character.gameObject;
		} catch (NullReferenceException ex) {
			Debug.Log ("Character was not set in the inspector");
		}


		if (toTransformOLD != null) {
			_toTransform.Value = toTransformOLD;
			toTransformOLD = null;
		}

		if (toPositionOLD != default(Vector3)) {
			_toPosition.Value = toPositionOLD;
			toPositionOLD = default(Vector3);
		}

	}

	#endregion
}
