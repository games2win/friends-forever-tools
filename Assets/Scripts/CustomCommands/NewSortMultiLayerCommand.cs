using UnityEngine;
using System.Collections;
using Fungus;
using Unity.Linq;

/// <summary>
/// Sorts Multiple Objects in single layer.
/// </summary>
[CommandInfo ("G2W", 
	"NEWSortMultiLayer", 
	"Sorts Multiple Objects in single layer.")]
[AddComponentMenu ("")]
public class NewSortMultiLayerCommand : Command
{

	[Tooltip ("Character to display")]
	[SerializeField] protected Character character;

	[Tooltip ("Character 2 to display")]
	[SerializeField] protected Character character2;

	[Tooltip ("Character 3 to display")]
	[SerializeField] protected Character character3;

	[Tooltip ("Character 4 to display")]
	[SerializeField] protected Character character4;

	[Tooltip ("Character 5 to display")]
	[SerializeField] protected Character character5;

	[Tooltip ("Character 6 to display")]
	[SerializeField] protected Character character6;

	[Tooltip ("Character 7 to display")]
	[SerializeField] protected Character character7;
	[Tooltip ("Character 8 to display")]
	[SerializeField] protected Character character8;
	[Tooltip ("Character 9 to display")]
	[SerializeField] protected Character character9;
	[Tooltip ("Character 10 to display")]
	[SerializeField] protected Character character10;
	[Tooltip ("Character 11 to display")]
	[SerializeField] protected Character character11;
	[Tooltip ("Character 12 to display")]
	[SerializeField] protected Character character12;

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character { get { return character; } set { character = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character2 { get { return character2; } set { character2 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character3 { get { return character3; } set { character3 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character4 { get { return character4; } set { character4 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character5 { get { return character5; } set { character5 = value; } }

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character6 { get { return character6; } set { character6 = value; } }


	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character7 { get { return character7; } set { character7 = value; } }


	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character8 { get { return character8; } set { character8 = value; } }


	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character9 { get { return character9; } set { character9 = value; } }


	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character10 { get { return character10; } set { character10 = value; } }


	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character11 { get { return character11; } set { character11 = value; } }


	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character12 { get { return character12; } set { character12 = value; } }


	public override void OnEnter ()
	{
		//base.OnEnter ();

		if (character != null) {
			foreach (var item in character.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_12";
				}
			}
		}
		if (character2 != null) {
			foreach (var item in character2.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_11";
				}
			}
		}
		if (character3 != null) {
			foreach (var item in character3.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_10";
				}
			}
		}
		if (character4 != null) {
			foreach (var item in character4.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_9";
				}
			}
		}
		if (character5 != null) {
			foreach (var item in character5.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_8";
				}
			}
		}
		if (character6 != null) {
			foreach (var item in character6.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_7";
				}
			}
		}
		if (character7 != null) {
			foreach (var item in character7.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_6";
				}
			}
		}
		if (character8 != null) {
			foreach (var item in character8.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_5";
				}
			}
		}
		if (character9 != null) {
			foreach (var item in character9.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_4";
				}
			}
		}
		if (character10 != null) {
			foreach (var item in character10.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_3";
				}
			}
		}
		if (character11 != null) {
			foreach (var item in character11.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_2";
				}
			}
		}
		if (character12 != null) {
			foreach (var item in character12.gameObject.Descendants()) {
				if (item.GetComponent<SpriteRenderer> () != null) {
					item.GetComponent<SpriteRenderer> ().sortingLayerName = "Layer_1_Top";
				}
			}
		}
		Continue ();
	}

	//	public override string GetSummary()
	//	{
	//		return character.name + "-" +character2.name + "-" +character3.name + "-" +character4.name + "-" +character5.name + "-" +character6.name;
	//	}
}

