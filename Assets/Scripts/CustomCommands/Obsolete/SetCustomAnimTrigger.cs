// This code is part of the Fungus library (http://fungusgames.com) maintained by Chris Gregan (http://twitter.com/gofungus).
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;
using UnityEngine.Serialization;

namespace Fungus
{
    /// <summary>
    /// Sets a trigger parameter on an Animator component to control a Unity animation. EDITED BY RAGHAVENDRA
    /// </summary>
	[CommandInfo("Obsolete", 
                 "Set Custom Anim Trigger", 
                 "Sets a trigger parameter on an Animator component to control a Unity animation")]
    [AddComponentMenu("")]
    [ExecuteInEditMode]
    public class SetCustomAnimTrigger : Command
    {
        [Tooltip("Reference to an Animator component in a game object")]
        [SerializeField] protected AnimatorData _animator;

        [Tooltip("Name of the trigger Animator parameter that will have its value changed")]
        [SerializeField] protected StringData _parameterName;

		[Tooltip("Duration to wait for")]
		[SerializeField] protected FloatData _duration = new FloatData(0);

		[Tooltip("The shape of the easing curve applied to the animation")]
		[SerializeField] protected AnimTypes animType = AnimTypes.Idle11;


        #region Public members

		public enum AnimTypes{
			Idle11,
			Idle22,
			Anger3,
			Arrogance4,
			Bored5,
			Confusion6,
			Disagree7,
			Disgust8,
			Embarrassment9,
			FearWorry10,
			Greeting11,
			Happiness12,
			Laughter13,
			Mischief14,
			Questioning15,
			Sadness16,
			Talking17,
			Listening18,
			Victorious19,
			Walking20,
			EyeWink21,
			Surprise22,
			RaisedEyebrow23,
			Blushing24,
			Irritated25,
			Smiling26,
			MouthNone27,
			MouthSpeaking28,
			MouthHappiness29,
			MouthDisgust30,
			MouthBored31,
			MouthAnger32,
			MouthSpeakingShort33
		}

		void GetAnimFunction(){
			switch (animType){
			case AnimTypes.Anger3:
				_parameterName.Value = "Anger";
				break;
			case AnimTypes.Arrogance4:
				_parameterName.Value = "Arrogance";
				break;
			case AnimTypes.Blushing24:
				_parameterName.Value = "Blushing";
				break;
			case AnimTypes.Bored5:
				_parameterName.Value = "Bored";
				break;
			case AnimTypes.Confusion6:
				_parameterName.Value = "Confusion";
				break;
			case AnimTypes.Disagree7:
				_parameterName.Value = "Disagree";
				break;
			case AnimTypes.Disgust8:
				_parameterName.Value = "Disgust";
				break;
			case AnimTypes.Embarrassment9:
				_parameterName.Value = "Embarrassment";
				break;
			case AnimTypes.EyeWink21:
				_parameterName.Value = "EyeWink";
				break;
			case AnimTypes.FearWorry10:
				_parameterName.Value = "FearWorry";
				break;
			case AnimTypes.Greeting11:
				_parameterName.Value = "Greeting";
				break;
			case AnimTypes.Happiness12:
				_parameterName.Value = "Happiness";
				break;
			case AnimTypes.Idle11:
				_parameterName.Value = "Idle1";
				break;
			case AnimTypes.Idle22:
				_parameterName.Value = "Idle2";
				break;
			case AnimTypes.Irritated25:
				_parameterName.Value = "Irritated";
				break;
			case AnimTypes.Laughter13:
				_parameterName.Value = "Laughter";
				break;
			case AnimTypes.Listening18:
				_parameterName.Value = "Listening";
				break;
			case AnimTypes.Mischief14:
				_parameterName.Value = "Mischief";
				break;
			case AnimTypes.MouthAnger32:
				_parameterName.Value = "MouthAnger";
				break;
			case AnimTypes.MouthBored31:
				_parameterName.Value = "MouthBored";
				break;
			case AnimTypes.MouthDisgust30:
				_parameterName.Value = "MouthDisgust";
				break;
			case AnimTypes.MouthHappiness29:
				_parameterName.Value = "MouthHappiness";
				break;
			case AnimTypes.MouthNone27:
				_parameterName.Value = "MouthNone";
				break;
			case AnimTypes.MouthSpeaking28:
				_parameterName.Value = "MouthSpeaking";
				break;
			case AnimTypes.Questioning15:
				_parameterName.Value = "Questioning";
				break;
			case AnimTypes.RaisedEyebrow23:
				_parameterName.Value = "RaisedEyebrow";
				break;
			case AnimTypes.Sadness16:
				_parameterName.Value = "Sadness";
				break;
			case AnimTypes.Smiling26:
				_parameterName.Value = "Smiling";
				break;
			case AnimTypes.Surprise22:
				_parameterName.Value = "Surprise";
				break;
			case AnimTypes.Talking17:
				_parameterName.Value = "Talking";
				break;
			case AnimTypes.Victorious19:
				_parameterName.Value = "Victorious";
				break;
			case AnimTypes.MouthSpeakingShort33:
				_parameterName.Value = "MouthSpeakingShort";
				break;

			}
		}

        public override void OnEnter()
        {

			Invoke ("OnWaitComplete", _duration.Value);
			Continue();
        }

		protected virtual void OnWaitComplete()
		{
			if (_animator.Value != null)
			{
				GetAnimFunction ();
				_animator.Value.SetTrigger(_parameterName.Value);
			}


		}

        public override string GetSummary()
        {
            if (_animator.Value == null)
            {
                return "Error: No animator selected";
            }

			return _animator.Value.name + " (" + animType.ToString() + ")";
        }

        public override Color GetButtonColor()
        {
            return new Color32(170, 204, 169, 255);
        }

        #endregion

        #region Backwards compatibility

        [HideInInspector] [FormerlySerializedAs("animator")] public Animator animatorOLD;
        [HideInInspector] [FormerlySerializedAs("parameterName")] public string parameterNameOLD = "";

        protected virtual void OnEnable()
        {
            if (animatorOLD != null)
            {
                _animator.Value = animatorOLD;
                animatorOLD = null;
            }

            if (parameterNameOLD != "")
            {
                _parameterName.Value = parameterNameOLD;
                parameterNameOLD = "";
            }
        }

        #endregion
    }
}