
using UnityEngine;
using UnityEngine.Serialization;
using Fungus;

//EDITED BY RAGHAVENDRA
    /// <summary>
/// Sets a trigger parameter on an Animator component to control a Unity animation. (Edited for using multiple animations at once)
    /// </summary>
[CommandInfo("Obsolete", 
                 "Set Multi Anim Trigger", 
                 "Sets a trigger parameter on an Animator component to control a Unity animation edited for using multiple animations at once")]
    [AddComponentMenu("")]
    [ExecuteInEditMode]
    public class SetMultipleAnimTrigger : Command
    {
        [Tooltip("Reference to an Animator component in a game object")]
        [SerializeField] protected AnimatorData _animator;

        [Tooltip("Name of the trigger Animator parameter that will have its value changed")]
        [SerializeField] protected StringData _parameterNamePrimary;

		[Tooltip("Name of the trigger Animator parameter that will have its value changed")]
		[SerializeField] protected StringData _parameterNameSecondary;

        #region Public members

        public override void OnEnter()
        {
            if (_animator.Value != null)
            {
			_animator.Value.SetTrigger(_parameterNamePrimary.Value);
			_animator.Value.SetTrigger(_parameterNameSecondary.Value);
            }

            Continue();
        }

        public override string GetSummary()
        {
            if (_animator.Value == null)
            {
                return "Error: No animator selected";
            }

		return _animator.Value.name + " (" + _parameterNamePrimary.Value + " " + _parameterNameSecondary.Value + ")";
        }

        public override Color GetButtonColor()
        {
            return new Color32(170, 204, 169, 255);
        }

        #endregion

        #region Backwards compatibility

        [HideInInspector] [FormerlySerializedAs("animator")] public Animator animatorOLD;
        [HideInInspector] [FormerlySerializedAs("parameterName")] public string parameterNameOLD = "";

        protected virtual void OnEnable()
        {
            if (animatorOLD != null)
            {
                _animator.Value = animatorOLD;
                animatorOLD = null;
            }

            if (parameterNameOLD != "")
            {
			_parameterNamePrimary.Value = parameterNameOLD;
                parameterNameOLD = "";
            }
        }

        #endregion
    }
