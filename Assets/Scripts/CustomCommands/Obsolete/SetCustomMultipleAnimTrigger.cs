
using UnityEngine;
using UnityEngine.Serialization;
using Fungus;

//EDITED BY RAGHAVENDRA
    /// <summary>
/// Sets a trigger parameter on an Animator component to control a Unity animation. (Edited for using multiple animations at once)
    /// </summary>
[CommandInfo("Obsolete", 
                 "Set Custom Multi Anim Trigger", 
                 "Sets a trigger parameter on an Animator component to control a Unity animation edited for using multiple animations at once")]
    [AddComponentMenu("")]
    [ExecuteInEditMode]
    public class SetCustomMultipleAnimTrigger : Command
    {
        [Tooltip("Reference to an Animator component in a game object")]
        [SerializeField] protected AnimatorData _animator;

        [Tooltip("Name of the trigger Animator parameter that will have its value changed")]
        [SerializeField] protected StringData _parameterNamePrimary;

		[Tooltip("Name of the trigger Animator parameter that will have its value changed")]
		[SerializeField] protected StringData _parameterNameSecondary;

	[Tooltip("The shape of the easing curve applied to the animation")]
	[SerializeField] protected PrimaryAnimTypes animType = PrimaryAnimTypes.MouthNone27;

	[Tooltip("The shape of the easing curve applied to the animation")]
	[SerializeField] protected SecondaryAnimTypes animType2 = SecondaryAnimTypes.Idle11;



        #region Public members

	public enum PrimaryAnimTypes{
		MouthNone27,
		MouthSpeaking28,
		MouthHappiness29,
		MouthDisgust30,
		MouthBored31,
		MouthAnger32,
		MouthSpeakingShort33
	}

	public enum SecondaryAnimTypes{
		Idle11,
		Idle22,
		Anger3,
		Arrogance4,
		Bored5,
		Confusion6,
		Disagree7,
		Disgust8,
		Embarrassment9,
		FearWorry10,
		Greeting11,
		Happiness12,
		Laughter13,
		Mischief14,
		Questioning15,
		Sadness16,
		Talking17,
		Listening18,
		Victorious19,
		Walking20,
		EyeWink21,
		Surprise22,
		RaisedEyebrow23,
		Blushing24,
		Irritated25,
		Smiling26
	}

	void GetSecondaryAnimFunction(){
		switch (animType2){
		case SecondaryAnimTypes.Anger3:
			_parameterNamePrimary.Value = "Anger";
			break;
		case SecondaryAnimTypes.Arrogance4:
			_parameterNamePrimary.Value = "Arrogance";
			break;
		case SecondaryAnimTypes.Blushing24:
			_parameterNamePrimary.Value = "Blushing";
			break;
		case SecondaryAnimTypes.Bored5:
			_parameterNamePrimary.Value = "Bored";
			break;
		case SecondaryAnimTypes.Confusion6:
			_parameterNamePrimary.Value = "Confusion";
			break;
		case SecondaryAnimTypes.Disagree7:
			_parameterNamePrimary.Value = "Disagree";
			break;
		case SecondaryAnimTypes.Disgust8:
			_parameterNamePrimary.Value = "Disgust";
			break;
		case SecondaryAnimTypes.Embarrassment9:
			_parameterNamePrimary.Value = "Embarrassment";
			break;
		case SecondaryAnimTypes.EyeWink21:
			_parameterNamePrimary.Value = "EyeWink";
			break;
		case SecondaryAnimTypes.FearWorry10:
			_parameterNamePrimary.Value = "FearWorry";
			break;
		case SecondaryAnimTypes.Greeting11:
			_parameterNamePrimary.Value = "Greeting";
			break;
		case SecondaryAnimTypes.Happiness12:
			_parameterNamePrimary.Value = "Happiness";
			break;
		case SecondaryAnimTypes.Idle11:
			_parameterNamePrimary.Value = "Idle1";
			break;
		case SecondaryAnimTypes.Idle22:
			_parameterNamePrimary.Value = "Idle2";
			break;
		case SecondaryAnimTypes.Irritated25:
			_parameterNamePrimary.Value = "Irritated";
			break;
		case SecondaryAnimTypes.Laughter13:
			_parameterNamePrimary.Value = "Laughter";
			break;
		case SecondaryAnimTypes.Listening18:
			_parameterNamePrimary.Value = "Listening";
			break;
		case SecondaryAnimTypes.Mischief14:
			_parameterNamePrimary.Value = "Mischief";
			break;
		case SecondaryAnimTypes.Questioning15:
			_parameterNamePrimary.Value = "Questioning";
			break;
		case SecondaryAnimTypes.RaisedEyebrow23:
			_parameterNamePrimary.Value = "RaisedEyebrow";
			break;
		case SecondaryAnimTypes.Sadness16:
			_parameterNamePrimary.Value = "Sadness";
			break;
		case SecondaryAnimTypes.Smiling26:
			_parameterNamePrimary.Value = "Smiling";
			break;
		case SecondaryAnimTypes.Surprise22:
			_parameterNamePrimary.Value = "Surprise";
			break;
		case SecondaryAnimTypes.Talking17:
			_parameterNamePrimary.Value = "Talking";
			break;
		case SecondaryAnimTypes.Victorious19:
			_parameterNamePrimary.Value = "Victorious";
			break;


		}
	}

	void GetPrimaryAnimFunction(){
		switch (animType){
		case PrimaryAnimTypes.MouthAnger32:
			_parameterNameSecondary.Value = "MouthAnger";
			break;
		case PrimaryAnimTypes.MouthBored31:
			_parameterNameSecondary.Value = "MouthBored";
			break;
		case PrimaryAnimTypes.MouthDisgust30:
			_parameterNameSecondary.Value = "MouthDisgust";
			break;
		case PrimaryAnimTypes.MouthHappiness29:
			_parameterNameSecondary.Value = "MouthHappiness";
			break;
		case PrimaryAnimTypes.MouthNone27:
			_parameterNameSecondary.Value = "MouthNone";
			break;
		case PrimaryAnimTypes.MouthSpeaking28:
			_parameterNameSecondary.Value = "MouthSpeaking";
			break;
		case PrimaryAnimTypes.MouthSpeakingShort33:
			_parameterNameSecondary.Value = "MouthSpeakingShort";
			break;
		}
	}

        public override void OnEnter()
        {
            if (_animator.Value != null)
            {
			GetPrimaryAnimFunction ();
			_animator.Value.SetTrigger(_parameterNamePrimary.Value);
			GetSecondaryAnimFunction ();
			_animator.Value.SetTrigger(_parameterNameSecondary.Value);
            }

            Continue();
        }

        public override string GetSummary()
        {
            if (_animator.Value == null)
            {
                return "Error: No animator selected";
            }

		return _animator.Value.name + " (" + _parameterNamePrimary.Value + " " + _parameterNameSecondary.Value + ")";
        }

        public override Color GetButtonColor()
        {
            return new Color32(170, 204, 169, 255);
        }

        #endregion

        #region Backwards compatibility

        [HideInInspector] [FormerlySerializedAs("animator")] public Animator animatorOLD;
        [HideInInspector] [FormerlySerializedAs("parameterName")] public string parameterNameOLD = "";

        protected virtual void OnEnable()
        {
            if (animatorOLD != null)
            {
                _animator.Value = animatorOLD;
                animatorOLD = null;
            }

            if (parameterNameOLD != "")
            {
			_parameterNamePrimary.Value = parameterNameOLD;
                parameterNameOLD = "";
            }
        }

        #endregion
    }
