﻿using UnityEngine;
using System.Collections;
using Fungus;


[CommandInfo("Obsolete",
	"Set Camera Size",
	"Modify Camera Size")]
[AddComponentMenu("")]
[ExecuteInEditMode]

public class ChangeCameraSize : iTweenCommand {

	[Range(-100.0f, 100.0f)]
	[Tooltip("Target transform that the GameObject will scale to")]
	[SerializeField] protected float cameraSize;


	//public float time;

	public override void DoTween()
	{
		Hashtable tweenParams = new Hashtable();
		//tweenParams.Add("from", ob.orthographicSize);
		tweenParams.Add("to", cameraSize);
		tweenParams.Add("time", _duration.Value);
		tweenParams.Add("easetype", easeType);
		tweenParams.Add("looptype", loopType);
		tweenParams.Add("oncomplete", "OniTweenComplete");
		tweenParams.Add("oncompletetarget", gameObject);
		tweenParams.Add("oncompleteparams", this);
		//iTween.ValueTo(cameraObject.gameObject, tweenParams);
	}

//	public override void OnEnter ()
//	{
//		//base.OnEnter ();
//		//cameraObject.orthographicSize = cameraSize;
//		cameraObject.orthographicSize = Mathf.Lerp(cameraObject.orthographicSize,cameraSize,4f);
//		iTween.ValueTo (cameraObject.gameObject,iTween.Hash("from", cameraObject.orthographicSize,
//															"to", cameraSize,
//			"time",time	));
//		Continue ();
//	}



//	public class ScaleTo : iTweenCommand
//	{
//		[Tooltip("Target transform that the GameObject will scale to")]
//		[SerializeField] protected TransformData _toTransform;
//
//		[Tooltip("Target scale that the GameObject will scale to, if no To Transform is set")]
//		[SerializeField] protected Vector3Data _toScale = new Vector3Data(Vector3.one);
//
//		#region Public members
//
//		public override void DoTween()
//		{
//			Hashtable tweenParams = new Hashtable();
//			tweenParams.Add("name", _tweenName.Value);
//			if (_toTransform.Value == null)
//			{
//				tweenParams.Add("scale", _toScale.Value);
//			}
//			else
//			{
//				tweenParams.Add("scale", _toTransform.Value);
//			}
//			tweenParams.Add("time", _duration.Value);
//			tweenParams.Add("easetype", easeType);
//			tweenParams.Add("looptype", loopType);
//			tweenParams.Add("oncomplete", "OniTweenComplete");
//			tweenParams.Add("oncompletetarget", gameObject);
//			tweenParams.Add("oncompleteparams", this);
//			iTween.ScaleTo(_targetObject.Value, tweenParams);
//		}
//
//		#endregion
//
	public override string GetSummary()
	{
		return "Camera Size" + cameraSize.ToString();
	}
}