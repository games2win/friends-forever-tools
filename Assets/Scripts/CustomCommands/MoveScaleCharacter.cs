﻿using UnityEngine;
using System.Collections;
using Fungus;
using UnityEngine.Serialization;
using System;


[CommandInfo ("G2W", 
	"Move Scale", 
	"Moves a Character to a specified position over time. The position can be defined by a transform in another object (using To Transform) or by setting an absolute position (using To Position, if To Transform is set to None).")]
[AddComponentMenu ("")]
[ExecuteInEditMode]
public class MoveScaleCharacter : iTweenCommand
{

	[Tooltip ("Character to display")]
	[SerializeField] public Character character;

	[Tooltip ("Target transform that the GameObject will move to")]
	[SerializeField] public TransformData _toTransform;

	[Tooltip ("Target world position that the GameObject will move to, if no From Transform is set")]
	[SerializeField] public Vector3Data _toPosition;

	 [Tooltip("Target scale that the GameObject will scale to, if no To Transform is set")]
    [SerializeField] public Vector3Data _toScale = new Vector3Data(Vector3.one);

	[Tooltip ("Whether to animate in world space or relative to the parent. False by default.")]
	[SerializeField] public bool isLocal;

	[SerializeField] public IntegerData totalCharactersInScene;

	[SerializeField] public IntegerData characterPositionInScene;



	#region Public members

	/// <summary>
	/// Character to display.
	/// </summary>
	public virtual Character _Character { get { return character; } set { character = value; } }

	public override void DoTween ()
	{
		Hashtable tweenParams = new Hashtable ();
		tweenParams.Add ("name", _tweenName.Value);
		if (_toTransform.Value == null) {
			tweenParams.Add ("position", _toPosition.Value);
			 tweenParams.Add("scale", _toScale.Value);
		} else {
			tweenParams.Add ("position", _toTransform.Value);
			tweenParams.Add("scale", _toTransform.Value);
		}
		tweenParams.Add ("time", _duration.Value);
		tweenParams.Add ("easetype", easeType);
		tweenParams.Add ("looptype", loopType);
		tweenParams.Add ("isLocal", isLocal);
		tweenParams.Add ("oncomplete", "OniTweenComplete");
		tweenParams.Add ("oncompletetarget", gameObject);
		tweenParams.Add ("oncompleteparams", this);
		iTween.MoveTo (_targetObject.Value, tweenParams);
	}

	#endregion

	#region Backwards compatibility

	[HideInInspector] [FormerlySerializedAs ("toTransform")] public Transform toTransformOLD;
	[HideInInspector] [FormerlySerializedAs ("toPosition")] public Vector3 toPositionOLD;

	protected override void OnEnable ()
	{
		base.OnEnable ();

		try {
			_targetObject.gameObjectVal = character.gameObject;
		} catch (NullReferenceException ex) {
			Debug.Log ("Character was not set in the inspector");
		}


		if (toTransformOLD != null) {
			_toTransform.Value = toTransformOLD;
			toTransformOLD = null;
		}

		if (toPositionOLD != default(Vector3)) {
			_toPosition.Value = toPositionOLD;
			toPositionOLD = default(Vector3);
		}
	}

	#endregion
}
