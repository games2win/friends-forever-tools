using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class MoPubEventListener : MonoBehaviour
{
	#if UNITY_ANDROID || UNITY_IPHONE

	void OnEnable ()
	{
		Debug.Log ("MOPUB EVENT");
		// Listen to all events for illustration purposes
		MoPubManager.onAdLoadedEvent += onAdLoadedEvent;
		MoPubManager.onAdFailedEvent += onAdFailedEvent;
		MoPubManager.onAdClickedEvent += onAdClickedEvent;
		MoPubManager.onAdExpandedEvent += onAdExpandedEvent;
		MoPubManager.onAdCollapsedEvent += onAdCollapsedEvent;

		MoPubManager.onInterstitialLoadedEvent += onInterstitialLoadedEvent;
		MoPubManager.onInterstitialFailedEvent += onInterstitialFailedEvent;
		MoPubManager.onInterstitialShownEvent += onInterstitialShownEvent;
		MoPubManager.onInterstitialClickedEvent += onInterstitialClickedEvent;
		MoPubManager.onInterstitialDismissedEvent += onInterstitialDismissedEvent;
		MoPubManager.onInterstitialExpiredEvent += onInterstitialExpiredEvent;

		MoPubManager.onRewardedVideoLoadedEvent += onRewardedVideoLoadedEvent;
		MoPubManager.onRewardedVideoFailedEvent += onRewardedVideoFailedEvent;
		MoPubManager.onRewardedVideoExpiredEvent += onRewardedVideoExpiredEvent;
		MoPubManager.onRewardedVideoShownEvent += onRewardedVideoShownEvent;
		MoPubManager.onRewardedVideoFailedToPlayEvent += onRewardedVideoFailedToPlayEvent;
		MoPubManager.onRewardedVideoReceivedRewardEvent += onRewardedVideoReceivedRewardEvent;
		MoPubManager.onRewardedVideoClosedEvent += onRewardedVideoClosedEvent;
		MoPubManager.onRewardedVideoLeavingApplicationEvent += onRewardedVideoLeavingApplicationEvent;
	}


	void OnDisable ()
	{
		// Remove all event handlers
		MoPubManager.onAdLoadedEvent -= onAdLoadedEvent;
		MoPubManager.onAdFailedEvent -= onAdFailedEvent;
		MoPubManager.onAdClickedEvent -= onAdClickedEvent;
		MoPubManager.onAdExpandedEvent -= onAdExpandedEvent;
		MoPubManager.onAdCollapsedEvent -= onAdCollapsedEvent;

		MoPubManager.onInterstitialLoadedEvent -= onInterstitialLoadedEvent;
		MoPubManager.onInterstitialFailedEvent -= onInterstitialFailedEvent;
		MoPubManager.onInterstitialShownEvent -= onInterstitialShownEvent;
		MoPubManager.onInterstitialClickedEvent -= onInterstitialClickedEvent;
		MoPubManager.onInterstitialDismissedEvent -= onInterstitialDismissedEvent;
		MoPubManager.onInterstitialExpiredEvent -= onInterstitialExpiredEvent;

		MoPubManager.onRewardedVideoLoadedEvent -= onRewardedVideoLoadedEvent;
		MoPubManager.onRewardedVideoFailedEvent -= onRewardedVideoFailedEvent;
		MoPubManager.onRewardedVideoExpiredEvent -= onRewardedVideoExpiredEvent;
		MoPubManager.onRewardedVideoShownEvent -= onRewardedVideoShownEvent;
		MoPubManager.onRewardedVideoFailedToPlayEvent -= onRewardedVideoFailedToPlayEvent;
		MoPubManager.onRewardedVideoReceivedRewardEvent -= onRewardedVideoReceivedRewardEvent;
		MoPubManager.onRewardedVideoClosedEvent -= onRewardedVideoClosedEvent;
		MoPubManager.onRewardedVideoLeavingApplicationEvent -= onRewardedVideoLeavingApplicationEvent;
	}


	// Banner Events

	void onAdLoadedEvent (float height)
	{
		// These are for handling visibility of ads after an ad LOADS.  An ad may get requested in a 
		bool bShowBanner = false;
		bool bShowExit = false;

		// Exit banners are hidden by default, unless on the exit panel
		if (SceneManager.GetActiveScene ().name == "IntroScene" ) {
			if(GameConstants.isExitGamePopupActive){
				bShowExit = true;
			}
		}

		// List scenes where banner ads should NOT be shown
		if (SceneManager.GetActiveScene ().name != "IntroScene"){
			bShowBanner = true;
		}

		if (DeviceSize.isPhone ()) {
			MoPub.showBanner (GameConstants.mopubBannerPhoneID, bShowBanner);
	#if UNITY_ANDROID
			MoPub.showBanner (GameConstants.mopubExitAdPhoneID, bShowExit);
	#endif
		} else {
				MoPub.showBanner (GameConstants.mopubBannerTabID, bShowBanner);
	#if UNITY_ANDROID
				MoPub.showBanner (GameConstants.mopubExitAdTabID, bShowExit);
	#endif
		}
	}



	void onAdFailedEvent (string errorMsg)
	{
		Debug.Log ("onAdFailedEvent: " + errorMsg);
	}

	void onAdClickedEvent (string adUnitId)
	{
		Debug.Log ("onAdClickedEvent: " + adUnitId);
	}

	void onAdExpandedEvent (string adUnitId)
	{
		Debug.Log ("onAdExpandedEvent: " + adUnitId);
	}

	void onAdCollapsedEvent (string adUnitId)
	{
		Debug.Log ("onAdCollapsedEvent: " + adUnitId);
	}


	// Interstitial Events

	void onInterstitialLoadedEvent (string adUnitId)
	{
		Debug.Log ("onInterstitialLoadedEvent: " + adUnitId);
		GameConstants.isInterstitialLoaded = true;
		GameConstants.isInterstitialRequested = false;
	}

	void onInterstitialFailedEvent (string errorMsg)
	{
		Debug.Log ("onInterstitialFailedEvent: " + errorMsg);
		GameConstants.isInterstitialLoaded = false;
		GameConstants.isInterstitialRequested = false;
		//  G2WAdHelper.CheckForInterstitialSuperSonic();
        G2WAdHelper.CheckForInterstitial ();
	}

	void onInterstitialShownEvent (string adUnitId)
	{
		Debug.Log ("onInterstitialShownEvent: " + adUnitId);
	}

	void onInterstitialClickedEvent (string adUnitId)
	{
		Debug.Log ("onInterstitialClickedEvent: " + adUnitId);
	}

	void onInterstitialDismissedEvent (string adUnitId)
	{
		Debug.Log ("onInterstitialDismissedEvent: " + adUnitId);
		G2WAdHelper.interstitialTimer = 0;
		GameConstants.isInterstitialLoaded = false;
		GameConstants.isInterstitialRequested = false;
	//	  G2WAdHelper.CheckForInterstitialSuperSonic();
        G2WAdHelper.CheckForInterstitial ();
	}

	void onInterstitialExpiredEvent (string adUnitId)
	{
		Debug.Log ("onInterstitialExpiredEvent: " + adUnitId);
//		  G2WAdHelper.CheckForInterstitialSuperSonic();
        G2WAdHelper.CheckForInterstitial ();
	}


	// Rewarded Video Events

	void onRewardedVideoLoadedEvent (string adUnitId)
	{
		Debug.Log ("onRewardedVideoLoadedEvent: " + adUnitId);
	}

	void onRewardedVideoFailedEvent (string errorMsg)
	{
		Debug.Log ("onRewardedVideoFailedEvent: " + errorMsg);
	}

	void onRewardedVideoExpiredEvent (string adUnitId)
	{
		Debug.Log ("onRewardedVideoExpiredEvent: " + adUnitId);
	}

	void onRewardedVideoShownEvent (string adUnitId)
	{
		Debug.Log ("onRewardedVideoShownEvent: " + adUnitId);
	}

	void onRewardedVideoFailedToPlayEvent (string errorMsg)
	{
		Debug.Log ("onRewardedVideoFailedToPlayEvent: " + errorMsg);
	}

	void onRewardedVideoReceivedRewardEvent (MoPubManager.RewardedVideoData rewardedVideoData)
	{
		Debug.Log ("onRewardedVideoReceivedRewardEvent: " + rewardedVideoData);
	}

	void onRewardedVideoClosedEvent (string adUnitId)
	{
		Debug.Log ("onRewardedVideoClosedEvent: " + adUnitId);
	}

	void onRewardedVideoLeavingApplicationEvent (string adUnitId)
	{
		Debug.Log ("onRewardedVideoLeavingApplicationEvent: " + adUnitId);
	}

	#endif
}


