﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEditor;
public class LinksTester : EditorWindow {

	// Use this for initialization

	private static Flowchart[] flowchart;

	[MenuItem("G2W/LinkChecker")]
    static void Test()
    {

        flowchart = GameObject.FindObjectsOfType<Flowchart>();

        foreach (Flowchart f in flowchart)
        {
            var menus = f.GetComponents<Fungus.Menu>();
            var calls = f.GetComponents<Call>();
            for (int i = 0; i < menus.Length; i++)
            {
                if (menus[i].targetBlock == null)
                {
                    Debug.Log(f.name + " has broken link in " + menus[i].ParentBlock.BlockName);
                }
				else
				{
                    Debug.Log("Double Thumbs up for Menus");
                }
            }
			  for (int i = 0; i < calls.Length; i++)
            {
                if (calls[i].targetBlock == null)
                {
                    Debug.Log(f.name + " has broken link for Call in " + calls[i].ParentBlock.BlockName);
                }
				else
				{
                    Debug.Log("Double Thumbs up for Calls");
                }
            }

        }
    }


}
