﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class DeletePrefs : MonoBehaviour {

	[MenuItem( "Window/DeletePrefs" )]

	static void deletePrefs()
	{
		PlayerPrefs.DeleteAll ();
		Debug.Log ("DELETED PREFS");
	}
}
