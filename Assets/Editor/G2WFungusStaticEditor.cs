﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Fungus;


/// MINUS is for offscreen characters.
public enum CharacterNumbersStatic
{
	MINUS = -1,
	ONE = 0,
	TWO = 1,
	THREE = 2,
	FOUR = 3,
	FIVE = 4,
	SIX = 5
}

public class G2WFungusStaticEditor : EditorWindow
{

	int tempCharPos = 0;

	int tempTotalChars = 0;

	private static Flowchart[] flowchart;
	string totalCharsInScene = "";
	string charPosInScene = "";

	string dialogBoxName = "";
	string menuBoxName = "";
	string sayBoxName = "";

	Vector3 objPosition;

	Vector3 objScale;

	public CharacterNumbers totalCharactersEnum;
	public CharacterNumbers charPositionEnum;

	bool customPos = false;
	bool customScale = false;



	[MenuItem ("G2W/Static/G2WFungusStaticEditor")]
	static void Init ()
	{
		G2WFungusStaticEditor window = (G2WFungusStaticEditor)EditorWindow.GetWindow (typeof(G2WFungusStaticEditor));
		//window.CheckResources();
		//window.minSize=new Vector2(MinWidth,475);
	}

	void OnGUI ()
	{

		using (var verticalScope = new GUILayout.VerticalScope ("box")) {
			totalCharactersEnum = (CharacterNumbers)EditorGUILayout.EnumPopup ("Total Characters in Scene", totalCharactersEnum);
			charPositionEnum = (CharacterNumbers)EditorGUILayout.EnumPopup ("Character Position In Scene", charPositionEnum);
			tempTotalChars = (int)totalCharactersEnum + 1;
			tempCharPos = (int)charPositionEnum;
			totalCharsInScene = tempTotalChars.ToString ();
			charPosInScene = tempCharPos.ToString ();
			Debug.Log ("TOTAL CHARS" + totalCharsInScene + " " + charPosInScene);

			//totalCharsInScene = EditorGUILayout.TextField("Total Characters in Scene", totalCharsInScene.ToString());
			//EditorGUILayout.Space();
			//charPosInScene = EditorGUILayout.TextField("Character Position In Scene", charPosInScene.ToString());
		}

		Rect r = (Rect)EditorGUILayout.BeginVertical ("Button");
		EditorGUILayout.LabelField ("Add Dialog Boxes");

		dialogBoxName = EditorGUILayout.TextField ("Dialog Box Name", dialogBoxName.ToString ());


		if (GUILayout.Button ("Add Dialog Boxes", GUILayout.Width (150), GUILayout.Height (40))) {
			getFlowcharts ();
		}
		EditorGUILayout.EndVertical ();
		using (var verticalScope = new GUILayout.VerticalScope ("box")) {
			EditorGUILayout.LabelField ("Add Menu Boxes");
			menuBoxName = EditorGUILayout.TextField ("Menu Box Name", menuBoxName.ToString ());

			if (GUILayout.Button ("Add Menu Boxes", GUILayout.Width (150), GUILayout.Height (40))) {
				AddMenuBoxes ();
			}
		}

			using (var verticalScope = new GUILayout.VerticalScope ("box")) {
			EditorGUILayout.LabelField ("Add Say Dialog Boxes");
			sayBoxName = EditorGUILayout.TextField ("Dialog Box Name", sayBoxName.ToString ());

			if (GUILayout.Button ("Add Say Dialog Boxes", GUILayout.Width (150), GUILayout.Height (40))) {
				addSayDialogBox ();
			}
		}

		using (var verticalScope = new GUILayout.VerticalScope ("position")) {
			EditorGUILayout.LabelField ("Changing Position");
			objPosition = EditorGUILayout.Vector3Field ("Position:", objPosition);
			customPos = EditorGUILayout.Toggle ("Custom Positions", customPos);

			if (!customPos)
				setPosValues ();
            
			if (GUILayout.Button ("Add Position", GUILayout.Width (80), GUILayout.Height (40))) {
				AddPositons ();
			}
		}

		using (var verticalScope = new GUILayout.VerticalScope ("scale")) {
			EditorGUILayout.LabelField ("Changing Scale");
			objScale = EditorGUILayout.Vector3Field ("Scale:", objScale);

			customScale = EditorGUILayout.Toggle ("Custom Scaling", customScale);

			if (!customScale)
				setScaleValues ();

			if (GUILayout.Button ("Add Scale", GUILayout.Width (80), GUILayout.Height (40))) {
				AddScaling ();
			}
		}


		//this.Repaint();

	}

	int charPos = 0;
	int totalChars = 0;

	void getFlowcharts ()
	{
		flowchart = GameObject.FindObjectsOfType<Flowchart> ();
		Debug.Log("_________________________________________________________________");
		foreach (Flowchart f in flowchart) {
			var sayanimation = f.GetComponents<Say> ();
			for (int i = 0; i < sayanimation.Length; i++) {
//				if (sayanimation [i].totalCharactersInScene.Value.ToString () == totalCharsInScene && sayanimation [i].characterPositionInScene.Value.ToString () == charPosInScene) {
				Debug.Log("_________________________________________________________________");
					try {
						sayanimation [i].setSayDialog = GameObject.Find (dialogBoxName).GetComponent<SayDialog> ();
					Debug.Log("_________________________________________________________________done");
					} catch (System.NullReferenceException) {
						// Debug.Log("Say Dialog box is either missing or inactive");
						EditorUtility.DisplayDialog ("DialogBox", "Say Dialog box is either missing or inactive", "OK");
					}

//				}

			}
		}
	}

void addSayDialogBox ()
	{
		flowchart = GameObject.FindObjectsOfType<Flowchart> ();

		foreach (Flowchart f in flowchart) {
			var sayanimation = f.GetComponents<SayAnimationStatic> ();
			for (int i = 0; i < sayanimation.Length; i++) {
				if (sayanimation [i].totalCharactersInScene.Value.ToString () == totalCharsInScene && sayanimation [i].characterPositionInScene.Value.ToString () == charPosInScene) {
					try {
						sayanimation [i].setSayDialog = GameObject.Find (sayBoxName).GetComponent<SayDialog> ();
					} catch (System.NullReferenceException) {
						// Debug.Log("Say Dialog box is either missing or inactive");
						EditorUtility.DisplayDialog ("DialogBox", "Say Dialog box is either missing or inactive", "OK");
					}
				}
			}
		}
	}


	void AddMenuBoxes ()
	{
		flowchart = GameObject.FindObjectsOfType<Flowchart> ();

		foreach (Flowchart f in flowchart) {
			var menuDialog = f.GetComponents<Fungus.Menu> ();
			for (int i = 0; i < menuDialog.Length; i++) {
				if (menuBoxName != "") {
					try {
						menuDialog [i].setMenuDialog = GameObject.Find (menuBoxName).GetComponent<MenuDialog> ();
					} catch (System.NullReferenceException) {
						// Debug.Log("Say Dialog box is either missing or inactive");
						EditorUtility.DisplayDialog ("DialogBox", "Menu Dialog box is either missing or inactive", "OK");
					}
				}


			}
		}
	}

	void AddPositons ()
	{
		flowchart = GameObject.FindObjectsOfType<Flowchart> ();

		foreach (Flowchart f in flowchart) {
			var position = f.GetComponents<MoveCharacterCommand> ();
			for (int i = 0; i < position.Length; i++) {
				if (position [i].totalCharactersInScene.Value.ToString () == totalCharsInScene && position [i].characterPositionInScene.Value.ToString () == charPosInScene)
					position [i]._toPosition.Value = objPosition;



			}
		}
	}

	void AddScaling ()
	{
		flowchart = GameObject.FindObjectsOfType<Flowchart> ();

		foreach (Flowchart f in flowchart) {
			var scaling = f.GetComponents<MoveScaleCharacter> ();
			for (int i = 0; i < scaling.Length; i++) {
				if (scaling [i].totalCharactersInScene.Value.ToString () == totalCharsInScene && scaling [i].characterPositionInScene.Value.ToString () == charPosInScene)
					scaling [i]._toScale.Value = objScale;



			}
		}
	}

	void setPosValues ()
	{
		Debug.Log ("SET POS VALUES" + charPosInScene + " " + totalCharsInScene);
		if (totalCharsInScene == "2") {
			if (charPosInScene == "0")
				objPosition = new Vector3 (2.18f, -7.23f, 0f);
			else if (charPosInScene == "1")
				objPosition = new Vector3 (4.67f, -7.4f, 0f);
		} else if (totalCharsInScene == "3") {
			if (charPosInScene == "0")
				objPosition = new Vector3 (2f, -7.41f, 0f);
			else if (charPosInScene == "1")
				objPosition = new Vector3 (3.24f, -7.37f, 0f);
			else if (charPosInScene == "2")
				objPosition = new Vector3 (5.16f, -7.58f, 0f);
		} else if (totalCharsInScene == "4") {
			if (charPosInScene == "0")
				objPosition = new Vector3 (2f, -7.41f, 0f);
			else if (charPosInScene == "1")
				objPosition = new Vector3 (3.24f, -7.37f, 0f);
			else if (charPosInScene == "2")
				objPosition = new Vector3 (5.16f, -7.58f, 0f);
		}
       
	}

	void setScaleValues ()
	{

	}
}
