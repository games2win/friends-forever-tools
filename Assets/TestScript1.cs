﻿using UnityEngine;
using System.Collections;
using Unity.Linq;
using UnityEngine.SceneManagement;

public class TestScript1 : MonoBehaviour {

	//public int currentBGnumber = 1;

	public GameObject characterobj;

	// Use this for initialization
	void Awake () {
		var levels1 = GameObject.Find("Backgrounds");
		var child = levels1.Child ("Level" + GameConstants.currentHiddenObjectLevelNumber);

		child.SetActive (true);
	}

	public void nextBG()
	{
		hideAll ();
		GameConstants.currentHiddenObjectLevelNumber++;
		if (GameConstants.currentHiddenObjectLevelNumber > 3) {
			GameConstants.currentHiddenObjectLevelNumber = 3;
		}

		var levels1 = GameObject.Find("Backgrounds");
		var child = levels1.Child ("Level" +GameConstants.currentHiddenObjectLevelNumber);
		child.SetActive (true);
		SceneManager.LoadScene ("Blank");
		SceneManager.LoadScene ("HiddenObjectScene");
	}

	public void previousBG()
	{
		hideAll ();
		GameConstants.currentHiddenObjectLevelNumber--;
		if (GameConstants.currentHiddenObjectLevelNumber < 1) {
			GameConstants.currentHiddenObjectLevelNumber = 1;
		}
		var levels1 = GameObject.Find("Backgrounds");
		var child = levels1.Child ("Level" +GameConstants.currentHiddenObjectLevelNumber);

		child.SetActive (true);
		SceneManager.LoadScene ("Blank");
		SceneManager.LoadScene ("HiddenObjectScene");
	}

	public void characterToggle()
	{
		characterobj.SetActive (!characterobj.activeSelf);
	}

	public void hideAll()
	{
		var levels1 = GameObject.Find("Backgrounds");

		for (int i = 1; i < 14; i++) {
			var child = levels1.Child ("Level" +i);

			child.SetActive (false);
		}


	}

	

}
