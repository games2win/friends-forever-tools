using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextLocalize : MonoBehaviour
{
	public bool allCaps = false;
	public string key;
	private Text label;
//EDITED BY RAGHAVENDRA
	

	public string value
	{
		set
		{
			if (!string.IsNullOrEmpty(value))
			{
				if(allCaps)
				label.text = value.ToUpper();
				else
				label.text = value;
#if UNITY_EDITOR
			UnityEditor.EditorUtility.SetDirty(label);
#endif
			}
		}
	}

	private void Awake()
	{
		TextLocalization.LocalizationChanged += OnLocalize;
		label = GetComponent<Text>();
	}

	private void Destroy()
	{
		TextLocalization.LocalizationChanged -= OnLocalize;
	}

	private void OnEnable()
	{
#if UNITY_EDITOR
	if (!Application.isPlaying) return;
#endif
		OnLocalize();
	}

	private void Start()
	{
#if UNITY_EDITOR
	if (!Application.isPlaying) return;
#endif
		OnLocalize();
	}

	public void OnLocalize()
	{
		if (string.IsNullOrEmpty(key))
			key = label.text;

		if (!string.IsNullOrEmpty(key)) value = TextLocalization.Get(key);
	}
}
