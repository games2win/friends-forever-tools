# Audio commands # {#audio_commands}

[TOC]
# Control Audio # {#ControlAudio}
Plays, loops, or stops an audiosource. Any AudioSources with the same tag as the target Audio Source will automatically be stoped.

Defined in Fungus.ControlAudio

Property | Type | Description
 --- | --- | ---
Control | Fungus.ControlAudioType | 
_audio Source | Fungus.AudioSourceData | 
Start Volume | System.Single | 
End Volume | System.Single | 
Fade Duration | System.Single | 
Wait Until Finished | System.Boolean | 

# Play Music # {#PlayMusic}
Plays looping game music. If any game music is already playing, it is stopped. Game music will continue playing across scene loads.

Defined in Fungus.PlayMusic

Property | Type | Description
 --- | --- | ---
Music Clip | UnityEngine.AudioClip | 
At Time | System.Single | 
Loop | System.Boolean | 
Fade Duration | System.Single | 

# Play Sound # {#PlaySound}
Plays a once-off sound effect. Multiple sound effects can be played at the same time.

Defined in Fungus.PlaySound

Property | Type | Description
 --- | --- | ---
Sound Clip | UnityEngine.AudioClip | 
Volume | System.Single | 
Wait Until Finished | System.Boolean | 

# Play Usfxr Sound # {#PlayUsfxrSound}
Plays a usfxr synth sound. Use the usfxr editor [Tools > Fungus > Utilities > Generate usfxr Sound Effects] to create the SettingsString. Set a ParentTransform if using positional sound. See https://github.com/zeh/usfxr for more information about usfxr.

Defined in Fungus.PlayUsfxrSound

Property | Type | Description
 --- | --- | ---
Parent Transform | UnityEngine.Transform | 
_ Settings String | Fungus.StringDataMulti | 
Wait Duration | System.Single | 

# Set Audio Pitch # {#SetAudioPitch}
Sets the global pitch level for audio played with Play Music and Play Sound commands.

Defined in Fungus.SetAudioPitch

Property | Type | Description
 --- | --- | ---
Pitch | System.Single | 
Fade Duration | System.Single | 
Wait Until Finished | System.Boolean | 

# Set Audio Volume # {#SetAudioVolume}
Sets the global volume level for audio played with Play Music and Play Sound commands.

Defined in Fungus.SetAudioVolume

Property | Type | Description
 --- | --- | ---
Volume | System.Single | 
Fade Duration | System.Single | 
Wait Until Finished | System.Boolean | 

# Stop Music # {#StopMusic}
Stops the currently playing game music.

Defined in Fungus.StopMusic
