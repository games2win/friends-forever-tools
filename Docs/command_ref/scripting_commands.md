# Scripting commands # {#scripting_commands}

[TOC]
# Comment # {#Comment}
Use comments to record design notes and reminders about your game.

Defined in Fungus.Comment

Property | Type | Description
 --- | --- | ---
Commenter Name | System.String | 
Comment Text | System.String | 

# Call Method # {#CallMethod}
Calls a named method on a GameObject using the GameObject.SendMessage() system.

Defined in Fungus.CallMethod

Property | Type | Description
 --- | --- | ---
Target Object | UnityEngine.GameObject | 
Method Name | System.String | 
Delay | System.Single | 

# Debug Log # {#DebugLog}
Writes a log message to the debug console.

Defined in Fungus.DebugLog

Property | Type | Description
 --- | --- | ---
Log Type | Fungus.DebugLogType | 
Log Message | Fungus.StringDataMulti | 

# Destroy # {#Destroy}
Destroys a specified game object in the scene.

Defined in Fungus.Destroy

Property | Type | Description
 --- | --- | ---
_target Game Object | Fungus.GameObjectData | 

# Execute Lua # {#ExecuteLua}
Executes a Lua code chunk using a Lua Environment.

Defined in Fungus.ExecuteLua

Property | Type | Description
 --- | --- | ---
Lua Environment | Fungus.LuaEnvironment | 
Lua File | UnityEngine.TextAsset | 
Lua Script | System.String | 
Run As Coroutine | System.Boolean | 
Wait Until Finished | System.Boolean | 
Return Variable | Fungus.Variable | 

# Invoke Event # {#InvokeEvent}
Calls a list of component methods via the Unity Event System (as used in the Unity UI). This command is more efficient than the Invoke Method command but can only pass a single parameter and doesn't support return values.

Defined in Fungus.InvokeEvent

Property | Type | Description
 --- | --- | ---
Delay | System.Single | 
Invoke Type | Fungus.InvokeType | 
Static Event | UnityEngine.Events.UnityEvent | 
Boolean Parameter | Fungus.BooleanData | 
Boolean Event | Fungus.InvokeEvent+BooleanEvent | 
Integer Parameter | Fungus.IntegerData | 
Integer Event | Fungus.InvokeEvent+IntegerEvent | 
Float Parameter | Fungus.FloatData | 
Float Event | Fungus.InvokeEvent+FloatEvent | 
String Parameter | Fungus.StringDataMulti | 
String Event | Fungus.InvokeEvent+StringEvent | 

# Invoke Method # {#InvokeMethod}
Invokes a method of a component via reflection. Supports passing multiple parameters and storing returned values in a Fungus variable.

Defined in Fungus.InvokeMethod

Property | Type | Description
 --- | --- | ---
Target Object | UnityEngine.GameObject | 
Target Component Assembly Name | System.String | 
Target Component Fullname | System.String | 
Target Component Text | System.String | 
Target Method | System.String | 
Target Method Text | System.String | 
Method Parameters | Fungus.InvokeMethodParameter[] | 
Save Return Value | System.Boolean | 
Return Value Variable Key | System.String | 
Return Value Type | System.String | 
Show Inherited | System.Boolean | 
Call Mode | Fungus.CallMode | 

# Open URL # {#OpenURL}
Opens the specified URL in the browser.

Defined in Fungus.LinkToWebsite

Property | Type | Description
 --- | --- | ---
Url | Fungus.StringData | 

# Set Active # {#SetActive}
Sets a game object in the scene to be active / inactive.

Defined in Fungus.SetActive

Property | Type | Description
 --- | --- | ---
_target Game Object | Fungus.GameObjectData | 
Active State | Fungus.BooleanData | 

# Sort Layer # {#SortLayer}
Sorts the image layers

Defined in SortingCommandScript
# Spawn Object # {#SpawnObject}
Spawns a new object based on a reference to a scene or prefab game object.

Defined in Fungus.SpawnObject

Property | Type | Description
 --- | --- | ---
_source Object | Fungus.GameObjectData | 
_parent Transform | Fungus.TransformData | 
_spawn Position | Fungus.Vector3Data | 
_spawn Rotation | Fungus.Vector3Data | 

