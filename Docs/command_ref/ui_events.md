# UI event handlers # {#ui_events}

[TOC]
# Button Clicked # {#ButtonClicked}
The block will execute when the user clicks on the target UI button object.

Defined in Fungus.ButtonClicked

Property | Type | Description
 --- | --- | ---
Target Button | UnityEngine.UI.Button | 

# End Edit # {#EndEdit}
The block will execute when the user finishes editing the text in the input field.

Defined in Fungus.EndEdit

Property | Type | Description
 --- | --- | ---
Target Input Field | UnityEngine.UI.InputField | 

# Toggle Changed # {#ToggleChanged}
The block will execute when the state of the target UI toggle object changes. The state of the toggle is stored in the Toggle State boolean variable.

Defined in Fungus.ToggleChanged

Property | Type | Description
 --- | --- | ---
Target Toggle | UnityEngine.UI.Toggle | 
Toggle State | Fungus.BooleanVariable | 

