# Animation commands # {#animation_commands}

[TOC]
# Play Anim State # {#PlayAnimState}
Plays a state of an animator according to the state name

Defined in Fungus.PlayAnimState

Property | Type | Description
 --- | --- | ---
Animator | Fungus.AnimatorData | 
State Name | Fungus.StringData | 
Layer | Fungus.IntegerData | 
Time | Fungus.FloatData | 

# Reset Anim Trigger # {#ResetAnimTrigger}
Resets a trigger parameter on an Animator component.

Defined in Fungus.ResetAnimTrigger

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name | Fungus.StringData | 

# Set Anim Bool # {#SetAnimBool}
Sets a boolean parameter on an Animator component to control a Unity animation

Defined in Fungus.SetAnimBool

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name | Fungus.StringData | 
Value | Fungus.BooleanData | 

# Set Anim Float # {#SetAnimFloat}
Sets a float parameter on an Animator component to control a Unity animation

Defined in Fungus.SetAnimFloat

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name | Fungus.StringData | 
Value | Fungus.FloatData | 

# Set Anim Integer # {#SetAnimInteger}
Sets an integer parameter on an Animator component to control a Unity animation

Defined in Fungus.SetAnimInteger

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name | Fungus.StringData | 
Value | Fungus.IntegerData | 

# Set Anim Trigger # {#SetAnimTrigger}
Sets a trigger parameter on an Animator component to control a Unity animation

Defined in Fungus.SetAnimTrigger

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name | Fungus.StringData | 
_duration | Fungus.FloatData | 

# Set Custom Anim Trigger # {#SetCustomAnimTrigger}
Sets a trigger parameter on an Animator component to control a Unity animation

Defined in Fungus.SetCustomAnimTrigger

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name | Fungus.StringData | 
_duration | Fungus.FloatData | 
Anim Type | Fungus.SetCustomAnimTrigger+AnimTypes | 

# Set Custom Multi Anim Trigger # {#SetCustomMultiAnimTrigger}
Sets a trigger parameter on an Animator component to control a Unity animation edited for using multiple animations at once

Defined in SetCustomMultipleAnimTrigger

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name Primary | Fungus.StringData | 
_parameter Name Secondary | Fungus.StringData | 
Anim Type | SetCustomMultipleAnimTrigger+PrimaryAnimTypes | 
Anim Type2 | SetCustomMultipleAnimTrigger+SecondaryAnimTypes | 

# Set Multi Anim Trigger # {#SetMultiAnimTrigger}
Sets a trigger parameter on an Animator component to control a Unity animation edited for using multiple animations at once

Defined in SetMultipleAnimTrigger

Property | Type | Description
 --- | --- | ---
_animator | Fungus.AnimatorData | 
_parameter Name Primary | Fungus.StringData | 
_parameter Name Secondary | Fungus.StringData | 

