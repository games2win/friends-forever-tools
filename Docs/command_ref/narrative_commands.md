# Narrative commands # {#narrative_commands}

[TOC]
# Clear Menu # {#ClearMenu}
Clears the options from a menu dialogue

Defined in Fungus.ClearMenu

Property | Type | Description
 --- | --- | ---
Menu Dialog | Fungus.MenuDialog | 

# Control Stage # {#ControlStage}
Controls the stage on which character portraits are displayed.

Defined in Fungus.ControlStage

Property | Type | Description
 --- | --- | ---
Stage | Fungus.Stage | 
Replaced Stage | Fungus.Stage | 
Use Default Settings | System.Boolean | 
Fade Duration | System.Single | 
Wait Until Finished | System.Boolean | 
Display | Fungus.StageDisplayType | 

# Conversation # {#Conversation}
Do multiple say and portrait commands in a single block of text. Format is: [character] [portrait] [stage position] [: Story text]

Defined in Fungus.Conversation
# Menu # {#Menu}
Displays a button in a multiple choice menu

Defined in Fungus.Menu

Property | Type | Description
 --- | --- | ---
Text | System.String | 
Description | System.String | 
Target Block | Fungus.Block | 
Hide If Visited | System.Boolean | 
Interactable | Fungus.BooleanData | 
Set Menu Dialog | Fungus.MenuDialog | 

# Menu Timer # {#MenuTimer}
Displays a timer bar and executes a target block if the player fails to select a menu option in time.

Defined in Fungus.MenuTimer

Property | Type | Description
 --- | --- | ---
_duration | Fungus.FloatData | 
Target Block | Fungus.Block | 

# Portrait # {#Portrait}
Controls a character portrait.

Defined in Fungus.Portrait

Property | Type | Description
 --- | --- | ---
Stage | Fungus.Stage | 
Character | Fungus.Character | 
Replaced Character | Fungus.Character | 
Portrait | UnityEngine.Sprite | 
Offset | Fungus.PositionOffset | 
From Position | UnityEngine.RectTransform | 
To Position | UnityEngine.RectTransform | 
Facing | Fungus.FacingDirection | 
Use Default Settings | System.Boolean | 
Fade Duration | System.Single | 
Move Duration | System.Single | 
Shift Offset | UnityEngine.Vector2 | 
Move | System.Boolean | 
Shift Into Place | System.Boolean | 
Wait Until Finished | System.Boolean | 
Display | Fungus.DisplayType | 

# Say # {#Say}
Writes text in a dialog box.

Defined in Fungus.Say

Property | Type | Description
 --- | --- | ---
Description | System.String | 
Character | Fungus.Character | 
Portrait | UnityEngine.Sprite | 
Voice Over Clip | UnityEngine.AudioClip | 
Show Always | System.Boolean | 
Show Count | System.Int32 | 
Extend Previous | System.Boolean | 
Fade When Done | System.Boolean | 
Wait For Click | System.Boolean | 
Stop Voiceover | System.Boolean | 
Set Say Dialog | Fungus.SayDialog | 

# Set Language # {#SetLanguage}
Set the active language for the scene. A Localization object with a localization file must be present in the scene.

Defined in Fungus.SetLanguage

Property | Type | Description
 --- | --- | ---
_language Code | Fungus.StringData | 

# Set Menu Dialog # {#SetMenuDialog}
Sets a custom menu dialog to use when displaying multiple choice menus

Defined in Fungus.SetMenuDialog

Property | Type | Description
 --- | --- | ---
Menu Dialog | Fungus.MenuDialog | 

# Set Say Dialog # {#SetSayDialog}
Sets a custom say dialog to use when displaying story text

Defined in Fungus.SetSayDialog

Property | Type | Description
 --- | --- | ---
Say Dialog | Fungus.SayDialog | 

