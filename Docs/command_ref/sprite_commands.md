# Sprite commands # {#sprite_commands}

[TOC]
# Fade Sprite # {#FadeSprite}
Fades a sprite to a target color over a period of time.

Defined in Fungus.FadeSprite

Property | Type | Description
 --- | --- | ---
Sprite Renderer | UnityEngine.SpriteRenderer | 
_duration | Fungus.FloatData | 
_target Color | Fungus.ColorData | 
Wait Until Finished | System.Boolean | 

# Set Clickable 2D # {#SetClickable2D}
Sets a Clickable2D component to be clickable / non-clickable.

Defined in Fungus.SetClickable2D

Property | Type | Description
 --- | --- | ---
Target Clickable2 D | Fungus.Clickable2D | 
Active State | Fungus.BooleanData | 

# Set Collider # {#SetCollider}
Sets all collider (2d or 3d) components on the target objects to be active / inactive

Defined in Fungus.SetCollider

Property | Type | Description
 --- | --- | ---
Target Objects | System.Collections.Generic.List`1[UnityEngine.GameObject] | 
Target Tag | System.String | 
Active State | Fungus.BooleanData | 

# Set Draggable 2D # {#SetDraggable2D}
Sets a Draggable2D component to be draggable / non-draggable.

Defined in Fungus.SetDraggable2D

Property | Type | Description
 --- | --- | ---
Target Draggable2 D | Fungus.Draggable2D | 
Active State | Fungus.BooleanData | 

# Set Mouse Cursor # {#SetMouseCursor}
Sets the mouse cursor sprite.

Defined in Fungus.SetMouseCursor

Property | Type | Description
 --- | --- | ---
Cursor Texture | UnityEngine.Texture2D | 
Hot Spot | UnityEngine.Vector2 | 

# Set Sorting Layer # {#SetSortingLayer}
Sets the Renderer sorting layer of every child of a game object. Applies to all Renderers (including mesh, skinned mesh, and sprite).

Defined in Fungus.SetSortingLayer

Property | Type | Description
 --- | --- | ---
Target Object | UnityEngine.GameObject | 
Sorting Layer | System.String | 

# Set Sprite Order # {#SetSpriteOrder}
Controls the render order of sprites by setting the Order In Layer property of a list of sprites.

Defined in Fungus.SetSpriteOrder

Property | Type | Description
 --- | --- | ---
Target Sprites | System.Collections.Generic.List`1[UnityEngine.SpriteRenderer] | 
Order In Layer | Fungus.IntegerData | 

# Show Sprite # {#ShowSprite}
Makes a sprite visible / invisible by setting the color alpha.

Defined in Fungus.ShowSprite

Property | Type | Description
 --- | --- | ---
Sprite Renderer | UnityEngine.SpriteRenderer | 
_visible | Fungus.BooleanData | 
Affect Children | System.Boolean | 

