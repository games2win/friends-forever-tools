# Flow commands # {#flow_commands}

[TOC]
# Break # {#Break}
Force a loop to terminate immediately.

Defined in Fungus.Break
# Call # {#Call}
Execute another block in the same Flowchart as the command, or in a different Flowchart.

Defined in Fungus.Call

Property | Type | Description
 --- | --- | ---
Target Flowchart | Fungus.Flowchart | 
Target Block | Fungus.Block | 
Start Index | System.Int32 | 
Call Mode | Fungus.CallMode | 

# Else # {#Else}
Marks the start of a command block to be executed when the preceding If statement is False.

Defined in Fungus.Else
# Else If # {#ElseIf}
Marks the start of a command block to be executed when the preceding If statement is False and the test expression is true.

Defined in Fungus.ElseIf

Property | Type | Description
 --- | --- | ---
Variable | Fungus.Variable | 
Boolean Data | Fungus.BooleanData | 
Integer Data | Fungus.IntegerData | 
Float Data | Fungus.FloatData | 
String Data | Fungus.StringDataMulti | 
Compare Operator | Fungus.CompareOperator | 

# End # {#End}
Marks the end of a conditional block.

Defined in Fungus.End
# If # {#If}
If the test expression is true, execute the following command block.

Defined in Fungus.If

Property | Type | Description
 --- | --- | ---
Variable | Fungus.Variable | 
Boolean Data | Fungus.BooleanData | 
Integer Data | Fungus.IntegerData | 
Float Data | Fungus.FloatData | 
String Data | Fungus.StringDataMulti | 
Compare Operator | Fungus.CompareOperator | 

# Jump # {#Jump}
Move execution to a specific Label command in the same block

Defined in Fungus.Jump

Property | Type | Description
 --- | --- | ---
_target Label | Fungus.StringData | 

# Label # {#Label}
Marks a position in the command list for execution to jump to.

Defined in Fungus.Label

Property | Type | Description
 --- | --- | ---
Key | System.String | 

# Load Scene # {#LoadScene}
Loads a new Unity scene and displays an optional loading image. This is useful for splitting a large game across multiple scene files to reduce peak memory usage. Previously loaded assets will be released before loading the scene to free up memory.The scene to be loaded must be added to the scene list in Build Settings.

Defined in Fungus.LoadScene

Property | Type | Description
 --- | --- | ---
_scene Name | Fungus.StringData | 
Loading Image | UnityEngine.Texture2D | 

# Quit # {#Quit}
Quits the application. Does not work in Editor or Webplayer builds. Shouldn't generally be used on iOS.

Defined in Fungus.Quit
# Send Message # {#SendMessage}
Sends a message to either the owner Flowchart or all Flowcharts in the scene. Blocks can listen for this message using a Message Received event handler.

Defined in Fungus.SendMessage

Property | Type | Description
 --- | --- | ---
Message Target | Fungus.MessageTarget | 
_message | Fungus.StringData | 

# Stop # {#Stop}
Stop executing the Block that contains this command.

Defined in Fungus.Stop
# Stop Block # {#StopBlock}
Stops executing the named Block

Defined in Fungus.StopBlock

Property | Type | Description
 --- | --- | ---
Flowchart | Fungus.Flowchart | 
Block Name | Fungus.StringData | 

# Stop Flowchart # {#StopFlowchart}
Stops execution of all Blocks in a Flowchart

Defined in Fungus.StopFlowchart

Property | Type | Description
 --- | --- | ---
Stop Parent Flowchart | System.Boolean | 
Target Flowcharts | System.Collections.Generic.List`1[Fungus.Flowchart] | 

# Wait # {#Wait}
Waits for period of time before executing the next command in the block.

Defined in Fungus.Wait

Property | Type | Description
 --- | --- | ---
_duration | Fungus.FloatData | 

# Wait Frames # {#WaitFrames}
Waits for a number of frames before executing the next command in the block.

Defined in Fungus.WaitFrames

Property | Type | Description
 --- | --- | ---
Frame Count | Fungus.IntegerData | 

# While # {#While}
Continuously loop through a block of commands while the condition is true. Use the Break command to force the loop to terminate immediately.

Defined in Fungus.While

Property | Type | Description
 --- | --- | ---
Variable | Fungus.Variable | 
Boolean Data | Fungus.BooleanData | 
Integer Data | Fungus.IntegerData | 
Float Data | Fungus.FloatData | 
String Data | Fungus.StringDataMulti | 
Compare Operator | Fungus.CompareOperator | 

